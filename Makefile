MAINS = src/Mains
CLASES = src/Dominio/Clases
CONTROLADORES = src/Dominio/Controladores
DRIVERS = src/Dominio/Tests/Drivers
JUNITS = src/Dominio/Tests/JUnits
PERSISTENCIA = src/Persistencia
PRESENTACION = src/Presentacion

default: compile

compile:
	javac -cp "lib/*" \
	$(MAINS)/*.java \
	$(CLASES)/*.java \
	$(CONTROLADORES)/*.java \
	$(DRIVERS)/*java \
	$(JUNITS)/*java \
	$(PERSISTENCIA)/*java \
	$(PRESENTACION)/*java \
	-d bin/


run:
	java -cp "bin/" Mains.Main
	
runConsole:
	java -cp "bin/" Mains.MainConsola
	
runDriver:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" Dominio.Tests.Drivers.MenuDrivers
	
	
jar:
	jar cvmf MANIFEST_APP.MF KakuroGameAPP.jar -C bin .
	
jarConsole:
	jar cvmf MANIFEST_CON.MF KakuroGameCON.jar -C bin . 

jarDriver:
	jar cvmf MANIFEST_DRI.MF MenuDrivers.jar -C bin . 


runjar: 
	java -jar KakuroGameAPP.jar
	
runjarConsole: 
	java -jar KakuroGameCON.jar

runjarDriver: 
	java -jar MenuDrivers.jar


	
allJunits: junitKakuro junitTile junitUtiles junitPersistenciaKakuro junitPersistenciaUsuario junitUsuario
	
	
junitKakuro:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestKakuro
	
junitTile:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestTile

junitUtiles:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestUtiles

junitPersistenciaKakuro:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestPersistenciaKakuro

junitPersistenciaUsuario:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestPersistenciaUsuario

junitUsuario:
	java -cp "lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin/" org.junit.runner.JUnitCore Dominio.Tests.JUnits.TestUsuario



clean:
	rm -rf bin/*
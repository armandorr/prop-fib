------------------------------------------------
|       Directorio general del proyecto       |
------------------------------------------------

Este es el directorio donde tenemos todas las carpetas esenciales del proyecto.

DIRECTORIOS:

·src:	archivos .java del proyecto
·data:  aqui guardamos carpetas que contienen archivos .txt o media para la aplicacion
·doc:   carpeta que contiene toda la documentacion del proyecto
·lib:   carpeta que contiene librerias externas que hemos utilizado
·bin:	carpeta que contiene los binarios


FICHEROS:

·descripcioGrupo.txt:   fichero .txt con la informacion de los diferentes integrantes del grupo
·index.txt: 	        este mismo fichero
·Makefile:	        archivo Makefile del proyecto
·MANIFEST_APP.MF:	archivo manifest de la aplicacion grafica
·MANIFEST_CON.MF:	archivo manifest de la aplicacion de consola
·MANIFEST_DRI.MF:	archivo manifest de los drivers
·readme.txt:            archivo readme para indicar como compilar y ejecutar
·KakuroGameAPP.jar	ejecutable de la aplicacion grafica
·KakuroGameCON.jar	ejecutable de la aplicacion por consola
·MenuDrivers.jar	ejecutable del menu de drivers
/** @file insertManualView.java
 @brief Especificación de la vista insertManualView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class insertManualView
 @brief Vista que nos sirve para crear un Kakuro a partir introducir su representación en formato oficial. Tiene dos botones, uno para confirmar la introducción y otro para volver hacia atrás
 Al presionar el botón de confirmar, nos informará de que se ha generado el Kakuro con éxito o si no hemos introducido un Kakuro correcto (error en la sintaxis)

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista insertManualView
 */
public class insertManualView extends JFrame
{

    /** @brief Area para introducir el kakuro manualmente.*/
    private final JTextArea kakuroTextArea = new JTextArea();
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>op vale entre 1 y 3</em>
    \post Hemos creado la vista que nos muestra la interfaz a través de la cual podemos introducir un Kakuro a mano
     */
    public insertManualView(int op)
    {
        setMinimumSize(new Dimension(800,800));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Introducir kakuro manualmente");

        JLabel mainTitle = new JLabel("Porfavor inserta un Kakuro en formato estándar", SwingConstants.CENTER);
        mainTitle.setBounds(0,50,800,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        kakuroTextArea.setBounds(150,130,500,500);
        add(kakuroTextArea);

        JButton insertButton = new JButton("Introducir");
        insertButton.setBounds(230,650,150,30);
        insertButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(insertButton);

        JButton cancelButton = new JButton("Atrás");
        cancelButton.setBounds(450,650,100,30);
        cancelButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(cancelButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(710,710,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(800, 800, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,800,800);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener insertListener = e -> {
            String kakuro = kakuroTextArea.getText();
            if(kakuro.equals("")){
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Porfavor introduce un Kakuro en formato estándar");
            }
            else{
                boolean correct = true;
                try{
                    CtrlPresentacion.cargarKakuroDeString(kakuro);
                }catch (Exception f){
                    correct = false;
                    JOptionPane.showMessageDialog(null, "Porfavor introduce un Kakuro correcto en formato estándar");
                    Toolkit.getDefaultToolkit().beep();
                }
                //luego le meteremos el nombre cuando se lo pidamos al user
                if(correct) {
                    if (op == 1) CtrlPresentacion.mostrarVistaKakuroVerificar(null, true);
                    else if(op == 2) CtrlPresentacion.mostrarVistaKakuroSolucionar();
                    setVisible(false);
                    dispose();
                }
            }
        };
        insertButton.addActionListener(insertListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(op);
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);
    }
}

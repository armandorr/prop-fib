/** @file generateOptionsView.java
 @brief Especificación de la vista generateOptionsView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/** @class generateOptionsView
 @brief Vista intermedia que nos permite ajustar los parámetros a la hora de generar un kakuro. Los parametros a introducir son
 numero de filas, número de columnas y dificultad.

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista generateOptionsView
 */

public class generateOptionsView extends JFrame
{

    /** @brief Label que contiene el título principal.*/
    private final JLabel mainTitle = new JLabel("Escoge los valores del kakuro",SwingConstants.CENTER);
    /** @brief Label que contiene la indicación del número de filas.*/
    private final JLabel numRowsLabel = new JLabel("Número de filas:");
    /** @brief Label que contiene la indicación del número de columnas.*/
    private final JLabel numColsLabel = new JLabel("Número de columnas:");
    /** @brief Label que contiene la indicación de la dificultad.*/
    private final JLabel diffLabel = new JLabel("Dificultad:");
    /** @brief Botón para generar el Kakuro.*/
    private final JButton generateButton = new JButton("Generar Kakuro");
    /** @brief Botón para regresar a la vista anterior.*/
    private final JButton exitButton = new JButton("Volver");
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Valor que tiene el spinner de numero de filas.*/
    public int rows;
    /** @brief Valor que tiene el spinner de numero de columnas.*/
    public int cols;
    /** @brief Valor que tiene el spinner de dificultad.*/
    public int diff = 1;
    /** @brief Indicador de la operación que estamos ejecutando.*/
    public int operation;
    /** @brief Valor usado para la animación en la pantalla de carga*/
    int estado = 0;
    /** @brief Bool que indica si hemos empezado a generar*/
    boolean pulsado = false;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos permite ajustar los parámetros a la hora de generar un kakuro.
     */
    public generateOptionsView(int op)
    {
        operation = op;
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Generar kakuro");

        SwingWorker<String[][],Integer> gw = new generatorWorker();

        mainTitle.setBounds(0,50,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        numRowsLabel.setBounds(90,150,150,30);
        numRowsLabel.setForeground(Color.BLACK);
        add(numRowsLabel);

        SpinnerNumberModel numRowsModel = new SpinnerNumberModel(15,5,30,1);
        JSpinner numRowsSpinner = new JSpinner(numRowsModel);
        JComponent editor = numRowsSpinner.getEditor();
        JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor)editor;
        spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
        spinnerEditor.getTextField().setForeground(Color.BLACK);
        numRowsSpinner.setBounds(120,200,50,35);
        add(numRowsSpinner);

        numColsLabel.setBounds(280,150,200,30);
        numColsLabel.setForeground(Color.BLACK);
        add(numColsLabel);

        SpinnerNumberModel numColsModel = new SpinnerNumberModel(15,5,30,1);
        JSpinner numColsSpinner = new JSpinner(numColsModel);
        editor = numColsSpinner.getEditor();
        spinnerEditor = (JSpinner.DefaultEditor)editor;
        spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
        spinnerEditor.getTextField().setForeground(Color.BLACK);
        numColsSpinner.setBounds(335,200,50,35);
        add(numColsSpinner);

        diffLabel.setBounds(170,100,150,30);
        diffLabel.setForeground(Color.BLACK);
        add(diffLabel);

        String[] dificultadSpinner = new String[3];
        dificultadSpinner[0] = "Fácil"; dificultadSpinner[1] = "Mediana"; dificultadSpinner[2] = "Difícil";
        SpinnerListModel model = new SpinnerListModel(dificultadSpinner);
        JSpinner diffSpinner = new JSpinner(model);
        editor = diffSpinner.getEditor();
        spinnerEditor = (JSpinner.DefaultEditor)editor;
        spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
        spinnerEditor.getTextField().setForeground(Color.BLACK);
        diffSpinner.setBounds(250,100,80,35);
        diffSpinner.addChangeListener(e -> {
            String value = diffSpinner.getValue().toString();
            int rows = Integer.parseInt(numRowsSpinner.getValue().toString());
            int cols = Integer.parseInt(numColsSpinner.getValue().toString());
            if(value.equals("Mediana")){
                numRowsModel.setMaximum(30);
                numColsModel.setMaximum(30);
            }
            else if(value.equals("Difícil")){
                numRowsModel.setMaximum(18);
                numRowsModel.setValue(Math.min(rows, 18));
                numColsModel.setMaximum(18);
                numColsModel.setValue(Math.min(cols, 18));
            }
        });
        add(diffSpinner);

        generateButton.setBounds(140, 270, 220, 30);
        generateButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(generateButton);

        exitButton.setBounds(140, 320, 220, 30);
        exitButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(exitButton);

        JLabel loadingLabel = new JLabel("Generando Kakuro",JTextField.CENTER);
        loadingLabel.setBounds(0,175,512,50);
        loadingLabel.setFont(new Font("Monospaced", Font.BOLD, 30));
        loadingLabel.setForeground(Color.BLACK);
        loadingLabel.setVisible(false);
        add(loadingLabel);

        JButton cancelButton = new JButton("Cancelar");
        cancelButton.setBounds(180,250,150,50);
        cancelButton.setFont(new Font("Monospaced", Font.BOLD, 20));
        cancelButton.setVisible(false);
        add(cancelButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener generateListener = e -> {
            String dificultad = diffSpinner.getValue().toString();

            if(dificultad.equals("Fácil")) diff = 1;
            else diff = (dificultad.equals("Mediana"))? 2 : 3;

            rows = Integer.parseInt(numRowsSpinner.getValue().toString());
            cols = Integer.parseInt(numColsSpinner.getValue().toString());

            mainTitle.setVisible(false);        generateButton.setVisible(false);
            numRowsLabel.setVisible(false);     numRowsSpinner.setVisible(false);
            numColsLabel.setVisible(false);     numColsSpinner.setVisible(false);
            diffLabel.setVisible(false);        diffSpinner.setVisible(false);
            soundButton.setVisible(false);      exitButton.setVisible(false);

            loadingLabel.setVisible(true);      cancelButton.setVisible(true);

            pulsado = true;
            gw.execute();
        };
        generateButton.addActionListener(generateListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            if(op==3) CtrlPresentacion.mostrarVistaGenerarOCargarKakuro();
            else CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(1);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            try{
                gw.cancel(true);
            }catch (Exception ignore){
                //do nothing, ignore
            }
            pulsado = false;
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);

        Timer timer = new Timer(1000, e -> {
            if(pulsado){
                if(estado == 0) loadingLabel.setText("Generando Kakuro");
                if(estado == 1) loadingLabel.setText("Generando Kakuro.");
                if(estado == 2) loadingLabel.setText("Generando Kakuro..");
                if(estado == 3) loadingLabel.setText("Generando Kakuro...");
                if(++estado == 4) estado = 0;
            }
        });
        timer.start();
    }

    /** @class generatorWorker
     @brief Worker creado para obtener un kakuro generado
            Contiene la función que realiza dicha tarea y la función que se ejecuta cuando acaba
     */
    class generatorWorker extends SwingWorker<String[][], Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a generar
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna el Kakuro generado
         */
        protected String[][] doInBackground()
        {
            return CtrlPresentacion.generarKakuro(rows,cols,diff);
        }

        /** @brief Operación que se ejecuta cuando ha acabado de ejecutarse la función doInBackground
        \pre <em>La función doInBackground ha acabado y ya tenemos el resultado</em>
        \post Se ha llamado a la vista correspondiente para visualizar el kakuro generado
         */
        @Override
        protected void done()
        {
            String[][] kakGen = null;
            try {
                kakGen = get();
            } catch (InterruptedException | ExecutionException interruptedException) {
                interruptedException.printStackTrace();
            }
            CtrlPresentacion.cargarKakuroDeMatrixString(kakGen);
            CtrlPresentacion.mostrarVistaKakuroGenerado(operation,kakGen);
            pulsado = false;
            setVisible(false);
            dispose();
        }
    }
}

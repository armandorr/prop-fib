/** @file userMenuView.java
 @brief Especificación de la vista userMenuView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class userMenuView
 @brief Vista que nos muestra las opciones disponibles una vez hemos iniciado sesión, o hemos decidido entrar como invitado.

 Hecha por David Marin

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista userMenuView
 */

public class userMenuView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista que muestra el menú principal al usuario. Cabe destacar que si
     *  entramos como invitado algunas funcionalidades no estan disponibles.
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos muestra el menú principal del usuario logeado
     */
    public userMenuView()
    {
        boolean invitado = CtrlPresentacion.getUserName() == null;
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Menú principal");

        CtrlPresentacion.setNombrePartida(null);

        JLabel mainTitle = new JLabel("Menú Principal", SwingConstants.CENTER);
        mainTitle.setBounds(160, 30, 186, 30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        if(!invitado) {
            JLabel userName = new JLabel("Usuario: user", SwingConstants.CENTER);
            userName.setText("Usuario: " + CtrlPresentacion.getUserName());
            userName.setBounds(160, 55, 186, 30);
            userName.setFont(new Font("Monospaced", Font.BOLD, 15));
            userName.setForeground(Color.BLACK);
            add(userName);
        }

        JButton playButton = new JButton("Jugar Kakuro");
        playButton.setBounds(160, 90, 186, 30);
        playButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(playButton);

        JButton verifyButton = new JButton("Verificar Kakuro");
        verifyButton.setBounds(160, 130, 186, 30);
        verifyButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(verifyButton);

        JButton solveButton = new JButton("Resolver Kakuro");
        solveButton.setBounds(160, 170, 186, 30);
        solveButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(solveButton);

        JButton exitButton = new JButton("Cerrar Sesión");
        exitButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        JButton personalRepoButton = new JButton("Repositorio Personal");
        personalRepoButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        JButton generalRepoButton = new JButton("Repositorio General");
        generalRepoButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        JButton editButton = new JButton("Editar Perfil");
        editButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        JButton rankingButton = new JButton("Ver Ranking");
        rankingButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        if (!invitado) {
            personalRepoButton.setBounds(160, 210, 186, 30);
            add(personalRepoButton);
            generalRepoButton.setBounds(160, 250, 186, 30);
            rankingButton.setBounds(160, 290, 186, 30);
            editButton.setBounds(160, 340, 186, 30);
            add(editButton);
            exitButton.setBounds(160, 380, 186, 30);
        }
        else {
            generalRepoButton.setBounds(160, 210, 186, 30);
            rankingButton.setBounds(160, 250, 186, 30);
            exitButton.setBounds(160, 320, 186, 30);
        }

        add(generalRepoButton);
        add(exitButton);
        add(rankingButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener playListener = e -> {
            CtrlPresentacion.mostrarVistaNuevoOCargarKakuro(invitado);
            setVisible(false);
            dispose();
        };
        playButton.addActionListener(playListener);

        ActionListener verifyListener = e -> {
            CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(1);
            setVisible(false);
            dispose();
        };
        verifyButton.addActionListener(verifyListener);

        ActionListener solveListener = e -> {
            CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(2);
            setVisible(false);
            dispose();
        };
        solveButton.addActionListener(solveListener);

        ActionListener pRepoListener = e -> {
            CtrlPresentacion.mostrarVistaRepositorioPersonal();
            setVisible(false);
            dispose();
        };
        personalRepoButton.addActionListener(pRepoListener);

        ActionListener gRepoListener = e -> {
            CtrlPresentacion.mostrarVistaRepoFiles(3);
            setVisible(false);
            dispose();
        };
        generalRepoButton.addActionListener(gRepoListener);

        ActionListener rankingListener = e -> {
            CtrlPresentacion.mostrarVistaRanking();
            setVisible(false);
            dispose();
        };
        rankingButton.addActionListener(rankingListener);

        ActionListener editListener = e -> {
            CtrlPresentacion.mostrarVistaEditUser();
            setVisible(false);
            dispose();
        };
        editButton.addActionListener(editListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.setUser(null, null);
            CtrlPresentacion.iniciarPresentacion(false);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

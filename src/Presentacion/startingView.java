/** @file startingView.java
 @brief Especificación de la vista startingView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class startingView
 @brief Primera vista que se muestra al iniciar nuestro programa

 Hecha por Marco Patiño

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista startingView
 */
public class startingView extends JFrame
{

    /** @brief Indicador de si esta sonando musica o no.*/
    private static boolean playingMusic;
    /** @brief Posicion de la pista de audio.*/
    private static long clipTimePosition;
    /** @brief Clip de audio.*/
    private static Clip clip;
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista para poder acceder a un usario, crear una cuenta, entrar como usuario o
               cerrar el programa
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos muestra el primer menú de nuestro programa
     */
    public startingView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Main page");

        CtrlPresentacion.setUser(null,null);

        JLabel mainTitle = new JLabel("Proyecto Kakuro", SwingConstants.CENTER);
        mainTitle.setBounds(6,50,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 25));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel secondTitle = new JLabel("Grupo 23.1", SwingConstants.CENTER);
        secondTitle.setBounds(6,80,500,30);
        secondTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        secondTitle.setForeground(Color.BLACK);
        add(secondTitle);

        JButton loginButton = new JButton("Acceder cuenta");
        loginButton.setBounds(166, 170, 180, 30);
        loginButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(loginButton);

        JButton signUpButton = new JButton("Crear cuenta");
        signUpButton.setBounds(166, 210, 180, 30);
        signUpButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(signUpButton);

        JButton playAsGuestButton = new JButton("Jugar como invitado");
        playAsGuestButton.setBounds(166, 250, 180, 30);
        playAsGuestButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(playAsGuestButton);

        JButton closeButton = new JButton("Salir");
        closeButton.setBounds(166, 290, 180, 30);
        closeButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(closeButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener loginListener = e -> {
            CtrlPresentacion.mostrarVistaLogin();
            setVisible(false);
            dispose();
        };
        loginButton.addActionListener(loginListener);

        ActionListener signListener = e -> {
            CtrlPresentacion.mostrarVistaCrearCuenta();
            setVisible(false);
            dispose();
        };
        signUpButton.addActionListener(signListener);

        ActionListener guestListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        playAsGuestButton.addActionListener(guestListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener closeListener = e -> System.exit(0);
        closeButton.addActionListener(closeListener);
    }

    /** @brief Función que hace que empieze a sonar la música
    \pre <em>Cierto</em>
    \post Ha empezado a sonar la música
     */
    public static void startMusic()
    {
        File musicPath = new File("data/Media/chillMusic.wav");
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(musicPath);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            playingMusic = true;
        } catch (Exception ignore){}
    }

    /** @brief Función que para o reanuda la música
    \pre <em>Cierto</em>
    \post La música ha sido parada si estaba sonando o ha sido reanudadd si estaba parada
     */
    public static void pause_reanude()
    {
        playingMusic = !playingMusic;
        if(clip != null) {
            if (playingMusic) {
                try {
                    clip.setMicrosecondPosition(clipTimePosition);
                    clip.start();
                    clip.loop(Clip.LOOP_CONTINUOUSLY);
                } catch (Exception ignore) {
                }
            } else {
                clipTimePosition = clip.getMicrosecondPosition();
                clip.stop();
            }
        }
    }

    /** @brief Consultora de si la musica esta parada o no
    \pre <em>Cierto</em>
    \post Retorna cierto si esta parado y falso en caso contrario
     */
    public static boolean getIfMuted()
    {
        return !playingMusic;
    }

    /** @brief Consultora de si la musica esta parada o no
    \pre <em>Cierto</em>
    \post Retorna cierto si esta parado y falso en caso contrario
     */
    public static boolean getCanListenToMusic()
    {
        return clip != null;
    }
}
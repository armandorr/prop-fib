/** @file verifyFinishedView.java
 @brief Especificación de la vista verifyFinishedView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class verifyFinishedView
 @brief Vista que nos muestra los resultados de la funcionalidad verificar.

 Hecha por Marco Patiño

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista verifyFinishedView
 */
public class verifyFinishedView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista que muestra los resultados de la funcionalidad verificar en un kakuro concreto.
     * Recibe algunos parametros que nos permiten representar mejor nuestra solución. El parámetro resultat contiene el numero de
     * soluciones del kakuro. El booleano manual indica si el kakuro ha sido introducido manualmente o no. Y finalmente la matriz
     * de strings es el kakuro original sobre el cual hemos aplicado la funcionalidad.
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos muestra los resultados de la funcionalidad verificar
     */
    public verifyFinishedView(int resultat, boolean manual, String[][] kakOriginal)
    {
        String[][] Kakuro = CtrlPresentacion.getKakuroStringMatrixFormat();
        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row= 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField();
                kakuroContainer.add(board[row][col]);
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                }
                else if(stringTile.length() == 1){
                    if (stringTile.equals("#")){
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    }
                    else{
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                }
                else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        kakuroContainer.setPreferredSize(new Dimension(cellSize*M, cellSize*N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize*M, cellSize*N));

        JLabel titleLabel = new JLabel("Kakuro Verificado");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        String sols;
        if (resultat == 0) sols = "No tiene solución";
        else if (resultat == 1) sols = "Tiene solución única";
        else sols = "Tiene más de una solución";

        JLabel solLabel = new JLabel(sols);
        solLabel.setFont(new Font("Monospaced", Font.BOLD, 18));
        solLabel.setForeground(Color.BLACK);
        solLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton saveButton = new JButton("Guardar Kakuro");
        saveButton.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Volver al menu principal");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT,40,5));
        if(CtrlPresentacion.getUserName() != null && (resultat == 1 || resultat == 2)) flowContainer.add(saveButton); //solo puede guardar un usuario si tiene una solución o dos
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;
        bagConstraints.gridy = 0;
        containerPrincipal.add(titleLabel, bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(kakuroContainer, bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(solLabel, bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 6;
        containerPrincipal.add(flowContainer, bagConstraints);

        bagConstraints.gridy = 7;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        int tamRow = Math.min(820,Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820,Math.max(512, cellSize * (M + 5)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Verificar Kakuro");
        setVisible(true);

        ActionListener saveListener = e -> {
            String nombre = CtrlPresentacion.getNombrePartida();
            int resposta;
            if (nombre == null) { //si se ha cargado manualmente o generado
                if (manual) nombre = JOptionPane.showInputDialog("¿Que nombre quieres ponerle al Kakuro introducido y a su solución?");
                else nombre = JOptionPane.showInputDialog("¿Que nombre quieres ponerle al Kakuro generado y a su solución?");
                if (nombre != null) {
                    // 0=yes, 1=no, 2=cancel
                    if(resultat == 2){
                        Toolkit.getDefaultToolkit().beep();
                        resposta = JOptionPane.showConfirmDialog(null,"El Kakuro tiene mas de una solución. ¿Seguro que quieres guardarlo?");
                    }
                    else resposta = 0;
                    if(resposta == 0) { //yes
                        String user = CtrlPresentacion.getUserName();
                        CtrlPresentacion.cargarKakuroDeMatrixString(kakOriginal);
                        if(CtrlPresentacion.noExisteEsteFichero(nombre + ".txt", "data/Users/" + user + "/partidasUser/")){
                            CtrlPresentacion.guardarKakuroEnRepositorio(nombre, false);
                            CtrlPresentacion.cargarKakuroDeMatrixString(Kakuro);
                            CtrlPresentacion.guardarKakuroEnRepositorio(nombre, true);
                            if(manual) JOptionPane.showMessageDialog(null, "Kakuro introducido y solución guardados en data/Repositorio como " + nombre + " .txt y .sol respectivamente");
                            else JOptionPane.showMessageDialog(null, "Kakuro generado y solución guardados en data/Repositorio como " + nombre + " .txt y .sol respectivamente");
                            CtrlPresentacion.mostrarVistaMenuUsuario();
                            setVisible(false);
                            dispose();
                        }
                        else{
                            Toolkit.getDefaultToolkit().beep();
                            JOptionPane.showMessageDialog(null, "Ya existe un Kakuro guardado con ese nombre. Porfavor vuelva a intentarlo" );
                        }
                    }
                }
            }
            else {
                if(resultat == 2){
                    Toolkit.getDefaultToolkit().beep();
                    resposta = JOptionPane.showConfirmDialog(null,"El Kakuro tiene mas de una solución. ¿Seguro que quieres guardarlo?");
                }
                else resposta = 0;
                if(resposta == 0) {
                    CtrlPresentacion.guardarKakuroEnRepositorio(nombre, true);
                    JOptionPane.showMessageDialog(null, "Solución del Kakuro guardada como " + nombre + ".sol en data/Repositorio");
                    CtrlPresentacion.mostrarVistaMenuUsuario();
                    setVisible(false);
                    dispose();
                }
            }
        };
        saveButton.addActionListener(saveListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}
/** @file generateOrLoadView.java
 @brief Especificación de la vista generateOrLoadView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class generateOrLoadView
 @brief Vista que nos sirve para acceder a dos funcionalidades. Se encarga de mostrarnos dos botones que podemos pulsar:
 El primer botón nos lleva a la vista para generar un Kakuro a partir de unos parámetros (filas, columnas, dificultad)
 El segundo botón nos permite abrir un Kakuro en blanco del repositorio de Kakuros

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista generateOrLoadView
 */

public class generateOrLoadView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos muestra el menú intermediario a través del cual elegimos si crear un Kakuro nuevo o elegir uno blanco del repositorio
     */
    public generateOrLoadView()
    {
        setMinimumSize(new Dimension(512, 512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Generar o cargar kakuro");

        JLabel mainTitle = new JLabel("Escoge el Kakuro nuevo para jugar", SwingConstants.CENTER);
        mainTitle.setBounds(0,50,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JButton generateButton = new JButton("Generar Kakuro");
        generateButton.setBounds(140, 130, 220, 30);
        generateButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(generateButton);

        JButton repoButton = new JButton("Escoger del repositorio");
        repoButton.setBounds(140, 170, 220, 30);
        repoButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(repoButton);

        JButton exitButton = new JButton("Volver");
        exitButton.setBounds(140, 220, 220, 30);
        exitButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(exitButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener generateListener = e -> {
            CtrlPresentacion.mostrarVistaOpcionesGenerar(3); //generated to play
            setVisible(false);
            dispose();
        };
        generateButton.addActionListener(generateListener);

        ActionListener repoListener = e -> {
            String userDir = System.getProperty("user.dir");
            userDir += "/data/Repositorio";
            JFileChooser chooser = new JFileChooser(userDir);
            chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File arxiu = chooser.getSelectedFile();
                CtrlPresentacion.cargarKakuroDeArchivo(arxiu.getAbsolutePath());
                CtrlPresentacion.mostrarVistaKakuroElegido(userDir, 0);
                setVisible(false);
                dispose();
            }
        };
        repoButton.addActionListener(repoListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaNuevoOCargarKakuro(CtrlPresentacion.getUserName()==null);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

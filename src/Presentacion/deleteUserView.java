/** @file deleteUserView.java
 @brief Especificación de la vista deleteUserView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class deleteUserView
 @brief Vista que nos sirve para acceder a la funcionalidad de eliminar un usuario. Para ello
 el usuario tendrá que rellenar tres campos de texto, uno para escribir la contraseña actual y dos para
 escribir la nueva contraseña.

 Hecha por David Marin

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista changePasswordView
 */

public class deleteUserView extends JFrame
{

    /** @brief Campo 1 para introducir la contraseña actual.*/
    private final JPasswordField userPassTextField = new JPasswordField();
    /** @brief Campo 2 para introducir la contraseña actual.*/
    private final JPasswordField userPassTextField2 = new JPasswordField();
    /** @brief Campo para introducir la palabra de confirmación.*/
    private final JTextField confirmTextField = new JTextField();
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos permite eliminar nuestro usuario.
     */
    public deleteUserView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Eliminar perfil");

        JLabel userName = new JLabel("Usuario: user", SwingConstants.CENTER);
        userName.setText("Usuario: " + CtrlPresentacion.getUserName());
        userName.setBounds(166, 55, 180, 30);
        userName.setFont(new Font("Monospaced", Font.BOLD, 15));
        userName.setForeground(Color.BLACK);
        add(userName);

        JLabel mainTitle = new JLabel("Eliminar Perfil", SwingConstants.CENTER);
        mainTitle.setBounds(166, 30, 180, 30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel userPassLabel = new JLabel("Contraseña:");
        userPassLabel.setBounds(117,130,150,30);
        userPassLabel.setForeground(Color.BLACK);
        add(userPassLabel);
        userPassTextField.setBounds(290,130,150,30);
        add(userPassTextField);

        JLabel userPassLabel2 = new JLabel("Repetir contraseña:");
        userPassLabel2.setBounds(117,170,150,30);
        userPassLabel2.setForeground(Color.BLACK);
        add(userPassLabel2);
        userPassTextField2.setBounds(290,170,150,30);
        add(userPassTextField2);

        JLabel confirmLabel = new JLabel("Escriba 'confirmo':");
        confirmLabel.setBounds(117,210,150,30);
        confirmLabel.setForeground(Color.BLACK);
        add(confirmLabel);
        confirmTextField.setBounds(290,210,150,30);
        add(confirmTextField);

        JButton backButton = new JButton("Volver");
        backButton.setBounds(166,340,180,30);
        backButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(backButton);

        JButton deleteButton = new JButton("Eliminar el perfil");
        deleteButton.setBounds(166,270,180,30);
        deleteButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(deleteButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener deleteListener = e -> {
            String p1 = String.valueOf(userPassTextField.getPassword());
            String p2 = String.valueOf(userPassTextField2.getPassword());
            String c = (confirmTextField.getText());
            String userName1 = CtrlPresentacion.getUserName();
            if(p1.equals(p2) && c.equals("confirmo") && p1.equals(CtrlPresentacion.getPassword(userName1))) {
                int res = JOptionPane.showConfirmDialog(null,"Esta seguro que desea eliminar el usuario "+userName1);
                if (res == 0) {
                    CtrlPresentacion.eliminarUsuario(userName1);
                    CtrlPresentacion.eliminarUsuarioEstado(userName1);
                    JOptionPane.showMessageDialog(null, "¡Usuario " + CtrlPresentacion.getUserName() + " eliminado!");
                    CtrlPresentacion.iniciarPresentacion(false);
                    setVisible(false);
                    dispose();
                }
            }
            else {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Contraseñas no coinciden, son incorrectas, o se le olvidó confirmar." +
                        " Revise los tres cuadros de texto");
            }
        };
        deleteButton.addActionListener(deleteListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener backListener = e -> {
            CtrlPresentacion.mostrarVistaEditUser();
            setVisible(false);
            dispose();
        };
        backButton.addActionListener(backListener);
    }
}

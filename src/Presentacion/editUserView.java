/** @file editUserView.java
 @brief Especificación de la vista editUserView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class editUserView
 @brief Vista intermedia que nos sirve para acceder a las funcionalidade que implicar editar un perfil, es decir: eliminar perfil y
 cambiar contraseña.

 Hecha por David Marin

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista editUserView
 */

public class editUserView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos permite acceder a las funcionalidades de editar perfiles.
     */
    public editUserView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Editar perfil");

        JLabel mainTitle = new JLabel("Editar Perfil", SwingConstants.CENTER);
        mainTitle.setBounds(166, 60, 180, 30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JButton deleteButton = new JButton("Eliminar Perfil");
        deleteButton.setBounds(166,150,180,30);
        deleteButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(deleteButton);

        JButton editPassButton = new JButton("Cambiar contraseña");
        editPassButton.setBounds(166,190,180,30);
        editPassButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(editPassButton);

        JButton backButton = new JButton("Volver");
        backButton.setBounds(166,250,180,30);
        backButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(backButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener deleteListener = e -> {
            CtrlPresentacion.mostrarVistaDeleteUser();
            setVisible(false);
            dispose();
        };
        deleteButton.addActionListener(deleteListener);

        ActionListener editPassListener = e -> {
            CtrlPresentacion.mostrarVistaCambiarContrasena();
            setVisible(false);
            dispose();
        };
        editPassButton.addActionListener(editPassListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener backListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        backButton.addActionListener(backListener);
    }
}

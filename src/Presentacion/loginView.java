/** @file loginView.java
 @brief Especificación de la vista loginView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class loginView
 @brief Vista que nos sirve para acceder a una cuenta. Contiene dos campos de texto para usuario y contraseña (que se oculta).
 Y dos botones, uno para intentar iniciar sesión, y otro para ir al menú inicial. Si la contraseña y el usuario son un match correcto y pulsamos el botón
 de acceder, nos iniciará sesión con el usuario introducido, de lo contrario nos notificará del problema

 Hecha por Marco Patiño

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista loginView
 */
public class loginView extends JFrame
{

    /** @brief Campo donde se introduce el nombre del usuario.*/
    private final JTextField userNameTextField = new JTextField();
    /** @brief Campo donde se introduce la constraseña del usuario.*/
    private final JPasswordField userPassTextField = new JPasswordField();
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos muestra el menú a través del cual iniciamos sesión
     */
    public loginView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Acceder cuenta");

        JLabel mainTitle = new JLabel("Porfavor ingresa tu cuenta", SwingConstants.CENTER);
        mainTitle.setBounds(6,50,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel userNameLabel = new JLabel("Usuario:");
        userNameLabel.setBounds(128,130,100,30);
        userNameLabel.setForeground(Color.BLACK);
        add(userNameLabel);
        userNameTextField.setBounds(220,130,150,30);
        add(userNameTextField);

        JLabel userPassLabel = new JLabel("Contraseña:");
        userPassLabel.setBounds(128,170,100,30);
        userPassLabel.setForeground(Color.BLACK);
        add(userPassLabel);
        userPassTextField.setBounds(220,170,150,30);
        add(userPassTextField);

        JButton loginButton = new JButton("Acceder");
        loginButton.setBounds(143,250,100,30);
        loginButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(loginButton);

        JButton cancelButton = new JButton("Volver");
        cancelButton.setBounds(268,250,100,30);
        cancelButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(cancelButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener loginListener = e -> {
            String username = userNameTextField.getText();
            String password = String.valueOf(userPassTextField.getPassword());
            boolean logued = CtrlPresentacion.accederCuenta(username, password);
            if (logued){
                CtrlPresentacion.mostrarVistaMenuUsuario();
                setVisible(false);
                dispose();
            }
            else {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrecta, prueba de nuevo. ");
            }
        };
        loginButton.addActionListener(loginListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.iniciarPresentacion(false);
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);
    }
}

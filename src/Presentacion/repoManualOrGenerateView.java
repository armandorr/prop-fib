/** @file repoManualOrGenerateView.java
 @brief Especificación de la vista repoManualOrGenerateView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class repoManualOrGenerateView
 @brief Vista que se nos muestra diferentes opciones para elegir el kakuro.
 Podemos elegir, dependiendo de la operacion con la que la llamemos, si introducir el kakuro maualmente,
 generarlo o seleccionar del repositorio.

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista repoManualOrGenerateView.
 */
public class repoManualOrGenerateView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em> 1 <= op <= 2 </em>
    \post Hemos creado la vista que nos muestra la pantalla diferentes opciones para elegir un kakuro
     */
    public repoManualOrGenerateView(int op)
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Escoger kakuro");

        JLabel mainTitle = new JLabel("Escoge la opción para elegir el kakuro", SwingConstants.CENTER);
        mainTitle.setBounds(0,50,512,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JButton repositorioButton = new JButton("Repositorio");
        repositorioButton.setBounds(166, 130, 180, 30);
        add(repositorioButton);

        JButton generateButton = new JButton("Generar uno nuevo");
        generateButton.setBounds(166, 170, 180, 30);
        add(generateButton);

        JButton manualButton = new JButton("Manualmente");
        manualButton.setBounds(166, 210, 180, 30);
        add(manualButton);

        JButton exitButton = new JButton("Volver");
        exitButton.setBounds(166, 260, 180, 30);
        add(exitButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener repoListener = e -> {
            String userDir = System.getProperty("user.dir");
            userDir += "/data/Repositorio";
            JFileChooser chooser = new JFileChooser(userDir);
            chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File arxiu = chooser.getSelectedFile();
                CtrlPresentacion.cargarKakuroDeArchivo(arxiu.getAbsolutePath());
                if (op == 1) {
                    String nombre = arxiu.getName();
                    nombre = nombre.replace(".txt", "");
                    CtrlPresentacion.mostrarVistaKakuroVerificar(nombre,false);
                }
                else if (op == 2) CtrlPresentacion.mostrarVistaKakuroSolucionar();
                setVisible(false);
                dispose();
            }
        };
        repositorioButton.addActionListener(repoListener);

        ActionListener generateListener = e -> {
            CtrlPresentacion.mostrarVistaOpcionesGenerar(op);
            setVisible(false);
            dispose();
        };
        generateButton.addActionListener(generateListener);

        ActionListener manualListener = e -> {
            CtrlPresentacion.mostrarVistaInsertarKakuroManual(op);
            setVisible(false);
            dispose();
        };
        manualButton.addActionListener(manualListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

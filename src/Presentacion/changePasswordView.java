/** @file changePasswordView.java
 @brief Especificación de la vista changePasswordView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class changePasswordView
 @brief Vista que nos sirve para acceder a la funcionalidad de cambiar la contraseña del usuario. Para ello
 el usuario tendrá que rellenar tres campos de texto, uno para escribir la contraseña actual y dos para
 escribir la nueva contraseña.

 Hecha por David Marin

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista changePasswordView
 */

public class changePasswordView extends JFrame
{

    /** @brief Campo 1 para introducir la contraseña nueva.*/
    private final JPasswordField userPassTextField = new JPasswordField();
    /** @brief Campo 2 para introducir la contraseña nueva.*/
    private final JPasswordField userPassTextField2 = new JPasswordField();
    /** @brief Campo para introducir la contraseña antigua.*/
    private final JPasswordField oldUserPassTextField = new JPasswordField();
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos permite cambiar la contraseña del usuario que la ha llamado.
     */
    public changePasswordView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Cambiar contraseña");

        JLabel mainTitle = new JLabel("Cambiar Contraseña", SwingConstants.CENTER);
        mainTitle.setBounds(76, 30, 360, 30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel userName = new JLabel("Usuario: user", SwingConstants.CENTER);
        userName.setText("Usuario: " + CtrlPresentacion.getUserName());
        userName.setBounds(166, 55, 180, 30);
        userName.setFont(new Font("Monospaced", Font.BOLD, 15));
        mainTitle.setForeground(Color.BLACK);
        add(userName);

        JButton backButton = new JButton("Volver");
        backButton.setBounds(166,340,180,30);
        backButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(backButton);

        JButton confirmButton = new JButton("Confirmar");
        confirmButton.setBounds(166,270,180,30);
        confirmButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(confirmButton);

        JLabel userPassLabel = new JLabel("Contraseña nueva:");
        userPassLabel.setBounds(97,170,200,30);
        userPassLabel.setForeground(Color.BLACK);
        add(userPassLabel);
        userPassTextField.setBounds(290,170,150,30);
        add(userPassTextField);

        JLabel userPassLabel2 = new JLabel("Repetir contraseña nueva:");
        userPassLabel2.setBounds(97,210,200,30);
        userPassLabel2.setForeground(Color.BLACK);
        add(userPassLabel2);
        userPassTextField2.setBounds(290,210,150,30);
        add(userPassTextField2);

        JLabel oldUserPassLabel = new JLabel("Contraseña actual:");
        oldUserPassLabel.setBounds(97,130,200,30);
        oldUserPassLabel.setForeground(Color.BLACK);
        add(oldUserPassLabel);
        oldUserPassTextField.setBounds(290,130,150,30);
        add(oldUserPassTextField);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener changePassListener = e -> {
            String p1 = String.valueOf(userPassTextField.getPassword());
            String p2 = String.valueOf(userPassTextField2.getPassword());
            String oldP = String.valueOf(oldUserPassTextField.getPassword());
            String user = CtrlPresentacion.getUserName();
            if(p1.equals(p2) && oldP.equals(CtrlPresentacion.getPassword(user))) {
                if(p1.length()<3) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(null, "La contraseña nueva debe ser más grande");
                }
                else {
                    CtrlPresentacion.cambiarContra(user, p1);
                    CtrlPresentacion.mostrarVistaMenuUsuario();
                    JOptionPane.showMessageDialog(null, "¡Contraseña cambiada!");
                    setVisible(false);
                    dispose();
                }
            }
            else if(!p1.equals(p2)){
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Contraseñas no coinciden." +
                        " Revise los dos cuadros de texto");
            }
            else {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "La contraseña actual no coincide con la introducida.");
            }
        };
        confirmButton.addActionListener(changePassListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener backListener = e -> {
            CtrlPresentacion.mostrarVistaEditUser();
            setVisible(false);
            dispose();
        };
        backButton.addActionListener(backListener);
    }
}

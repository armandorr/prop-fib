/** @file playingView.java
 @brief Especificación de la vista playingView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class playingView
 @brief Vista que se nos muestra cuando estamos jugando una partida.
 Podemos añadir valores a las casillas blancas, pedir pistas, pausar la partida, etc.
 También tenemos la opción de salir.

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista playingView.
 */
public class playingView extends JFrame
{

    /** @brief Valor para indicar que hemos parado el tiempo.*/
    boolean stop = false;
    /** @brief Valor que representa el tiempo.*/
    double tiempo = 0;
    /** @brief Boton que controla el sonido.*/
    private JButton soundButton = null;

    /** @brief Creadora por defecto
    \pre <em>kakSolved es la solución del Kakuro que se está jugando, time es el tiempo que llevamos jugando y numPistas es el número de pistas que hemos pedido hasta ahora</em>
    \post Hemos creado la vista que nos muestra la pantalla de pausa
     */
    public playingView(String[][] Kakuro, double time, String[][] KakuroSolved, int numPistas)
    {
        if(CtrlPresentacion.getIfKakuroCompleted()){ //si el kakuro no esta ya solucionado
            stop = true;
            if(CtrlPresentacion.getUserName() != null)
                JOptionPane.showMessageDialog(null,"Partida finalizada guardada en la carpeta " +CtrlPresentacion.getUserName()+"/partidasAcabadas/ con el nombre de su identificador: "+ CtrlPresentacion.getIDkakuro());
            CtrlPresentacion.mostrarVistaKakuroAcabado(Kakuro, tiempo);
        }
        else{
            tiempo = time;
            int N = Kakuro.length;
            int M = Kakuro[0].length;

            Container containerPrincipal = new Container();
            containerPrincipal.setLayout(new GridBagLayout());
            GridBagConstraints bagConstraints = new GridBagConstraints();

            JTextField[][] board = new JTextField[N][M];
            int cellSize = 60;

            Container kakuroContainer = new Container();
            kakuroContainer.setLayout(new GridLayout(N, M));

            for (int row = 0; row < N; ++row) {
                for (int col = 0; col < M; ++col) {
                    board[row][col] = new JTextField(); // Allocate element of array
                    kakuroContainer.add(board[row][col]);            // ContentPane adds JTextField
                    String stringTile = Kakuro[row][col];
                    board[row][col].setText(stringTile);
                    if (stringTile.equals("")) { //white tile empty
                        board[row][col].setEditable(true);
                        board[row][col].setBackground(Color.white);
                    } else if (stringTile.length() == 1) {
                        if (stringTile.equals("#")) {
                            board[row][col].setText("");
                            board[row][col].setEditable(false);
                            board[row][col].setBackground(Color.black);
                        } else {
                            board[row][col].setEditable(true);
                            board[row][col].setBackground(Color.white);
                        }
                    } else {
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                        board[row][col].setForeground(Color.white);
                    }

                    board[row][col].setHorizontalAlignment(JTextField.CENTER);
                    board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
                }
            }

            kakuroContainer.setMinimumSize(new Dimension(cellSize * M, cellSize * N));
            kakuroContainer.setPreferredSize(new Dimension(cellSize * M, cellSize * N));

            JLabel titleLabel = new JLabel("Kakuro a resolver");
            titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
            titleLabel.setForeground(Color.BLACK);
            titleLabel.setHorizontalAlignment(JTextField.CENTER);

            JLabel timerLabel = new JLabel();
            timerLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
            timerLabel.setForeground(Color.BLACK);
            timerLabel.setHorizontalAlignment(JTextField.CENTER);

            JButton hintButton = new JButton("Pista");
            hintButton.setHorizontalAlignment(JTextField.CENTER);

            JButton pauseButton = new JButton("Pausar");
            pauseButton.setHorizontalAlignment(JTextField.CENTER);

            JButton saveButton = new JButton("Guardar Partida");
            pauseButton.setHorizontalAlignment(JTextField.CENTER);

            JButton exitButton = new JButton("Salir");
            exitButton.setHorizontalAlignment(JTextField.CENTER);

            BufferedImage soundPicture = null;
            try {
                if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

            Container flowContainer = new Container();
            flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
            flowContainer.add(hintButton);
            if (CtrlPresentacion.getUserName() != null) flowContainer.add(saveButton);
            flowContainer.add(pauseButton);
            if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

            bagConstraints.fill = GridBagConstraints.RELATIVE;

            bagConstraints.gridy = 0;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            bagConstraints.gridy = 1;
            containerPrincipal.add(titleLabel, bagConstraints);

            bagConstraints.gridy = 2;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            bagConstraints.gridy = 3;
            containerPrincipal.add(kakuroContainer, bagConstraints);

            bagConstraints.gridy = 4;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            bagConstraints.gridy = 5;
            containerPrincipal.add(timerLabel, bagConstraints);

            bagConstraints.gridy = 6;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            bagConstraints.gridy = 7;
            containerPrincipal.add(flowContainer, bagConstraints);

            bagConstraints.gridy = 8;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            bagConstraints.gridy = 9;
            containerPrincipal.add(exitButton, bagConstraints);

            bagConstraints.gridy = 10;
            containerPrincipal.add(new JLabel(" "), bagConstraints);

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewportView(containerPrincipal);
            scrollPane.getViewport().setBackground(new Color(9, 175, 237));

            int tamRow = Math.min(820, Math.max(512, cellSize * (N + 5)));
            int tamCol = Math.min(820, Math.max(512, cellSize * (M + 5)));
            scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

            scrollPane.getVerticalScrollBar().setUnitIncrement(10);
            scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

            add(scrollPane);

            Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
            String sistema = System.getProperty("os.name").toLowerCase();
            sistema = sistema.substring(0, 3);
            setUndecorated(true);
            setResizable(false);
            if (sistema.equals("lin")){
                GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
                GraphicsDevice vc = env.getDefaultScreenDevice();
                vc.setFullScreenWindow(this);
            }
            else {
                setMinimumSize(screenUser);
                setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
            }

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setTitle("Jugar kakuro");

            setVisible(true);

            ActionListener hintListener = e -> {
                CtrlPresentacion.mostrarVistaPista(Kakuro, tiempo, KakuroSolved, numPistas);
                setVisible(false);
                dispose();
            };
            hintButton.addActionListener(hintListener);

            ActionListener pauseListener = e -> {
                CtrlPresentacion.pausarPartida(tiempo, KakuroSolved, numPistas);
                setVisible(false);
                dispose();
            };
            pauseButton.addActionListener(pauseListener);

            ActionListener saveListener = e -> {
                stop = true;
                String nombrePartida = CtrlPresentacion.getNombrePartida();
                boolean canSave = true;

                if (nombrePartida == null) {
                    String name = JOptionPane.showInputDialog("Que nombre quieres ponerle a la partida?");
                    if(name != null){
                        String user = CtrlPresentacion.getUserName();
                        if(CtrlPresentacion.noExisteEsteFichero(name + ".txt", "data/Users/" + user + "/partidasUser/")){
                            CtrlPresentacion.setNombrePartida(name);
                        }
                        else{
                            Toolkit.getDefaultToolkit().beep();
                            JOptionPane.showMessageDialog(null, "Ya existe un Kakuro guardado con ese nombre. Porfavor vuelva a intentarlo" );
                            canSave = false;
                        }
                    }
                    else canSave = false;
                }
                if(canSave) {
                    nombrePartida = CtrlPresentacion.getNombrePartida();
                    CtrlPresentacion.guardarPartida(tiempo);
                    JOptionPane.showMessageDialog(null, "Partida con nombre " + nombrePartida + " guardada con éxito");
                }
                stop = false;
            };
            saveButton.addActionListener(saveListener);

            ActionListener soundListener = e -> {
                startingView.pause_reanude();
                BufferedImage soundPicture1 = null;
                try {
                    if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                    else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
                } catch (Exception ignore){}
                soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
            };
            soundButton.addActionListener(soundListener);

            ActionListener exitListener = e -> {
                stop = true;
                int resposta;
                if(CtrlPresentacion.getUserName() == null)
                    resposta = JOptionPane.showConfirmDialog(null,"¿Estás seguro de querer salir?");
                else
                    resposta = JOptionPane.showConfirmDialog(null,"¿Estás seguro de querer salir? \nSi no has guardado la parida perderás el progreso actual");
                if(resposta == 0) {
                    CtrlPresentacion.mostrarVistaMenuUsuario();
                    setVisible(false);
                    dispose();
                }
                stop = false;
            };
            exitButton.addActionListener(exitListener);

            for (int row = 0; row < N; ++row) {
                for (int col = 0; col < M; ++col) {
                    board[row][col].getDocument().putProperty("row", row);
                    board[row][col].getDocument().putProperty("col", col);
                    board[row][col].getDocument().addDocumentListener(new DocumentListener() {
                        @Override
                        public void insertUpdate(DocumentEvent e)
                        {
                            int rowSelected = (int) e.getDocument().getProperty("row");
                            int colSelected = (int) e.getDocument().getProperty("col");
                            String stringIn = board[rowSelected][colSelected].getText();
                            if(stringIn.equals("")) resetText(rowSelected,colSelected);
                            else inputListener(e);
                        }

                        @Override
                        public void removeUpdate(DocumentEvent e)
                        {
                            int rowSelected = (int) e.getDocument().getProperty("row");
                            int colSelected = (int) e.getDocument().getProperty("col");
                            String stringIn = board[rowSelected][colSelected].getText();
                            if(stringIn.equals("")) resetText(rowSelected,colSelected);
                            else inputListener(e);
                        }

                        @Override
                        public void changedUpdate(DocumentEvent e)
                        {
                            //dont used
                        }

                        private void resetText(int row, int col)
                        {
                            Runnable resetTextTile = () -> board[row][col].setText("");
                            Kakuro[row][col] = "";
                            CtrlPresentacion.ponerValorEnKakuro(row, col, 0);
                            SwingUtilities.invokeLater(resetTextTile);
                        }

                        public void inputListener(DocumentEvent e){

                                int rowSelected = (int) e.getDocument().getProperty("row");
                                int colSelected = (int) e.getDocument().getProperty("col");

                                String stringIn = board[rowSelected][colSelected].getText();
                                boolean found = false;
                                for (int i = 0; !found && i < stringIn.length(); ++i) {
                                    if (!Character.isDigit(stringIn.charAt(i))) {
                                        found = true;
                                    }
                                }
                                if (stringIn.length() == 0 || found || Integer.parseInt(stringIn) == 0) {
                                    Toolkit.getDefaultToolkit().beep();
                                    JOptionPane.showMessageDialog(null, "Debes introducir un número entre el 1 y el 9");
                                    resetText(rowSelected,colSelected);
                                }
                                else {
                                    int valueIn = Integer.parseInt(stringIn);
                                    String[][] kak = CtrlPresentacion.ponerValorEnKakuro(rowSelected, colSelected, valueIn);
                                    if (kak.length == 1) {
                                        String res = kak[0][0];
                                        if("1".equals(res) || "4".equals(res) || "5".equals(res) || "6".equals(res) || "7".equals(res))
                                        {
                                            Toolkit.getDefaultToolkit().beep();
                                            switch (res) {
                                                case "1":
                                                    JOptionPane.showMessageDialog(null, "El valor introducido no es un valor válido");
                                                    break;
                                                case "4":
                                                    JOptionPane.showMessageDialog(null, "El valor introducido se repite en la fila " + (rowSelected+1));
                                                    break;
                                                case "5":
                                                    JOptionPane.showMessageDialog(null, "La suma de la fila " + (rowSelected+1) + " se ha completado pero no coincide");
                                                    break;
                                                case "6":
                                                    JOptionPane.showMessageDialog(null, "El valor introducido se repite en la columna " + (colSelected+1));
                                                    break;
                                                case "7":
                                                    JOptionPane.showMessageDialog(null, "La suma de la columna " + (colSelected+1) + " se ha completado pero no coincide");
                                                    break;
                                            }
                                        }
                                        resetText(rowSelected,colSelected);
                                    } else {
                                        if (CtrlPresentacion.getIfKakuroCompleted()) {
                                            stop = true;
                                            if(CtrlPresentacion.getUserName() != null)
                                                if(CtrlPresentacion.getIDkakuro()!=-1) JOptionPane.showMessageDialog(null,"Partida finalizada guardada en la carpeta " +CtrlPresentacion.getUserName()+"/partidasAcabadas/ con el nombre de su identificador: "+ CtrlPresentacion.getIDkakuro());
                                                else JOptionPane.showMessageDialog(null,"Partida finalizada guardada en la carpeta " +CtrlPresentacion.getUserName()+"/partidasAcabadas/ con el nombre de su identificador: "+ CtrlPresentacion.nextKakuroID());

                                            CtrlPresentacion.mostrarVistaKakuroAcabado(kak, tiempo);
                                            setVisible(false);
                                            dispose();
                                        }
                                        else{
                                            if(valueIn != 0) Kakuro[rowSelected][colSelected] = String.valueOf(valueIn);
                                            else Kakuro[rowSelected][colSelected] = "";
                                        }
                                    }
                                }
                            }

                    });
                }
            }

            Timer timer = new Timer(100, e -> {
                if ((int) tiempo < 60) timerLabel.setText("Tiempo: " + (float) tiempo + "s");
                else {
                    double tiempoAux = tiempo - (59.99) * (int) (tiempo / 60);
                    timerLabel.setText("Tiempo: " + (int) tiempo / 60 + "m " + (float) tiempoAux + "s");
                }
                if (!stop) tiempo += 0.1;
            });
            timer.start();
        }
    }
}

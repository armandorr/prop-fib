/** @file hintView.java
 @brief Especificación de la vista hintView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class hintView
 @brief Vista que nos sirve para pedir una pista. Se nos pausa el timer de la partida y se nos abre una nueva vista que nos da la opción de
 hacer clic en una casilla blanca vacía y nos revela qué número va en esa casilla. Añade tiempo al timer para compensar el hecho de haber recibido una pista

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista hintView
 */

public class hintView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>Kakuro contiene la representación del Kakuro que se está jugando, KakuroSolved es la solución de dicho Kakuro, time contiene el tiempo de la partida y numPistas es el número de pistas pedidas hasta ahora</em>
    \post Hemos creado la vista que nos muestra el menú para pedir una pista
     */
    public hintView(String[][] Kakuro, double time, String[][] KakuroSolved, int numPistas)
    {
        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JButton[][] board = new JButton[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row= 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JButton();
                kakuroContainer.add(board[row][col]);
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) {
                    board[row][col].setEnabled(true);
                    board[row][col].setBackground(Color.white);
                }
                else if(stringTile.length() == 1){
                    if (stringTile.equals("#")){
                        board[row][col].setText("");
                        board[row][col].setEnabled(false);
                        board[row][col].setBackground(Color.black);
                    }
                    else{
                        board[row][col].setEnabled(false);
                        board[row][col].setBackground(Color.white);
                    }
                }
                else {
                    board[row][col].setText("");
                    board[row][col].setEnabled(false);
                    board[row][col].setBackground(Color.black);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 25));
            }
        }

        kakuroContainer.setPreferredSize(new Dimension(cellSize*M, cellSize*N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize*M, cellSize*N));

        JLabel titleLabel = new JLabel("Elige una casilla blanca para obtener una pista");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 17));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JLabel hintLabel = new JLabel("Obtener una pista penalizará tu tiempo en "+(10+((numPistas+1)*2))+" segundos");
        hintLabel.setFont(new Font("Monospaced", Font.BOLD, 17));
        hintLabel.setForeground(Color.BLACK);
        hintLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Cancelar");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;

        bagConstraints.gridy = 0;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(titleLabel,bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(hintLabel,bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(kakuroContainer,bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 6;
        containerPrincipal.add(flowContainer,bagConstraints);

        bagConstraints.gridy = 7;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);

        int tamRow = Math.min(820,Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820,Math.max(512, cellSize * (M + 5)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        add(scrollPane);

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Handle window closing
        setTitle("Escoger pista");
        setVisible(true);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaJugarKakuro(Kakuro, time, KakuroSolved,numPistas);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);

        ActionListener pressListener = e -> {
            int rowSelected = -1;
            int colSelected = -1;
            JButton source = (JButton) e.getSource();
            boolean found = false;
            for (int row = 0; row < N && !found; ++row) {
                for (int col = 0; col < M && !found; ++col) {
                    if (board[row][col] == source) {
                        rowSelected = row;
                        colSelected = col;
                        found = true;
                    }
                }
            }
            if(found) {
                int valueIn = Integer.parseInt(KakuroSolved[rowSelected][colSelected]);
                String[][] kak = CtrlPresentacion.ponerValorEnKakuro(rowSelected, colSelected,valueIn);
                if(kak.length != 1) {
                    if(CtrlPresentacion.getIfKakuroCompleted()) {
                        JOptionPane.showMessageDialog(null,"Partida finalizada guardada en la carpeta " +CtrlPresentacion.getUserName()+"/partidasAcabadas/ con el nombre de su identificador: "+ CtrlPresentacion.getIDkakuro());
                        CtrlPresentacion.mostrarVistaKakuroAcabado(kak, time+10);
                    }
                    else{
                        int newNumPistas = numPistas + 1;
                        CtrlPresentacion.mostrarVistaJugarKakuro(kak,time+10+(numPistas*2),KakuroSolved,newNumPistas);
                    }
                    setVisible(false);
                    dispose();
                }
                else{
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(null, "Algun valor del Kakuro conflicta con el valor solución de la casilla indicada");
                }
            }
        };

        for (int row = 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col].addActionListener(pressListener);
            }
        }
    }
}

/** @file verifyAndSolveView.java
 @brief Especificación de la vista verifyAndSolveView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/** @class verifyAndSolveView
 @brief Vista que nos sirve para dos funcionalidades. Se encarga de mostrarnos el kakuro elegido en resolver o verificar.
 Una vez mostrado el kakuro, nos da la opción de probar la funcionalidad en el kakuro en cuestión, o cancelar para volver a
 la vista anterior.

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista verifyAndSolveView
 */
public class verifyAndSolveView extends JFrame
{

    /** @brief Bool que nos srive para saber si el botton para probar la funcionalidad ha sido pulsado o no*/
    boolean pulsado = false;
    /** @brief Valor usado para la animación en la pantalla de carga*/
    int estado = 0;
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista que muestra el menú intermediario para probar las funcionalidades de resolver
     * y verificar. Si el parametro op vale 1, estamos probando la funcionalidad verificar. En caso contrario, estamos probando
     * la funcionalidad resolver. El parámetro nombre indica el nombre del kakuro elegido. Y el booleano manual indica si hemos
     * introducido el kakuro manualmente o mediante otro medio.
    \pre <em>El parámetro op ha de valer 1 o 2</em>
    \post Hemos creado la vista que nos muestra el menú intermediario que nos muestra el kakuro en el que queremos probar la
     funcionalidad.
     */
    public verifyAndSolveView(int op, String nombre, boolean manual)
    {
        String[][] Kakuro = CtrlPresentacion.getKakuroStringMatrixFormat();
        SwingWorker<Boolean,Integer> sw = new solutionWorker();
        SwingWorker<Integer,Integer> vw = new verifyWorker();
        CtrlPresentacion.cargarKakuroDeMatrixString(Kakuro);

        if(op == 1) vw.execute();
        else if(op == 2) sw.execute();

        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row= 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField(); // Allocate element of array
                kakuroContainer.add(board[row][col]);            // ContentPane adds JTextField
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                }
                else if(stringTile.length() == 1){
                    if (stringTile.equals("#")){
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    }
                    else{
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                }
                else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        kakuroContainer.setPreferredSize(new Dimension(cellSize*M, cellSize*N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize*M, cellSize*N));

        JLabel titleLabel = new JLabel("Kakuro a verificar");
        if (op == 2) titleLabel = new JLabel("Kakuro a resolver");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton verifyorSolveButton = new JButton("Verificar");
        verifyorSolveButton.setHorizontalAlignment(JTextField.CENTER);
        if (op == 2) verifyorSolveButton = new JButton("Resolver");
        JButton exitButton = new JButton("Volver");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT,40,5));
        flowContainer.add(verifyorSolveButton);
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;

        bagConstraints.gridy = 0;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(titleLabel,bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(kakuroContainer,bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(flowContainer,bagConstraints);

        bagConstraints.gridy = 6;
        containerPrincipal.add(new JLabel(" "),bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        int tamRow = Math.min(820,Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820,Math.max(512, cellSize * (M + 5)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        JLabel loadingLabel = new JLabel("",JTextField.CENTER);
        if(op == 1) loadingLabel.setText("Verificando Kakuro");
        else if (op == 2) loadingLabel.setText("Solucionando Kakuro");
        loadingLabel.setBounds(screenUser.width/2-tamCol/2,screenUser.height/2-50/2,tamCol,50);
        loadingLabel.setFont(new Font("Monospaced", Font.BOLD, 30));
        loadingLabel.setForeground(Color.BLACK);
        loadingLabel.setVisible(false);
        add(loadingLabel);

        JButton cancelButton = new JButton("Cancelar");
        cancelButton.setBounds(screenUser.width/2-150/2,tamRow/2+150,150,50);
        cancelButton.setFont(new Font("Monospaced", Font.BOLD, 20));
        cancelButton.setVisible(false);
        add(cancelButton);

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Handle window closing
        if (op == 1) setTitle("Verificar Kakuro");
        else if (op == 2) setTitle("Resolver Kakuro");

        setVisible(true);

        ActionListener verifyorSolveListener = e -> {
            if (op == 1) {
                CtrlPresentacion.setNombrePartida(nombre);
                if (vw.isDone()) {
                    int numSols = -1;
                    try {
                        numSols = vw.get();
                    } catch (InterruptedException | ExecutionException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    CtrlPresentacion.mostrarVistaKakuroVerificado(numSols,manual,Kakuro);
                    setVisible(false);
                    dispose();
                } else {
                    pulsado = true;
                    scrollPane.setVisible(false);
                    loadingLabel.setVisible(true);
                    cancelButton.setVisible(true);
                    getContentPane().setBackground(new Color(9, 175, 237));
                }
            }
            else if(op == 2){
                boolean hasSol = false;
                if(sw.isDone()){
                    try {
                        hasSol = sw.get();
                    } catch (InterruptedException | ExecutionException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    CtrlPresentacion.mostrarVistaKakuroSolucionado(hasSol);
                    setVisible(false);
                    dispose();
                }
                else {
                    pulsado = true;
                    scrollPane.setVisible(false);
                    loadingLabel.setVisible(true);
                    cancelButton.setVisible(true);
                    getContentPane().setBackground(new Color(9, 175, 237));
                }
            }
        };
        verifyorSolveButton.addActionListener(verifyorSolveListener);

        ActionListener exitListener = e -> {
            if (op == 1) CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(1);
            else if (op == 2) CtrlPresentacion.mostrarVistaRepositorioOManualKakuro(2);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            pulsado=false;
            try{
                if(op==1) vw.cancel(true);
                else if (op==2) sw.cancel(true);
            }catch (Exception ignore){
                //do nothing, ignore
            }
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);

        Timer timer = new Timer(1000, e -> {
            if(pulsado){
                if (op == 1) {
                    if(estado == 0) loadingLabel.setText("Verificando Kakuro");
                    if(estado == 1) loadingLabel.setText("Verificando Kakuro.");
                    if(estado == 2) loadingLabel.setText("Verificando Kakuro..");
                    if(estado == 3) loadingLabel.setText("Verificando Kakuro...");
                    if(++estado == 4) estado = 0;
                    if (vw.isDone()) {
                        int numSols = -1;
                        try {
                            numSols = vw.get();
                        } catch (InterruptedException | ExecutionException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                        CtrlPresentacion.mostrarVistaKakuroVerificado(numSols,manual,Kakuro);
                        pulsado = false;
                        setVisible(false);
                        dispose();
                    }
                }
                else if(op == 2) {
                    if(estado == 0) loadingLabel.setText("Solucionando Kakuro");
                    if(estado == 1) loadingLabel.setText("Solucionando Kakuro.");
                    if(estado == 2) loadingLabel.setText("Solucionando Kakuro..");
                    if(estado == 3) loadingLabel.setText("Solucionando Kakuro...");
                    if(++estado == 4) estado = 0;
                    boolean hasSol = false;
                    if (sw.isDone()) {
                        try {
                            hasSol = sw.get();
                        } catch (InterruptedException | ExecutionException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                        CtrlPresentacion.mostrarVistaKakuroSolucionado(hasSol);
                        pulsado = false;
                        setVisible(false);
                        dispose();
                    }
                }
            }
        });
        timer.start();
    }

    /** @class solutionWorker
     @brief Worker creado para obtener si un Kakuro tiene solución en paralelo
     Contiene la función que realiza dicha tarea.
     */
    static class solutionWorker extends SwingWorker<Boolean, Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a resolver
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna si el kakuro tiene solución, si la tiene esta disponible en el Kakuro de dominio
         */
        protected Boolean doInBackground()
        {
            return CtrlPresentacion.getHasSolutionKakuro();
        }
    }

    /** @class verifyWorker
     @brief Worker creado para verificar un Kakuro en paralelo
     Contiene la función que realiza dicha tarea.
     */
    static class verifyWorker extends SwingWorker<Integer, Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a verificar
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna el numero de soluciones del kakuro
         */
        protected Integer doInBackground()
        {
            return CtrlPresentacion.getVerificationKakuro();
        }
    }
}

/** @file pauseView.java
 @brief Especificación de la vista pauseView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class pauseView
 @brief Vista que se nos muestra al pausar una partida. El timer de la partida deja de incrementar y no se nos muestra el tablero.
 Se nos dan dos botones, uno para reanudar la partida y seguir jugando y otro para salir al menú principal

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista generateOrLoadView
 */

public class pauseView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>kakSolved es la solución del Kakuro que se está jugando, time es el tiempo que llevamos jugando y numPistas es el número de pistas que hemos pedido hasta ahora</em>
    \post Hemos creado la vista que nos muestra la pantalla de pausa
     */
    public pauseView(double time, String[][] kakSolved, int numPistas)
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        getContentPane().setBackground(new Color(9, 175, 237));

        JLabel titleLabel = new JLabel("Juego Pausado");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton reanudeButton = new JButton("Reanudar");
        reanudeButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT,40,5));
        flowContainer.add(reanudeButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;
        bagConstraints.gridy = 0;
        add(titleLabel, bagConstraints);

        bagConstraints.gridy = 1;
        add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 2;
        add(flowContainer, bagConstraints);

        bagConstraints.gridy = 3;
        add(new JLabel(" "), bagConstraints);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Partida pausada");

        setVisible(true);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener reanudeListener = e -> {
            CtrlPresentacion.reanudarPartida(time,kakSolved,numPistas);
            setVisible(false);
            dispose();
        };
        reanudeButton.addActionListener(reanudeListener);
    }
}

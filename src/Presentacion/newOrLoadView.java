/** @file newOrLoadView.java
 @brief Especificación de la vista newOrLoadView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class newOrLoadView
 @brief Vista que nos sirve como menú intermedio a la hora de escoger qué Kakuro jugar. Hay dos botones que nos dejan elegir entre jugar un Kakuro nuevo o cargar partida (si no somos invitado)

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista newOrLoadView
 */
public class newOrLoadView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista que sirve como menú intermedio para escoger jugar un Kakuro nuevo o cargar partida
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos muestra las dos opciones (si somos usuario loggeado) o solo la de jugar Kakuro nuevo si somos invitados
     */
    public newOrLoadView(boolean invitado)
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Partida nueva o empezada");

        JLabel mainTitle = new JLabel("Escoge la opción para elegir el kakuro", SwingConstants.CENTER);
        mainTitle.setBounds(0,70,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JButton newButton = new JButton("Jugar Kakuro nuevo");
        newButton.setBounds(140, 150, 220, 30);
        add(newButton);

        JButton loadStartedButton = new JButton("Jugar Kakuro empezado");
        JButton exitButton = new JButton("Volver");

        if (!invitado) {
            loadStartedButton.setBounds(140, 190, 220, 30);
            add(loadStartedButton);
            exitButton.setBounds(140, 250, 220, 30);
        }
        else {
            exitButton.setBounds(140, 220, 220, 30);
        }
        add(exitButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener newListener = e -> {
            CtrlPresentacion.mostrarVistaGenerarOCargarKakuro();
            setVisible(false);
            dispose();
        };
        newButton.addActionListener(newListener);

        ActionListener startedListener = e -> {
            String username = CtrlPresentacion.getUserName();
            String userDir = System.getProperty("user.dir");
            userDir += "/data/Users/"+username+"/partidasUser";
            JFileChooser chooser = new JFileChooser(userDir);
            chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File arxiu = chooser.getSelectedFile();
                String nombre = arxiu.getName();
                nombre = nombre.replace(".txt", "");
                CtrlPresentacion.setNombrePartida(nombre);
                CtrlPresentacion.cargarKakuroDeArchivo(arxiu.getAbsolutePath());
                Integer tiempo = CtrlPresentacion.getTiempoKakuro(arxiu.getAbsolutePath());
                CtrlPresentacion.mostrarVistaKakuroElegido(userDir, tiempo/1000);
                setVisible(false);
                dispose();
            }
        };
        loadStartedButton.addActionListener(startedListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

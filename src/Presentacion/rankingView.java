/** @file rankingView.java
 @brief Especificación de la vista rankingView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class rankingView
 @brief Vista que se nos muestra el ranking.
 Podemos ver, ordenados de mayor a menor, los nombres de los jugadores con su respectiva puntuación

 Hecha por David Marin

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista rankingView.
 */
public class rankingView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos muestra la pantalla donde se ve el ranking con todos los jugadores
     */
    public rankingView()
    {
        setSize(512, 512);
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Ranking");

        JLabel mainTitle = new JLabel("Ranking:", SwingConstants.CENTER);
        mainTitle.setBounds(166,50,180,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 25));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JButton returnButton = new JButton("Volver");
        returnButton.setBounds(166, 370, 180, 30);
        add(returnButton);

        String[] rank = CtrlPresentacion.getRanking().split("\n");

        DefaultListModel<String> listModel = new DefaultListModel<>();
        for(int i = 0; i<rank.length; ++i) {
            String[] par = rank[i].split(" ");
            String pos = Integer.toString(i+1);
            String elem = pos + ". " + par[0] + " ".repeat(Math.max(0, 25 - (pos.length() + 2 + par[0].length() + par[1].length()))) + par[1];
            listModel.addElement(elem);
        }

        JList<String> listTxt = new JList<>(listModel);
        listTxt.setFont(new Font("Monospaced", Font.BOLD, 20));

        JScrollPane scrollPane = new JScrollPane(listTxt);
        scrollPane.setBounds(96,95,320,256);

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener returnListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        returnButton.addActionListener(returnListener);
    }
}

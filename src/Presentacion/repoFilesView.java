/** @file repoFilesView.java
 @brief Especificación de la vista repoFilesView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class repoFilesView
 @brief Vista que se nos muestra el repositorio de kakuros.
 Podemos ver todos los ficheros de distintas carpetas que hemos seleccionado y pulsar sobre ellos para ver los kakuros

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista repoFilesView.
 */
public class repoFilesView extends JFrame
{

    /** @brief Lista de ficheros del repositorio.*/
    private final DefaultListModel<String> listModel = new DefaultListModel<>();
    /** @brief Envoltorio de la lista listModel.*/
    private final JList<String> listTxt = new JList<>(listModel);
    /** @brief Vector con los nombres de los ficheros.*/
    private final String[] nombres;
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em> 1 <= operacion <= 3 </em>
    \post Hemos creado la vista que nos muestra la pantalla donde se ve el repositorio de kakuros
     */
    public repoFilesView(int op) {
        JLabel mainTitle = new JLabel("Kakuros empezados:",SwingConstants.CENTER);
        setTitle("Kakuros empezados");
        if (op == 2) {
            mainTitle =  new JLabel("Kakuros acabados:",SwingConstants.CENTER);
            setTitle("Kakuros acabados");
        }
        else if (op == 3) {
            mainTitle =  new JLabel("Kakuros del repositorio:",SwingConstants.CENTER);
            setTitle("Kakuros del repositorio");
        }
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);

        mainTitle.setBounds(6,30,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 25));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel subtTitle = new JLabel("Haga click en el Kakuro que desee visualizar",SwingConstants.CENTER);
        subtTitle.setBounds(0,60,512,30);
        subtTitle.setFont(new Font("Monospaced", Font.BOLD, 15));
        subtTitle.setForeground(Color.BLACK);
        add(subtTitle);

        JButton returnButton = new JButton("Volver");
        returnButton.setBounds(166, 410, 180, 30);
        add(returnButton);

        listTxt.setSelectionMode(0);
        JScrollPane scrollPane = new JScrollPane(listTxt);
        nombres = CtrlPresentacion.getKakurosNames(op);
        if (nombres != null) {
            if (op == 1 || op == 2) {
                for (String nombre : nombres) {
                    String nom = nombre.replace(".txt", "");
                    nom = nom.replace(".sol", "");
                    StringBuilder elem = new StringBuilder(" " + nom);
                    String carpeta = "/partidasUser/";
                    if (op == 2) carpeta = "/partidasAcabadas/";
                    int tiempo = CtrlPresentacion.getTiempoKakuro("data/Users/" + CtrlPresentacion.getUserName() + carpeta + nombre);
                    tiempo /= 1000;
                    int segundos = tiempo%60;
                    int minutos = tiempo/60;
                    String segundosString = Integer.toString(segundos);
                    String minutosString = Integer.toString(minutos);
                    String tiempoCompleto = minutosString+"m "+segundosString+"s";
                    int tam = 32 - (nom.length() + tiempoCompleto.length() + 1);
                    elem.append(" ".repeat(Math.max(0, tam)));
                    elem.append(tiempoCompleto);
                    listModel.addElement(elem.toString());
                }
            } else if (op == 3) {
                for (String nombre : nombres) {
                    String elem = " " + nombre;
                    listModel.addElement(elem);
                }
            }
        }
        listTxt.setFont(new Font("Monospaced", Font.BOLD, 20));
        scrollPane.setBounds(60, 95, 400, 300);

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        mainTitle.setForeground(Color.BLACK);
        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        listTxt.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int choosed = listTxt.getSelectedIndex();
                String camino;
                if (op == 1) camino = "data/Users/" + CtrlPresentacion.getUserName() + "/partidasUser/" + Objects.requireNonNull(nombres)[choosed];
                else if (op == 2) camino = "data/Users/" + CtrlPresentacion.getUserName() + "/partidasAcabadas/" + Objects.requireNonNull(nombres)[choosed];
                else camino = "data/Repositorio/" + Objects.requireNonNull(nombres)[choosed];
                String nombrePartida = Objects.requireNonNull(nombres)[choosed];
                CtrlPresentacion.setNombrePartida(nombrePartida);
                CtrlPresentacion.cargarKakuroDeArchivo(camino);
                CtrlPresentacion.mostrarVistaInfoKakuro(camino, op, Objects.requireNonNull(nombres)[choosed]);
                setVisible(false);
                dispose();
            }
        });

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener returnListener = e -> {
            if (op == 1 || op == 2) CtrlPresentacion.mostrarVistaRepositorioPersonal();
            else CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        returnButton.addActionListener(returnListener);
    }
}
/** @file playRepoChoosedOptionsView.java
 @brief Especificación de la vista playRepoChoosedOptionsView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/** @class playRepoChoosedOptionsView
 @brief Vista que se nos muestra al escoger un kakuro.
 Podemos decidir si jugar el Kakuro escogido mostrado o escoger otro
 También tenemos la opción de salir.

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista playRepoChoosedOptionsView.
 */
public class playRepoChoosedOptionsView extends JFrame
{

    /** @brief Kakuro que serà solucionado.*/
    String[][] KakuroClass;
    /** @brief Kakuro original que jugaremos.*/
    String[][] Kakuro;
    /** @brief Bool que indica si hemos cancelada la carga de la partida.*/
    boolean pulsado = false;
    /** @brief Valor usado para la animación en la pantalla de carga*/
    int estado = 0;
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>path es un path válido que existe, tiempo es el tiempo que llevamos jugando</em>
    \post Hemos creado la vista que nos muestra la pantalla donde se ve el kakuro escogio
     */
    public playRepoChoosedOptionsView(String path, Integer tiempo)
    {
        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        Kakuro = CtrlPresentacion.getKakuroStringMatrixFormat();
        SwingWorker<String[][],Integer> sw = new solutionWorker();
        setResizable(false);
        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        String[][] KakWithNoWhitesValues = new String[N][M];

        for (int row = 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField();
                kakuroContainer.add(board[row][col]);
                String stringTile = Kakuro[row][col];
                KakWithNoWhitesValues[row][col] = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                } else if (stringTile.length() == 1) {
                    if (stringTile.equals("#")) {
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    } else {
                        KakWithNoWhitesValues[row][col] = "";
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                } else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        KakuroClass = KakWithNoWhitesValues;
        sw.execute();

        kakuroContainer.setPreferredSize(new Dimension(cellSize * M, cellSize * N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize * M, cellSize * N));

        JLabel titleLabel = new JLabel("Kakuro escogido");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton playButton = new JButton("Jugar Kakuro escogido");
        playButton.setHorizontalAlignment(JTextField.CENTER);

        JButton chooseButton = new JButton("Escoger otro Kakuro");
        playButton.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Cancelar");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
        flowContainer.add(playButton);
        flowContainer.add(chooseButton);
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;
        bagConstraints.gridy = 0;
        containerPrincipal.add(titleLabel, bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(kakuroContainer, bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(flowContainer, bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        int tamRow = Math.min(820, Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820, Math.max(512, cellSize * (M + 8)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        JLabel loadingLabel = new JLabel("Preparando partida",JTextField.CENTER);
        loadingLabel.setBounds(screenUser.width/2-tamCol/2,screenUser.height/2-50/2,tamCol,50);
        loadingLabel.setFont(new Font("Monospaced", Font.BOLD, 30));
        loadingLabel.setForeground(Color.BLACK);
        loadingLabel.setVisible(false);
        add(loadingLabel);

        JButton cancelButton = new JButton("Cancelar");
        cancelButton.setBounds(screenUser.width/2-150/2,tamRow/2+150,150,50);
        cancelButton.setFont(new Font("Monospaced", Font.BOLD, 20));
        cancelButton.setVisible(false);
        add(cancelButton);

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Jugar kakuro seleccionado");
        setVisible(true);

        ActionListener playListener = e -> {
            String[][] KakuroSolved = null;
            if(sw.isDone()){
                try {
                    KakuroSolved = sw.get();
                } catch (InterruptedException | ExecutionException interruptedException) {
                    Toolkit.getDefaultToolkit().beep();
                    interruptedException.printStackTrace();
                }
                CtrlPresentacion.mostrarVistaJugarKakuro(Kakuro, tiempo, KakuroSolved, 0);
                setVisible(false);
                dispose();
            }
            else {
                pulsado = true;
                scrollPane.setVisible(false);
                loadingLabel.setVisible(true);
                cancelButton.setVisible(true);
                getContentPane().setBackground(new Color(9, 175, 237));
            }
        };
        playButton.addActionListener(playListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener chooseListener = e -> {
            JFileChooser chooser = new JFileChooser(path);
            chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File arxiu = chooser.getSelectedFile();
                CtrlPresentacion.cargarKakuroDeArchivo(arxiu.getAbsolutePath());
                int t = 0;
                if(tiempo != 0) {
                    t = CtrlPresentacion.getTiempoKakuro(arxiu.getAbsolutePath());
                }
                CtrlPresentacion.mostrarVistaKakuroElegido(path, t / 1000);
                setVisible(false);
                dispose();
            }
        };
        chooseButton.addActionListener(chooseListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaGenerarOCargarKakuro();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            pulsado = false;
            try {
                sw.cancel(true);
            } catch (Exception ignore) {
                //do nothing, ignore
            }
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);

        Timer timer = new Timer(1000, e -> {
            if (pulsado) {
                if(estado == 0) loadingLabel.setText("Preparando partida");
                if(estado == 1) loadingLabel.setText("Preparando partida.");
                if(estado == 2) loadingLabel.setText("Preparando partida..");
                if(estado == 3) loadingLabel.setText("Preparando partida...");
                if(++estado == 4) estado = 0;
                if (sw.isDone()) {
                    String[][] KakuroSolved = null;
                    try {
                        KakuroSolved = sw.get();
                    } catch (InterruptedException | ExecutionException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    CtrlPresentacion.mostrarVistaJugarKakuro(KakuroClass, 0, KakuroSolved, 0);
                    setVisible(false);
                    pulsado = false;
                    dispose();
                }
            }
        });
        timer.start();
    }

    /** @class solutionWorker
     @brief Worker creado para obtener la solución de un kakuro
     Contiene la función que realiza dicha tarea
     */
    class solutionWorker extends SwingWorker<String[][], Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a resolver
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna el Kakuro resuelto
         */
        protected String[][] doInBackground()
        {
            CtrlPresentacion.cargarKakuroDeMatrixString(KakuroClass);
            return CtrlPresentacion.getSolutionKakuro(Kakuro);
        }
    }
}

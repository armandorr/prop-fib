/** @file repoPersonalView.java
 @brief Especificación de la vista repoPersonalView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class repoPersonalView
 @brief Vista que se nos muestra el si queremos ver el repositorio personal de empezadas o acabadas.

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista repoPersonalView.
 */
public class repoPersonalView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em> cierto </em>
    \post Hemos creado la vista que nos muestra la pantalla donde elegir si ver el repositorio personal de empezadas o acabadas
     */
    public repoPersonalView()
    {
        setMinimumSize(new Dimension(512, 512));
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(null);
        JLabel titleLabel = new JLabel("¿Qué Kakuros quieres ver?");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton startedButton = new JButton("Kakuros empezados");
        startedButton.setHorizontalAlignment(JTextField.CENTER);

        JButton finishedButton = new JButton("Kakuros acabados");
        finishedButton.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Volver");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        titleLabel.setBounds(6, 50, 500, 30);
        add(titleLabel);

        startedButton.setBounds(166, 120, 180, 30);
        add(startedButton);

        finishedButton.setBounds(166, 160, 180, 30);
        add(finishedButton);

        exitButton.setBounds(166, 220, 180, 30);
        add(exitButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Repositorio personal");

        setVisible(true);

        ActionListener startedListener = e -> {
            CtrlPresentacion.mostrarVistaRepoFiles(1);
            setVisible(false);
            dispose();
        };
        startedButton.addActionListener(startedListener);

        ActionListener finishedListener = e -> {
            CtrlPresentacion.mostrarVistaRepoFiles(2);
            setVisible(false);
            dispose();
        };
        finishedButton.addActionListener(finishedListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

/** @file generatedKakuroView.java
 @brief Especificación de la vista generatedKakuroView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/** @class generatedKakuroView
 @brief Vista intermedia que nos sirve para acceder a las distintas funcionalidades que podemos experimentar con el kakuro recién
 generado. Estas funciones son verificar, jugar y solucionar.

 Hecha por Armando Rodriguez

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista generatedKakuroView.
 Además contiene 3 swing workers que ejecutan tareas en paralelo.
 */
public class generatedKakuroView extends JFrame
{

    /** @brief Kakuro de la clase que se usará para los workers.*/
    String[][] KakuroClass;
    /** @brief Bool que indica si hemos pulsado el botón de cancelar.*/
    boolean pulsado = false;
    /** @brief Valor usado para la animación en la pantalla de carga*/
    int estado = 0;
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Hemos creado la vista que nos permite acceder a la visualización del kakuro generador y nos da ciertas opciones.
     */
    public generatedKakuroView(int op,String[][] Kakuro)
    {
        SwingWorker<Integer,Integer> verifyWorker = new verifyWorker();
        SwingWorker<Boolean,Integer> solveWorker = new solveWorker();
        SwingWorker<String[][],Integer> playWorker = new playWorker();
        KakuroClass = Kakuro;

        if(op != 3) {
            CtrlPresentacion.cargarKakuroDeMatrixString(Kakuro);
            if (op == 1) verifyWorker.execute();
            else solveWorker.execute();
        }
        else playWorker.execute();

        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row= 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField(); // Allocate element of array
                kakuroContainer.add(board[row][col]);            // ContentPane adds JTextField
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                }
                else if(stringTile.length() == 1){
                    if (stringTile.equals("#")){
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    }
                    else{
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                }
                else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        kakuroContainer.setPreferredSize(new Dimension(cellSize*M, cellSize*N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize*M, cellSize*N));

        JLabel titleLabel = new JLabel("Kakuro Generado");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setHorizontalAlignment(JTextField.CENTER);
        titleLabel.setForeground(Color.BLACK);

        JButton veriSolvePlayButton = new JButton();
        if(op == 1) veriSolvePlayButton.setText("Verificar Kakuro generado");
        else if(op == 2) veriSolvePlayButton.setText("Solucionar Kakuro generado");
        else if(op == 3) veriSolvePlayButton.setText("Jugar Kakuro generado");
        veriSolvePlayButton.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Volver");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT,40,5));
        flowContainer.add(veriSolvePlayButton);
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;
        bagConstraints.gridy = 0;
        containerPrincipal.add(titleLabel, bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(kakuroContainer, bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(flowContainer, bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);

        int tamRow = Math.min(820,Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820,Math.max(512, cellSize * (M + 5)));

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        JLabel loadingLabel = new JLabel("",JTextField.CENTER);
        if(op == 1) loadingLabel.setText("Verificando Kakuro");
        else if(op == 2) loadingLabel.setText("Solucionando Kakuro");
        else if(op == 3) loadingLabel.setText("Preparando partida");
        loadingLabel.setBounds(0,tamRow/2-50,tamCol,50);
        loadingLabel.setFont(new Font("Monospaced", Font.BOLD, 30));
        loadingLabel.setVisible(false);
        add(loadingLabel);

        JButton cancelButton = new JButton("Cancelar");
        cancelButton.setBounds(tamCol/2-75,tamRow/2+50,150,50);
        cancelButton.setFont(new Font("Monospaced", Font.BOLD, 20));
        cancelButton.setVisible(false);
        add(cancelButton);

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        add(scrollPane);

        setMinimumSize(new Dimension(tamCol, tamRow));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Handle window closing
        setTitle("Kakuro generado");
        setVisible(true);

        ActionListener veriSolveGenListener = e -> {
            try {
                if (op == 1) {
                    if (verifyWorker.isDone()) {
                        int numSols = verifyWorker.get();
                        CtrlPresentacion.mostrarVistaKakuroVerificado(numSols, false, Kakuro);
                        setVisible(false);
                        dispose();
                    } else {
                        pulsado = true;
                        scrollPane.setVisible(false);
                        loadingLabel.setVisible(true);
                        cancelButton.setVisible(true);
                        getContentPane().setBackground(new Color(9, 175, 237));
                    }
                } else if (op == 2) {
                    if (solveWorker.isDone()) {
                        boolean hasSol = solveWorker.get();
                        CtrlPresentacion.mostrarVistaKakuroSolucionado(hasSol);
                        setVisible(false);
                        dispose();
                    } else {
                        pulsado = true;
                        scrollPane.setVisible(false);
                        loadingLabel.setVisible(true);
                        cancelButton.setVisible(true);
                        getContentPane().setBackground(new Color(9, 175, 237));
                    }
                } else if (op == 3) {
                    if (playWorker.isDone()) {
                        String[][] KakuroSolved = playWorker.get();
                        CtrlPresentacion.mostrarVistaJugarKakuro(KakuroClass, 0, KakuroSolved, 0);
                        setVisible(false);
                        dispose();
                    } else {
                        pulsado = true;
                        scrollPane.setVisible(false);
                        loadingLabel.setVisible(true);
                        cancelButton.setVisible(true);
                        getContentPane().setBackground(new Color(9, 175, 237));
                    }
                }
            }catch (InterruptedException | ExecutionException interruptedException) {
                interruptedException.printStackTrace();
            }
        };
        veriSolvePlayButton.addActionListener(veriSolveGenListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaOpcionesGenerar(op);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            pulsado=false;
            try{
                if(op == 1) verifyWorker.cancel(true);
                else if(op == 2) solveWorker.cancel(true);
                else playWorker.cancel(true);
            }catch (Exception ignore){
                //do nothing, ignore
            }
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);

        Timer timer = new Timer(1000, e -> {
            if(pulsado){
                try {
                    if (op == 1) {
                        if(estado == 0) loadingLabel.setText("Verificando Kakuro");
                        if(estado == 1) loadingLabel.setText("Verificando Kakuro.");
                        if(estado == 2) loadingLabel.setText("Verificando Kakuro..");
                        if(estado == 3) loadingLabel.setText("Verificando Kakuro...");
                        if(++estado == 4) estado = 0;
                        if (verifyWorker.isDone()) {
                            int numSols = verifyWorker.get();
                            CtrlPresentacion.mostrarVistaKakuroVerificado(numSols, false, Kakuro);
                            setVisible(false);
                            pulsado = false;
                            dispose();
                        }
                    }
                    else if (op == 2) {
                        if(estado == 0) loadingLabel.setText("Solucionando Kakuro");
                        if(estado == 1) loadingLabel.setText("Solucionando Kakuro.");
                        if(estado == 2) loadingLabel.setText("Solucionando Kakuro..");
                        if(estado == 3) loadingLabel.setText("Solucionando Kakuro...");
                        if(++estado == 4) estado = 0;
                        if (solveWorker.isDone()) {
                            boolean hasSol = solveWorker.get();
                            CtrlPresentacion.mostrarVistaKakuroSolucionado(hasSol);
                            setVisible(false);
                            pulsado = false;
                            dispose();
                        }
                    }
                    else if (op == 3) {
                        if(estado == 0) loadingLabel.setText("Preparando partida");
                        if(estado == 1) loadingLabel.setText("Preparando partida.");
                        if(estado == 2) loadingLabel.setText("Preparando partida..");
                        if(estado == 3) loadingLabel.setText("Preparando partida...");
                        if(++estado == 4) estado = 0;
                        if (playWorker.isDone()) {
                            String[][] KakuroSolved = playWorker.get();
                            CtrlPresentacion.mostrarVistaJugarKakuro(KakuroClass, 0, KakuroSolved, 0);
                            setVisible(false);
                            pulsado = false;
                            dispose();
                        }
                    }
                } catch (InterruptedException | ExecutionException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        });
        timer.start();
    }

    /** @class verifyWorker
     @brief Worker creado para verificar un Kakuro en paralelo
            Contiene la función que realiza dicha tarea.
     */
    static class verifyWorker extends SwingWorker<Integer, Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a verificar
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna el numero de soluciones del kakuro
         */
        protected Integer doInBackground()
        {
            return CtrlPresentacion.getVerificationKakuro();
        }
    }

    /** @class solveWorker
     @brief Worker creado para obtener si un Kakuro tiene solución en paralelo
            Contiene la función que realiza dicha tarea.
     */
    static class solveWorker extends SwingWorker<Boolean, Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a resolver
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna si el kakuro tiene solución, si la tiene esta disponible en el Kakuro de dominio
         */
        protected Boolean doInBackground()
        {
            return CtrlPresentacion.getHasSolutionKakuro();
        }
    }

    /** @class playWorker
     @brief Worker creado para obtener la solución de un Kakuro en paralelo
            Contiene la función que realiza dicha tarea.
     */
    class playWorker extends SwingWorker<String[][], Integer>
    {
        /** @brief Operación que se utiliza para llamar en segundo plano a resolver
        \pre <em>cierto</em>
        \post El worker ha acabado y retorna la solución del kakuro en string[][]
         */
        protected String[][] doInBackground()
        {
            return CtrlPresentacion.getSolutionKakuro(KakuroClass);
        }
    }
}

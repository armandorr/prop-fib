/** @file signUpView.java
 @brief Especificación de la vista signUpView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class signUpView
 @brief Vista dedicada a hacer signUp de un nuevo usuario

 Hecha por Marco Patiño

 Contiene diversos atributos que sirven para definir la vista. Solo contiene una creadora que se encarga de hacer el trabajo
 */
public class signUpView extends JFrame
{

    /** @brief JTextField que nos permite leer el nombre de usuario*/
    private final JTextField userNameTextField = new JTextField();
    /** @brief JTextField que nos permite leer la contraseña del usuario*/
    private final JPasswordField userPassTextField = new JPasswordField();
    /** @brief JTextField que nos permite confirmar la contraseña del usuario*/
    private final JPasswordField userPassTextField2 = new JPasswordField();
    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista para que el usuario pueda crearse una cuenta
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos permite crear una cuenta nueva en el sistema
     */
    public signUpView()
    {
        setMinimumSize(new Dimension(512,512));
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        setTitle("Crear cuenta");

        JLabel mainTitle = new JLabel("Crea una cuenta nueva", SwingConstants.CENTER);
        mainTitle.setBounds(6,50,500,30);
        mainTitle.setFont(new Font("Monospaced", Font.BOLD, 20));
        mainTitle.setForeground(Color.BLACK);
        add(mainTitle);

        JLabel userNameLabel = new JLabel("Usuario:");
        userNameLabel.setBounds(100,130,150,30);
        userNameLabel.setForeground(Color.BLACK);
        add(userNameLabel);
        userNameTextField.setBounds(270,130,150,30);
        add(userNameTextField);

        JLabel userPassLabel = new JLabel("Contraseña:");
        userPassLabel.setBounds(100,170,150,30);
        userPassLabel.setForeground(Color.BLACK);
        add(userPassLabel);
        userPassTextField.setBounds(270,170,150,30);
        add(userPassTextField);

        JLabel userPassLabel2 = new JLabel("Repetir contraseña:");
        userPassLabel2.setBounds(100,210,150,30);
        userPassLabel2.setForeground(Color.BLACK);
        add(userPassLabel2);
        userPassTextField2.setBounds(270,210,150,30);
        add(userPassTextField2);

        JButton signButton = new JButton("Crear cuenta");
        signButton.setBounds(130,290,135,30);
        signButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(signButton);

        JButton cancelButton = new JButton("Volver");
        cancelButton.setBounds(290,290,100,30);
        cancelButton.setFont(new Font("SansSerif", Font.BOLD, 12));
        add(cancelButton);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));
        soundButton.setBounds(410,410,40,40);
        if(startingView.getCanListenToMusic()) add(soundButton);

        BufferedImage myPicture = null;
        try { myPicture = ImageIO.read(new File("data/Media/background.jpg")); } catch (Exception ignore){}
        JLabel picLabel = new JLabel(new ImageIcon(Objects.requireNonNull(myPicture).getScaledInstance(512, 512, Image.SCALE_FAST)),SwingConstants.LEFT);
        picLabel.setBounds(0,0,512,512);
        add(picLabel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ActionListener signListener = e -> {
            String username = userNameTextField.getText();
            boolean validUsername = true;
            if (username == null || username.equals("")) validUsername = false;
            for (int i = 0; i< Objects.requireNonNull(username).length(); ++i)
                if (username.charAt(i) == ' ') {
                    validUsername = false;
                    break;
                }
            String password = String.valueOf(userPassTextField.getPassword());
            String password2 = String.valueOf(userPassTextField2.getPassword());
            if (!password.equals(password2)){
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "La confirmación de contraseña no coincide.");
            }
            else if (!validUsername){
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Por favor, introduce un nombre de usuario sin espacios");
            }
            else if (password.length() < 3){
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "La contraseña ha de tener al menos 3 caracteres");
            }
            else {
                if (CtrlPresentacion.crearCuenta(username, password)){
                    CtrlPresentacion.mostrarVistaMenuUsuario();
                    setVisible(false);
                    dispose();
                }
                else{
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(null, "Error: El usuario ya existe");
                }
            }
        };
        signButton.addActionListener(signListener);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener cancelListener = e -> {
            CtrlPresentacion.iniciarPresentacion(false);
            setVisible(false);
            dispose();
        };
        cancelButton.addActionListener(cancelListener);
    }
}

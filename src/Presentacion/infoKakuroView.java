/** @file infoKakuroView.java
 @brief Especificación de la vista infoKakuroView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class infoKakuroView
 @brief Vista que nos sirve para ver la información de un Kakuro. Nos muestra el Kakuro representado gráficamente, el nombre del archivo que lo cintiene
 y su dificultad. Además, hay dos botones: uno para salir y otro para guardar la representación oficial (sin parámetros extra) del Kakuro en el repositorio

 Hecha por Hasnain Shafqat

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista infoKakuroView
 */
public class infoKakuroView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto
    \pre <em>en camino tenemos un fichero de texto que contiene la representación de un Kakuro y todos los parámetros necesarios para ello, nombre es el nombre del Kakuro y op vale entre 1 y 3</em>
    \post Hemos creado la vista que nos muestra toda la información de un Kakuro. op=1 es para partidas en curso, op=2 es para partidas acabadas y op=3 es para Kakuros blancos (que no se están jugando)
     */
    public infoKakuroView(String camino, int op, String nombre)
    {
        String[][] Kakuro = CtrlPresentacion.getKakuroStringMatrixFormat();
        double tiempo = -1;
        int puntos = -1;
        if (op != 3) {
            tiempo = CtrlPresentacion.getTiempoKakuro(camino);
            tiempo /= 1000;
        }
        String difi = "Fácil";
        int dificultad = CtrlPresentacion.getDificultad();
        if (dificultad == 2) difi = "Mediana";
        else if (dificultad == 3) difi = "Difícil";
        if (op == 2) puntos = CtrlPresentacion.getPuntosKakuro(camino);
        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row = 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField(); // Allocate element of array
                kakuroContainer.add(board[row][col]);            // ContentPane adds JTextField
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                } else if (stringTile.length() == 1) {
                    if (stringTile.equals("#")) {
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    } else {
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                } else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }

                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        kakuroContainer.setMinimumSize(new Dimension(cellSize * M, cellSize * N));
        kakuroContainer.setPreferredSize(new Dimension(cellSize * M, cellSize * N));

        JLabel titleLabel = new JLabel("Información Kakuro");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JLabel nombreLabel = new JLabel(nombre);
        nombreLabel.setFont(new Font("Monospaced", Font.BOLD, 15));
        nombreLabel.setForeground(Color.BLACK);
        nombreLabel.setHorizontalAlignment(JTextField.CENTER);

        JLabel tiempoLabel = new JLabel("Tiempo: "+tiempo+" s");
        tiempoLabel.setFont(new Font("Monospaced", Font.BOLD, 13));
        tiempoLabel.setForeground(Color.BLACK);
        tiempoLabel.setHorizontalAlignment(JTextField.CENTER);

        JLabel dificultadLabel = new JLabel("Dificultad: "+difi);
        dificultadLabel.setFont(new Font("Monospaced", Font.BOLD, 13));
        dificultadLabel.setForeground(Color.BLACK);
        dificultadLabel.setHorizontalAlignment(JTextField.CENTER);

        JLabel puntosLabel = new JLabel("Puntos: "+puntos);
        puntosLabel.setFont(new Font("Monospaced", Font.BOLD, 13));
        puntosLabel.setForeground(Color.BLACK);
        puntosLabel.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainerInfo = new Container();
        flowContainerInfo.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
        if (tiempo != -1) flowContainerInfo.add(tiempoLabel);
        flowContainerInfo.add(dificultadLabel);
        if (puntos != -1)flowContainerInfo.add(puntosLabel);
        if(startingView.getCanListenToMusic()) flowContainerInfo.add(soundButton);

        JButton saveStandardButton = new JButton("Guardar en formato estándar");
        saveStandardButton.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Volver");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        Container flowContainerOptions = new Container();
        flowContainerOptions.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
        if(CtrlPresentacion.getUserName() != null) flowContainerOptions.add(saveStandardButton);
        flowContainerOptions.add(exitButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;

        bagConstraints.gridy = 0;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(titleLabel, bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(nombreLabel, bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(kakuroContainer, bagConstraints);

        bagConstraints.gridy = 6;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 7;
        containerPrincipal.add(flowContainerInfo, bagConstraints);

        bagConstraints.gridy = 8;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 9;
        containerPrincipal.add(flowContainerOptions, bagConstraints);

        bagConstraints.gridy = 10;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);

        int tamRow = Math.min(820, Math.max(512, cellSize * (N + 6)));
        int tamCol = Math.min(820, Math.max(512, cellSize * (M + 6)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        add(scrollPane);

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Información kakuro");
        setVisible(true);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener saveStandardListener = e -> {
            String path;
            if(op == 1) path = "data/Users/"+CtrlPresentacion.getUserName()+"/partidasUser/";
            else if(op == 2) path = "data/Users/"+CtrlPresentacion.getUserName()+"/partidasAcabadas/";
            else path = "data/Repositorio/";
            String nombrePartida = CtrlPresentacion.getNombrePartida();
            String extension = nombrePartida.substring(nombrePartida.length() - 4);
            nombrePartida = nombrePartida.replace(".txt","");
            nombrePartida = nombrePartida.replace(".sol","");
            if(extension.equals(".sol")) nombrePartida += "_SOL";
            CtrlPresentacion.setNombrePartida(nombrePartida);
            CtrlPresentacion.guardarKakuroFormatoStandard(path);
            JOptionPane.showMessageDialog(null,"El kakuro "+nombrePartida+".std se ha guardado en "+path);
        };
        saveStandardButton.addActionListener(saveStandardListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaRepoFiles(op);
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}

/** @file solveFinishedView.java
 @brief Especificación de la vista solveFinishedView
 */

package Presentacion;

import Dominio.Controladores.CtrlPresentacion;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;

/** @class solveFinishedView
 @brief Vista que nos muestra los resultados de Solve

 Hecha por Marco Patiño

 Contiene una creadora que se encarga de hacer las tareas para mostrar la vista
 */
public class solveFinishedView extends JFrame
{

    /** @brief Boton que controla el sonido.*/
    private final JButton soundButton;

    /** @brief Creadora por defecto crea la vista para observar los resultados de Solve. Recibe una parametro hasSol, si es
        true mostraremos la solución del kakuro, en caso contrario indicaremos que no tiene solución con el kakuro vacío
    \pre <em>Cierto</em>
    \post Hemos creado la vista que nos permite ver el kakuro resuelto
     */
    public solveFinishedView(boolean hasSol)
    {
        String[][] Kakuro = CtrlPresentacion.getKakuroStringMatrixFormat();
        int N = Kakuro.length;
        int M = Kakuro[0].length;

        Container containerPrincipal = new Container();
        containerPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints bagConstraints = new GridBagConstraints();

        JTextField[][] board = new JTextField[N][M];
        int cellSize = 60;

        Container kakuroContainer = new Container();
        kakuroContainer.setLayout(new GridLayout(N, M));

        for (int row= 0; row < N; ++row) {
            for (int col = 0; col < M; ++col) {
                board[row][col] = new JTextField(); // Allocate element of array
                kakuroContainer.add(board[row][col]);            // ContentPane adds JTextField
                String stringTile = Kakuro[row][col];
                board[row][col].setText(stringTile);
                if (stringTile.equals("")) { //white tile empty
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.white);
                }
                else if(stringTile.length() == 1){
                    if (stringTile.equals("#")){
                        board[row][col].setText("");
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.black);
                    }
                    else{
                        board[row][col].setEditable(false);
                        board[row][col].setBackground(Color.white);
                    }
                }
                else {
                    board[row][col].setEditable(false);
                    board[row][col].setBackground(Color.black);
                    board[row][col].setForeground(Color.white);
                }
                board[row][col].setHorizontalAlignment(JTextField.CENTER);
                board[row][col].setFont(new Font("Monospaced", Font.BOLD, 15));
            }
        }

        kakuroContainer.setPreferredSize(new Dimension(cellSize*M, cellSize*N));
        kakuroContainer.setMinimumSize(new Dimension(cellSize*M, cellSize*N));

        JLabel titleLabel;
        if(!hasSol) titleLabel = new JLabel("El Kakuro no tiene solución");
        else titleLabel = new JLabel("Kakuro Resuelto");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setHorizontalAlignment(JTextField.CENTER);

        JButton exitButton = new JButton("Volver al menu principal");
        exitButton.setHorizontalAlignment(JTextField.CENTER);

        BufferedImage soundPicture = null;
        try {
            if(startingView.getIfMuted()) soundPicture = ImageIO.read(new File("data/Media/muted.png"));
            else soundPicture = ImageIO.read(new File("data/Media/sound.png"));
        } catch (Exception ignore){}
        soundButton = new JButton(new ImageIcon(Objects.requireNonNull(soundPicture).getScaledInstance(30, 30, Image.SCALE_FAST)));

        Container flowContainer = new Container();
        flowContainer.setLayout(new FlowLayout(FlowLayout.LEFT, 40, 5));
        flowContainer.add(exitButton);
        if(startingView.getCanListenToMusic()) flowContainer.add(soundButton);

        bagConstraints.fill = GridBagConstraints.RELATIVE;
        bagConstraints.gridy = 0;
        containerPrincipal.add(titleLabel, bagConstraints);

        bagConstraints.gridy = 1;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 2;
        containerPrincipal.add(kakuroContainer, bagConstraints);

        bagConstraints.gridy = 3;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        bagConstraints.gridy = 4;
        containerPrincipal.add(flowContainer, bagConstraints);

        bagConstraints.gridy = 5;
        containerPrincipal.add(new JLabel(" "), bagConstraints);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(containerPrincipal);
        scrollPane.getViewport().setBackground(new Color(9, 175, 237));

        int tamRow = Math.min(820,Math.max(512, cellSize * (N + 5)));
        int tamCol = Math.min(820,Math.max(512, cellSize * (M + 5)));
        scrollPane.setPreferredSize(new Dimension(tamCol, tamRow));

        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);

        add(scrollPane);

        Dimension screenUser = Toolkit.getDefaultToolkit().getScreenSize();
        String sistema = System.getProperty("os.name").toLowerCase();
        sistema = sistema.substring(0, 3);
        setUndecorated(true);
        setResizable(false);
        if (sistema.equals("lin")){
            GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice vc = env.getDefaultScreenDevice();
            vc.setFullScreenWindow(this);
        }
        else {
            setMinimumSize(screenUser);
            setExtendedState(this.getExtendedState() | MAXIMIZED_BOTH);
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Handle window closing
        setTitle("Kakuro solved");
        setVisible(true);

        ActionListener soundListener = e -> {
            startingView.pause_reanude();
            BufferedImage soundPicture1 = null;
            try {
                if(startingView.getIfMuted()) soundPicture1 = ImageIO.read(new File("data/Media/muted.png"));
                else soundPicture1 = ImageIO.read(new File("data/Media/sound.png"));
            } catch (Exception ignore){}
            soundButton.setIcon(new ImageIcon(Objects.requireNonNull(soundPicture1).getScaledInstance(30, 30, Image.SCALE_FAST)));
        };
        soundButton.addActionListener(soundListener);

        ActionListener exitListener = e -> {
            CtrlPresentacion.mostrarVistaMenuUsuario();
            setVisible(false);
            dispose();
        };
        exitButton.addActionListener(exitListener);
    }
}
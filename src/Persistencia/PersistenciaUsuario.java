/** @file PersistenciaUsuario.java
 @brief Especificación de la clase PersistenciaUsuario
 */

package Persistencia;

import java.io.File;
import java.nio.file.Files;
import java.util.Objects;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

/** @class PersistenciaUsuario
    @brief Clase de persistencia relacionada con los usuarios

    Hecha por Hasnain Shafqat

    Contiene diversas funciones relacionadas con la obtencion de datos del Usuario
 */

public class PersistenciaUsuario
{

    /** @brief Creadora por defecto
    \pre <em>cierto</em>
    \post Crea una PersistenciaUsuario vacía
     */
    public PersistenciaUsuario()
    {

    }

    /** @brief Operación de crear un usuario nuevo
    \pre <em>pass y user no son vacíos</em>
    \post Si ya existe un usuario con nombre user, retorna falso. Sino, retorna cierto y crea todas las carpetas y datos pertinentes correspondientes al nuevo ususario
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public boolean creaUsuario(String user, String pass)
    {
        String[] pathnames;
        File file = new File("data/Users/");
        pathnames = file.list();
        for (String pathname : Objects.requireNonNull(pathnames)) {
            if (pathname.equals(user)) return false;
        }
        File userFolder = new File("data/Users/"+user);
        userFolder.mkdir();

        File gamesFolder = new File("data/Users/"+user+"/partidasUser");
        gamesFolder.mkdir();

        File gamesFinishedFolder = new File("data/Users/"+user+"/partidasAcabadas");
        gamesFinishedFolder.mkdir();

        File infoFolder = new File("data/Users/"+user+"/infoUser");
        infoFolder.mkdir();

        try {
            File infoDocument = new File("data/Users/" + user + "/infoUser/info.txt");
            infoDocument.createNewFile();

            FileWriter fileWriterInfo = new FileWriter("data/Users/" + user + "/infoUser/info.txt");
            fileWriterInfo.write(pass);
            fileWriterInfo.close();

            File fileEstado = new File("data/estado.txt");
            Scanner scan = new Scanner(fileEstado);
            StringBuilder contenido = new StringBuilder();
            while (scan.hasNextLine()) {
                contenido.append(scan.nextLine());
                if (scan.hasNextLine()) contenido.append("\n");
            }
            contenido.append("\n").append(user).append(" 0");

            FileWriter fileWriterEstado = new FileWriter("data/estado.txt");
            fileWriterEstado.write(contenido.toString());
            fileWriterEstado.close();
            scan.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    /** @brief Operación de iniciar sesión
    \pre <em>cierto</em>
    \post Retorna cierto sii existe un usuario con nombre user y contraseña pass
     */
    public boolean loginUsuario(String user, String pass)
    {
        String[] pathnames;
        File file = new File("data/Users/");
        pathnames = file.list();
        boolean encontrado = false;
        int i = 0;
        while (!encontrado && i < Objects.requireNonNull(pathnames).length){
            if (pathnames[i].equals(user)) encontrado = true;
            else ++i;
        }
        if (!encontrado) return false;
        else {
            String line = "";
            try {
                File file2 = new File("data/Users/" + pathnames[i] + "/infoUser/info.txt");
                Scanner scan = new Scanner(file2);
                line = scan.nextLine();
                scan.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            if (!line.equals(pass)) return false;
            return line.equals(getPass(pathnames[i]));
        }
    }

    /** @brief Operación de eliminar usuario
    \pre <em>existe un ususario con nombre user</em>
    \post Se eliminan todas las carpetas del usuario user
     */
    public void eliminarUsuario(String user)
    {
        File file = new File("data/Users/"+user);
        deleteDir(file);
    }

    /** @brief Operación de obtener contraseña
    \pre <em>existe un usuario con nombre user</em>
    \post Devuelve la contraseña de user
     */
    public String getPassword(String user)
    {
        return getPass(user);
    }

    /** @brief Operación de eliminar usuario de Estado
    \pre <em>existe un ususario con nombre user</em>
    \post Se elimina la fila de user de Estado.txt
     */
    public void eliminarUsuarioEstado(String user)
    {
        try {
            File file = new File("data/estado.txt");
            Scanner scan = new Scanner(file);
            StringBuilder contenido = new StringBuilder();
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                String[] par = line.split(" ");
                if (!par[0].equals(user)) {
                    contenido.append(line);
                    if (scan.hasNextLine()) contenido.append("\n");
                }
            }
            FileWriter fileWriter = new FileWriter("data/estado.txt");
            fileWriter.write(contenido.toString());
            fileWriter.close();
            scan.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /** @brief Operación de cambiar contraseña
    \pre <em>existe un ususario con nombre user y pass no es vacía</em>
    \post La contraseña de user pasa a ser pass
     */
    public void changePass(String user, String pass)
    {
        try {
            FileWriter fileWriter = new FileWriter("data/Users/" + user + "/infoUser/info.txt");
            fileWriter.write(pass);
            fileWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    /** @brief Operación de obtener contraseña
    \pre <em>existe un ususario con nombre user</em>
    \post Devuelve la contraseña de user
     */
    private String getPass(String user)
    {
        String resultado = "";
        try {
            File file = new File("data/Users/" + user + "/infoUser/info.txt");
            Scanner scan = new Scanner(file);
            resultado = scan.nextLine();
            scan.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return resultado;
    }

    /** @brief Función de eliminar un directorio
    \pre <em>el path file existe</em>
    \post Se elimina el fichero file y si era una carpeta se elimina lo que tuviera dentro
     */
    private void deleteDir(File file)
    {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (!Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        //noinspection ResultOfMethodCallIgnored
        file.delete();
    }


    /*                  |==========================================================|                  */
    /*                  |  FUNCIONES CAMBIADAS PARA TESTEAR EL PERSISTENCIAKAKURO  |                  */
    /*                  |==========================================================|                  */

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void creaUsuarioForJUNIT(String user, String pass)
    {
        String[] pathnames;
        File file = new File("data/JUnits/testsPersistencia/Users/");
        pathnames = file.list();
        for (String pathname : Objects.requireNonNull(pathnames)) {
            if (pathname.equals(user)) return;
        }
        File userFolder = new File("data/JUnits/testsPersistencia/Users/"+user);
        userFolder.mkdir();

        File gamesFolder = new File("data/JUnits/testsPersistencia/Users/"+user+"/partidasUser");
        gamesFolder.mkdir();

        File gamesFinishedFolder = new File("data/JUnits/testsPersistencia/Users/"+user+"/partidasAcabadas");
        gamesFinishedFolder.mkdir();

        File infoFolder = new File("data/JUnits/testsPersistencia/Users/"+user+"/infoUser");
        infoFolder.mkdir();

        try {
            File infoDocument = new File("data/JUnits/testsPersistencia/Users/" + user + "/infoUser/info.txt");
            infoDocument.createNewFile();

            FileWriter fileWriterInfo = new FileWriter("data/JUnits/testsPersistencia/Users/" + user + "/infoUser/info.txt");
            fileWriterInfo.write(pass);
            fileWriterInfo.close();

            File fileEstado = new File("data/JUnits/testsPersistencia/Estado.txt");
            Scanner scan = new Scanner(fileEstado);
            StringBuilder contenido = new StringBuilder();
            while (scan.hasNextLine()) {
                contenido.append(scan.nextLine());
                if (scan.hasNextLine()) contenido.append("\n");
            }
            contenido.append("\n").append(user).append(" 0");

            FileWriter fileWriterEstado = new FileWriter("data/JUnits/testsPersistencia/Estado.txt");
            fileWriterEstado.write(contenido.toString());
            fileWriterEstado.close();
            scan.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public boolean loginUsuarioForJUNIT(String user, String pass)
    {
        String[] pathnames;
        File file = new File("data/JUnits/testsPersistencia/Users/");
        pathnames = file.list();
        boolean encontrado = false;
        int i = 0;
        while (!encontrado && i < Objects.requireNonNull(pathnames).length){
            if (pathnames[i].equals(user)) encontrado = true;
            else ++i;
        }
        if (!encontrado) return false;
        else {
            String line = "";
            try {
                File file2 = new File("data/JUnits/testsPersistencia/Users/" + pathnames[i] + "/infoUser/info.txt");
                Scanner scan = new Scanner(file2);
                line = scan.nextLine();
                scan.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            if (!line.equals(pass)) return false;
            return line.equals(getPassForJUNIT(pathnames[i]));
        }
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    private String getPassForJUNIT(String user)
    {
        String resultado = "";
        try {
            File file = new File("data/JUnits/testsPersistencia/Users/" + user + "/infoUser/info.txt");
            Scanner scan = new Scanner(file);
            resultado = scan.nextLine();
            scan.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return resultado;
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public void eliminarUsuarioForJUNIT(String user)
    {
        File file = new File("data/JUnits/testsPersistencia/Users/"+user);
        deleteDir(file);
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public String getPasswordForJUNIT(String user)
    {
        return getPassForJUNIT(user);
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public void eliminarUsuarioEstadoForJUNIT(String user)
    {
        try {
            File file = new File("data/JUnits/testsPersistencia/Estado.txt");
            Scanner scan = new Scanner(file);
            StringBuilder contenido = new StringBuilder();
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                String[] par = line.split(" ");
                if (!par[0].equals(user)) {
                    contenido.append(line);
                    if (scan.hasNextLine()) contenido.append("\n");
                }
            }
            FileWriter fileWriter = new FileWriter("data/JUnits/testsPersistencia/Estado.txt");
            fileWriter.write(contenido.toString());
            fileWriter.close();
            scan.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public void changePassForJUNIT(String user, String pass)
    {
        try {
            FileWriter fileWriter = new FileWriter("data/JUnits/testsPersistencia/Users/" + user + "/infoUser/info.txt");
            fileWriter.write(pass);
            fileWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
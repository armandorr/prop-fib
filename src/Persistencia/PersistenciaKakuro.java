/** @file PersistenciaKakuro.java
    @brief Especificación de la clase PersistenciaKakuro
 */

package Persistencia;

import java.io.*;
import java.util.Scanner;

/** @class PersistenciaKakuro
    @brief Clase de persistencia relacionada con Kakuros

    Hecha por Marco Patiño

    Contiene funciones relacionadas con la obtencion de datos de archivos de usuario
 */
public class PersistenciaKakuro
{

    /** @brief Creadora por defecto
    \pre <em>ciertor</em>
    \post Se crea una PersistenciaKakuro vacía
     */
    public PersistenciaKakuro()
    {

    }

    /** @brief Operación de leer un Kakuro
    \pre <em>el archivo de path contiene la represntación de un Kakuro</em>
    \post Retorna el contenido de ese fichero en String
     */
    public String readKakuro(String path)
    {
        StringBuilder resultado = new StringBuilder();
        try {
            File file = new File(path);
            Scanner scan = new Scanner(file);
            resultado = new StringBuilder();
            while (scan.hasNextLine()) {
                String aux = scan.nextLine();
                resultado.append(aux);
                if (scan.hasNextLine()) resultado.append("\n");
            }
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return resultado.toString();
    }

    /** @brief Operación de guardar en el repositorio
    \pre <em>stringKakuro contiene la representación oficial de un Kakuro</em>
    \post Se guarda dicha String en un fichero de texto (cuya extensión dice si es una solución o no) y cuyo nombre es name
     */
    public void guardarKakuroEnRepositorio(String name, String stringKakuro, boolean isSol)
    {
        try {
            String path = "data/Repositorio/"+name+".txt";
            if(isSol) path = "data/Repositorio/"+name+".sol";
            File file = new File(path);
            if (!file.exists()) //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(stringKakuro);
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación de establecer la nueva ID disponible
    \pre <em>ID>=0</em>
    \post La ID de Estado.txt pasa a ser ID
     */
    public void setLastKakuroID(int ID)
    {
        try {
            File file = new File("data/estado.txt");
            Scanner scan = new Scanner(file);
            StringBuilder contenido = new StringBuilder(ID + "\n");
            scan.nextLine();
            while (scan.hasNextLine()) {
                contenido.append(scan.nextLine());
                if (scan.hasNextLine()) contenido.append("\n");
            }
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(contenido.toString());
            bufferedWriter.close();
            scan.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación de obtener el tiempo de una partida
    \pre <em>en path hay un fichero que representa un Kakuro que se está jugando</em>
    \post Retorna el tiempo, en ms, que lleva esa partida en juego
     */
    public int leerTiempoPartidaActual(String path)
    {
        String line = "";
        try {
            File file = new File(path);
            Scanner scan = new Scanner(file);
            line = scan.nextLine();
            String[] tamanoKakuro = line.split(",");
            int n = Integer.parseInt(tamanoKakuro[0]);
            for (int i = 0; i < n + 3; ++i) {
                line = scan.nextLine();
            }
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(line);
    }

    /** @brief Operación de obtener la última ID
    \pre <em>cierto</em>
    \post Obtiene la última ID asignada a un Kakuro
     */
    public int getLastKakuroID()
    {
        String id = "";
        try {
            File file = new File("data/estado.txt");
            Scanner scan = new Scanner(file);
            id = scan.nextLine();
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(id);
    }

    /** @brief Operación de obtener la puntuación de una partida finalizada
    \pre <em>en path hay un archivo de texto que representa una partida acabada</em>
    \post Retorna la puntuación que jugar esa partida aportó al usuario que la jugó
     */
    public int getPuntosGameAcabado(String path)
    {
        try {
            File file = new File(path);
            Scanner scan = new Scanner(file);
            String line = "";
            while (scan.hasNextLine()) {
                line = scan.nextLine();
            }
            return Integer.parseInt(line);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /** @brief Operación de establecer la info. extra necesaria a un Kakuro
    \pre <em>en path hay un fichero de texto que contiene la representación en formato oficial de un Kakuro SIN parámetros extra</em>
    \post Dicho archivo contiene ahora la ID del Kakuro que es ID, y su dificultad, que es dificultad
     */
    public void setDataToKakuro(int id, int dificultad, String path)
    {
        try {
            File file = new File(path);
            Scanner scan = new Scanner(file);
            StringBuilder contenido = new StringBuilder();
            while (scan.hasNextLine()) {
                contenido.append(scan.nextLine());
                contenido.append("\n");
            }
            scan.close();
            contenido.append(dificultad).append("\n").append(id).append("\n");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(contenido.toString());
            scan.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación de guardar Kakuro en su representación en formato oficial
    \pre <em>kakuroStandard contiene la representación oficial de un Kakuro y path es un directorio que existe</em>
    \post Si en path había un archivo con nombre nombrePartida, lo actualiza y el archivo ahora es el contenido de kakuroStandard. Si no existía, lo crea con el mismo contenido
     */
    public void guardarKakuroFormatoStandard(String path, String nombrePartida, String kakuroStandard)
    {
        try {
            String pathCompleto = path+nombrePartida+".std";
            File file = new File(pathCompleto);
            if (!file.exists()) //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(kakuroStandard);
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*                  |==========================================================|                  */
    /*                  |  FUNCIONES CAMBIADAS PARA TESTEAR EL PERSISTENCIAKAKURO  |                  */
    /*                  |==========================================================|                  */

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public void guardarKakuroEnRepositorioForJUNIT(String name, String stringKakuro, boolean isSol)
    {
        try {
            String path = "data/JUnits/testsPersistencia/Repositorio/"+name+".txt";
            if(isSol) path = "data/JUnits/testsPersistencia/Repositorio/"+name+".sol";
            File file = new File(path);
            if (!file.exists()) //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(stringKakuro);
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public void setLastKakuroIDForJUNIT(int ID)
    {
        try {
            File file = new File("data/JUnits/testsPersistencia/Estado.txt");
            Scanner scan = new Scanner(file);
            StringBuilder contenido = new StringBuilder(ID + "\n");
            scan.nextLine();
            while (scan.hasNextLine()) {
                contenido.append(scan.nextLine());
                if (scan.hasNextLine()) contenido.append("\n");
            }
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(contenido.toString());
            bufferedWriter.close();
            scan.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación con path cambiado para poder hacer los tests con seguridad */
    public int getLastKakuroIDForJUnit()
    {
        String id = "";
        try {
            File file = new File("data/JUnits/testsPersistencia/Estado.txt");
            Scanner scan = new Scanner(file);
            id = scan.nextLine();
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(id);
    }

}
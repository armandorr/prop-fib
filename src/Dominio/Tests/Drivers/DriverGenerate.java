/** @file DriverGenerate.java
 @brief Especificación del DriverGenerate
 */

package Dominio.Tests.Drivers;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;
import Dominio.Controladores.CtrlGenerate;
import Dominio.Controladores.CtrlVerify;

/** @class DriverGenerate
 @brief Driver dedicado al testeo de todas las funciones del generador de Kakuros.

 Hecha por Marco Patiño

 Contiene diversas funciones que son capaces de testear las funciones que ayudan a la generación de Kakuros y a la
 própia funcionalidad de generación.
 */
public class DriverGenerate
{

    /** @brief Variable utiles que nos ayuda a simplificar la impresión por pantalla y la lectura de ficheros*/
    static Utiles utiles = new Utiles();

    /** @brief Menu del driver de testeo de todas las funciones de CtrlGenerate
    \pre <em>Cierto</em>
    \post El menu ha llamado a ciertas funciones y se ha ejecutado hasta que el usuario ha decidido terminar y salir.
     */
    public static void main(String[] args)
    {
        int opcion = 0;
        while (opcion != 13) {
            utiles.printNice("MENU DRIVER GENERATE");
            System.out.println(utiles.wellTabed("|     Escriba el numero de la tarea que desea hacer:", "|"));
            System.out.println(utiles.wellTabed("|          [1]:  Testear la función 'firstWhiteTile'", "|"));
            System.out.println(utiles.wellTabed("|          [2]:  Testear la función 'eraseWhiteTileValues'", "|"));
            System.out.println(utiles.wellTabed("|          [3]:  Testear la función 'setValueToBlackRight' ", "|"));
            System.out.println(utiles.wellTabed("|          [4]:  Testear la función 'setValueToBlackBottom' ", "|"));
            System.out.println(utiles.wellTabed("|          [5]:  Testear la función 'putValuesInBlackTiles'", "|"));
            System.out.println(utiles.wellTabed("|          [6]:  Testear la función 'setGoodNumbers'", "|"));
            System.out.println(utiles.wellTabed("|          [7]:  Testear la función 'tryPutCombinationRight'", "|"));
            System.out.println(utiles.wellTabed("|          [8]:  Testear la función 'fillWithBlack'", "|"));
            System.out.println(utiles.wellTabed("|          [9]:  Testear la función 'createLayout'", "|"));
            System.out.println(utiles.wellTabed("|          [10]: Testear la función 'completeUniqueness'", "|"));
            System.out.println(utiles.wellTabed("|          [11]: Testear la función 'trivialMaked'", "|"));
            System.out.println(utiles.wellTabed("|          [12]: Testear la funcionalidad 'generarKakuro'", "|"));
            System.out.println(utiles.wellTabed("|          [13]: Salir", "|"));

            utiles.printLineEqual();
            opcion = utiles.readInt();

            if      (opcion == 1) TestFirstWhiteTile();
            else if (opcion == 2) TestEraseWhiteTileValues();
            else if (opcion == 3) TestSetValueToBlackRight();
            else if (opcion == 4) TesSetValueToBlackBottom();
            else if (opcion == 5) TestPutValuesInBlackTiles();
            else if (opcion == 6) TestSetGoodNumbers();
            else if (opcion == 7) TestTryPutCombinationRight();
            else if (opcion == 8) TestFillWithBlack();
            else if (opcion == 9) TestCreateLayout();
            else if (opcion == 10) TestCompleteUniqueness();
            else if (opcion == 11) TestTrivialMaked();
            else if (opcion == 12) TestGenerarKakuro();
        }
        utiles.clearScreen();
    }

    /** @brief Función que testea la función createLayout de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestCreateLayout()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'createLayout'", " "));
        System.out.println(utiles.Centered("Has de seleccionar una Kakuro con la propiedad Layout"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();
        Kakuro[] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if(ret==0) {
            utiles.clearScreen();
            utiles.printNice("KAKURO SELECCIONADO");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("Quieres ejecutar el test? Sí[1]/No[0]");
            int answer = utiles.readInt();
            if(answer == 1) {
                CtrlGenerate CG = new CtrlGenerate(k[0]);
                CG.createLayoutPublic();
                CG.eraseWhiteTileValuesPublic();
                utiles.printNice("KAKURO GENERADO");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("¿El Kakuro ha quedado sin ninguna fila o columna de más de 9 casillas blanca seguidas? Sí[1]/No[0]");
                int ans = utiles.readInt();
                utiles.clearScreen();
                if (ans == 1) utiles.printNiceWithSpaces("createLayout funcionó correctamente");
                else utiles.printNiceWithSpaces("createLayout NO funcionó correctamente");
            }
            else utiles.clearScreen();
        }
    }

    /** @brief Función que testea la función tryPutCombinationRight de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestTryPutCombinationRight()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'TestTryPutCombinationRight'", " "));
        System.out.println(utiles.Centered("Has de seleccionar una Kakuro con la propiedad TestTryPutCombinationRight"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();
        Kakuro[] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            boolean goodInp = false;
            String line;
            int[] nums = new int[0];
            while (!goodInp) {
                utiles.clearScreen();
                utiles.printNice("TEST DE LA FUNCIONALIDAD 'TestTryPutCombinationRight'");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printLineEqual();
                System.out.println(utiles.Centered(" ", " "));
                System.out.println(utiles.Centered("Introduce unos valores correctos para la i, j, (coordenadas casilla negra) y el vector comb (ha de ser", " "));
                System.out.println(utiles.Centered("una secuencia de numeros de longitud igual al numero de blancas a la derecha de la negra indicada)"," "));
                System.out.println(utiles.Centered(" ", " "));
                utiles.printLineEqual();
                line = utiles.readLine();
                nums = utiles.convertStringtoIntArrayNums(line);
                if (nums.length > 0 && nums[0] < k[0].getKakuroRows() && nums[1] < k[0].getKakuroCols()) goodInp = true;
            }
            int [] comb = new int[nums.length-2];
            System.arraycopy(nums, 2, comb, 0, nums.length - 2);
            boolean done = CG.tryPutCombinationRightPublic(nums[0], nums[1], comb);
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("¿Es possible poner los números que añadiste? Sí[1]/No[0]");
            int ans = utiles.readInt();
            utiles.clearScreen();
            if ((ans==1) == done) utiles.printNiceWithSpaces("PutCombinationRight funcionó correctamente");
            else utiles.printNiceWithSpaces("PutCombinationRight NO funcionó correctamente");
        }
    }

    /** @brief Función que testea la función setGoodNumbers de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestSetGoodNumbers()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'TestSetGoodNumbers'", " "));
        System.out.println(utiles.Centered("Has de seleccionar una Kakuro con la propiedad setGoodNumbers"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();
        Kakuro[] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            boolean result = CG.setGoodNumbersPublic();
            if (result){
                utiles.printNiceWithSpaces("Se ha podido rellenar el kakuro ");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("¿El kakuro ha quedado rellenado? Sí[1]/No[0]");
                int ans = utiles.readInt();
                utiles.clearScreen();
                if (ans == 1) utiles.printNiceWithSpaces("setGoodNumbers funcionó correctamente");
                else utiles.printNiceWithSpaces("setGoodNumbers NO funcionó correctamente");
            }
            else utiles.printNiceWithSpaces("No se ha podido rellenar. Significa que ha encontrado una colisión. No significa que haya fallado");
        }
    }

    /** @brief Función que testea la función firstWhiteTile de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void TestFirstWhiteTile()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'TestFirstWhiteTile'");
        Kakuro[] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            boolean goodInp = false;
            String line;
            int[] nums = new int[0];
            while (!goodInp) {
                utiles.clearScreen();
                utiles.printNice("TEST DE LA FUNCIONALIDAD 'TestFirstWhiteTile'");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("Introduce la posición i,j desde la que quieres empezar a buscar la siguiente casilla blanca.");
                line = utiles.readLine();
                nums = utiles.convertStringtoIntArrayNums(line);
                if (nums.length > 0 && nums[0] < k[0].getKakuroRows() && nums[1] < k[0].getKakuroCols()) goodInp = true;
            }
            utiles.clearScreen();
            int[] ii = new int[1]; ii[0] = nums[0];
            int[] jj = new int[1]; jj[0] = nums[1];

            CG.firstWhiteTilePublic(ii, jj);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'TestFirstWhiteTile'");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("Introduce la posición de la primera blanca que encontrarías desde "+nums[0]+","+nums[1]+".");
            String line2 = utiles.readLine();
            int[] nums2 = utiles.convertStringtoIntArrayNums(line2);

            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));

            if (ii[0] == nums2[0] && jj[0] == nums2[1]) {
                System.out.println(utiles.Centered("'testFirstWhiteTile' funcionó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testFirstWhiteTile' NO funcionó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + ii[0] + " " + jj[0], " "));
                System.out.println(utiles.Centered("EXPECTED: " + nums2[0] + " " + nums2[1], " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
        }
    }

    /** @brief Función que testea la función eraseWhiteTileValues de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestEraseWhiteTileValues()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'TestEraseWhiteTileValues'", " "));
        System.out.println(utiles.Centered("Has de seleccionar una Kakuro con la propiedad eraseWhiteTileValues"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();

        Kakuro[] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            utiles.printNice("KAKURO SELECCIONADO");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("Pulsa 1 para vaciar el Kakuro");
            int ans = utiles.readInt();
            while (ans != 1) ans = utiles.readInt();
            utiles.clearScreen();
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            CG.eraseWhiteTileValuesPublic();
            utiles.printNice("KAKURO VACIADO");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("¿El kakuro ha quedado vacío? Sí[1]/No[0]");
            ans = utiles.readInt();
            utiles.clearScreen();
            if (ans == 1) utiles.printNiceWithSpaces("'eraseWhiteTileValues' funcionó correctamente");
            else utiles.printNiceWithSpaces("'eraseWhiteTileValues' NO funcionó correctamente");
        }
    }

    /** @brief Función que testea la función completeUniqueness de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestCompleteUniqueness()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'completeUniqueness'", " "));
        System.out.println(utiles.Centered("Has de seleccionar una Kakuro con la propiedad Unique para visualizar mejor el efecto"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();

        Kakuro [] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            utiles.printNice("kakuro elegido");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("Quieres ejecutar el test? Sí[1]/No[0]");
            int answer = utiles.readInt();
            if(answer == 1) {
                CtrlGenerate CG = new CtrlGenerate(k[0]);
                boolean res = CG.completeUniquenessPublic();
                utiles.clearScreen();
                utiles.printNice("TEST COMPLETE UNIQUENESS");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("¿Se han completado todas las casillas únicas/triviales que había en el original? Sí[1]/No[0]");
                answer = utiles.readInt();
                utiles.clearScreen();
                if(answer == 1) {
                    utiles.printNice("TEST COMPLETE UNIQUENESS");
                    System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                    utiles.printNiceWithSpaces("¿Se han convertido almenos una casilla a negra? Sí[1]/No[0]");
                    answer = utiles.readInt();
                    utiles.clearScreen();
                    if (res == (1 == answer)) utiles.printNiceWithSpaces("'TestCompleteUniqueness' ha funcionado correctamente");
                    else {
                        utiles.printLineEqual();
                        System.out.println(utiles.Centered("ERROR: 'TestCompleteUniqueness' NO ha funcionado correctamente", " "));
                        System.out.println(utiles.Centered("ACTUAL: " + (1==answer), " "));
                        System.out.println(utiles.Centered("EXPECTED: " + res, " "));
                        utiles.printLineEqual();
                    }
                }
                else {
                    utiles.printLineEqual();
                    System.out.println(utiles.Centered("ERROR: 'TestCompleteUniqueness' NO ha funcionado correctamente", " "));
                    System.out.println(utiles.Centered("ACTUAL: " + answer, " "));
                    System.out.println(utiles.Centered("EXPECTED: " + 1, " "));
                    utiles.printLineEqual();
                }
            }
        }
    }

    /** @brief Función que testea la función fillWithBlack de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestFillWithBlack()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'fillWithBlack'", " "));
        System.out.println(utiles.Centered("Has de seleccionar un Kakuro con la propiedad fillWithBlack para visualizar mejor el efecto"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();

        Kakuro [] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            boolean goodInp = false;
            int[] nums = new int[0];
            while (!goodInp) {
                utiles.clearScreen();
                utiles.printNice("KAKURO SELECCIONADO");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("Introduce la dificultad 1 o 3, y si quieres llenar el kakuro de forma ordenada Sí[1]/No[0]");
                String line = utiles.readLine();
                nums = utiles.convertStringtoIntArrayNums(line);
                if (nums.length > 0 && (nums[0] == 1 || nums[0] == 3) && (nums[1] == 1 || nums[1] == 0)) goodInp = true;
            }
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            CG.fillWithBlackPublic(nums[0], nums[1] == 1);
            utiles.clearScreen();
            utiles.printNice("KAKURO CREADO");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
            utiles.printNiceWithSpaces("¿En el kakuro hay algun caso trivial o ha dejado de ser connexo? Sí[1]/No[0]");
            int answer = utiles.readInt();
            utiles.clearScreen();
            if(answer == 0) utiles.printNiceWithSpaces("'fillWithBlack' ha funcionado correctamente");
            else if (answer == 1)utiles.printNiceWithSpaces("'fillWithBlack' NO ha funcionado correctamente");
        }
        else utiles.clearScreen();
    }

    /** @brief Función que testea la función trivialMaked de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestTrivialMaked()
    {
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("TEST DE LA FUNCIONALIDAD 'trivialMaked'", " "));
        System.out.println(utiles.Centered("Has de seleccionar un Kakuro con la propiedad trivial para visualizar mejor el efecto"," "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();

        Kakuro [] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            boolean goodInp = false;
            int[] nums = new int[0];
            while (!goodInp) {
                utiles.clearScreen();
                utiles.printNice("KAKURO SELECCIONADO");
                System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));
                utiles.printNiceWithSpaces("Introduce la posición i,j y la dificultad 1 o 3 (no habilitado el testeo con dificultad 2 por ser aleatorio)");
                String line = utiles.readLine();
                nums = utiles.convertStringtoIntArrayNums(line);
                if (nums.length > 0 && nums[0] < k[0].getKakuroRows() && nums[1] < k[0].getKakuroCols()) goodInp = true;
            }
            CtrlGenerate CG = new CtrlGenerate(k[0]);
            boolean trivialMaked = CG.trivialMakedPublic(nums[0], nums[1], nums[2]);
            utiles.clearScreen();
            utiles.printNiceWithSpaces("¿Se debería crear alguna casilla trivial? Sí[1]/No[0]");
            int answer = utiles.readInt();
            utiles.clearScreen();
            if(trivialMaked == (1==answer)) utiles.printNiceWithSpaces("'TestTrivialMaked' ha funcionado correctamente");
            else utiles.printNiceWithSpaces("'TestTrivialMaked' NO ha funcionado correctamente");
        }
        else utiles.clearScreen();
    }

    /** @brief Función que testea la función setValueToBlackRight de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void TestSetValueToBlackRight()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'setValueToBlackRight'");
        System.out.println(utiles.Centered("Has de usar un Kakuro con la propiedad 'SetValueInBlack'", "|"));
        Kakuro [] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'setValueToBlackRight'");
            utiles.printNice("El kakuro es:");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla negra que quieras, con el siguiente formato: i j");
            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);

            utiles.printNice("¿Qué valor hacia la derecha debería haber en la casilla negra que has elegido?");
            int expected = utiles.readInt();

            CtrlGenerate CG = new CtrlGenerate(k[0]);
            CG.setValueToBlackRightPublic(nums[0], nums[1]);

            int actual = CG.getKakuro().getRightValueBlackTile(nums[0], nums[1]);
            boolean correct = (actual == expected);

            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered("", " "));
            if (correct) {
                System.out.println(utiles.Centered("'testPutPossibilitiesBottom' funcionó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testPutPossibilitiesBottom' NO funcionó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + actual, " "));
                System.out.println(utiles.Centered("EXPECTED: " + expected, " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
        }
    }

    /** @brief Función que testea la función setValueToBlackBottom de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TesSetValueToBlackBottom()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'setValueToBlackBottom'");
        System.out.println(utiles.Centered("Has de usar un Kakuro con la propiedad 'SetValueInBlack'", "|"));
        Kakuro [] k = new Kakuro[1];
        int ret = utiles.getI(k, "data/Drivers/DriverGenerate/");
        if (ret == 0) {
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'setValueToBlackBottom'");
            utiles.printNice("El kakuro es:");
            System.out.println(utiles.KakuroToCentred(k[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla negra que quieras, con el siguiente formato: i j");
            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);

            utiles.printNice("¿Qué valor hacia abajo debería haber en la casilla negra que has elegido?");
            int expected = utiles.readInt();

            CtrlGenerate CG = new CtrlGenerate(k[0]);
            CG.setValueToBlackBottomPublic(nums[0], nums[1]);

            int actual = CG.getKakuro().getBottomValueBlackTile(nums[0], nums[1]);
            boolean correct = (actual == expected);

            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if (correct) {
                System.out.println(utiles.Centered("'testPutPossibilitiesBottom' funcionó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testPutPossibilitiesBottom' NO funcionó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + actual, " "));
                System.out.println(utiles.Centered("EXPECTED: " + expected, " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
        }
    }

    /** @brief Función que testea la función putValuesInBlackTiles de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    private static void TestPutValuesInBlackTiles()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'putValuesInBlackTiles'");
        System.out.println(utiles.Centered("Has de usar un Kakuro con la propiedad 'putValueInBlack'", "|"));
        Kakuro[] k = new Kakuro[1];
        String[] ke = new String[1];
        ke[0] = "";
        if(utiles.getIO(k, ke,"data/Drivers/DriverGenerate/")==0) {
            utiles.printNice("El kakuro es:");
            System.out.println(k[0].kakuroToStringNice());

            CtrlGenerate CG = new CtrlGenerate(k[0]);
            CG.putValuesInBlackTilesPublic();
            utiles.clearScreen();

            boolean correct = CG.getKakuro().kakuroToStringStandard(false).equals(ke[0]);
            utiles.clearScreen();
            if (correct) utiles.printNiceWithSpaces("'putValuesInBlackTiles' funcionó correctamente");
            else {
                utiles.printNiceWithSpaces("ERROR: 'putValuesInBlackTiles' NO funcionó correctamente");
                utiles.printNice("ACTUAL:");
                System.out.println(k[0].kakuroToStringStandard(false));
                utiles.printNice("EXPECTED:");
                System.out.println(ke[0]);
            }
        }
    }

    /** @brief Función que testea la funcionalidad principal de la clase CtrlGenerate, la generación de un Kakuro determinado
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    private static void TestGenerarKakuro()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'generarKakuro'");
        System.out.println(utiles.wellTabed("|    [n m Dificultad]: Indica el tamaño del Kakuro n x m y la dificultad [1,2,3]", "|"));
        System.out.println(utiles.wellTabed("|    [Q]             : Salir", "|"));
        utiles.printLineEqual();
        String line = utiles.readLine();
        int x = utiles.treatInput(line);
        utiles.clearScreen();
        if (x == 0) {
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            if (nums.length > 0) {
                utiles.printNice("GENERANDO KAKURO...");
                CtrlGenerate CG = new CtrlGenerate();
                Kakuro k = CG.generarKakuro(nums[0], nums[1], nums[2]);
                utiles.clearScreen();
                utiles.printNice("TEST DE LA FUNCIONALIDAD 'generarKakuro'");
                utiles.printNice("KAKURO GENERADO");
                System.out.println(utiles.KakuroToCentred(k.kakuroToStringNice()));
                utiles.printNiceWithSpaces("¿El kakuro es connexo? Sí[1]/No[0]");
                int answer = utiles.readInt();
                utiles.clearScreen();
                if (answer == 1) {
                    utiles.printNice("TEST DE LA FUNCIONALIDAD 'generarKakuro'");
                    utiles.printNice("KAKURO GENERADO");
                    System.out.println(utiles.KakuroToCentred(k.kakuroToStringNice()));
                    utiles.printNiceWithSpaces("¿El kakuro contiene alguna casilla blanca aislada? Sí[1]/No[0]");
                    answer = utiles.readInt();
                    utiles.clearScreen();
                    if (answer == 0){
                        CtrlVerify ver = new CtrlVerify(k);
                        Kakuro[] solucion = new Kakuro[1];
                        int numSols = ver.verify(solucion);
                        utiles.printNice("VERIFICANDO SOLUCIÓN ÚNICA");
                        if(numSols == 1) utiles.printNiceWithSpaces("'generarKakuro' ha funcionado correctamente porque, además, tiene solución única");
                        else utiles.printNiceWithSpaces("'generarKakuro' ha generado un kakuro sin solución única");
                    }
                    else
                        utiles.printNiceWithSpaces("ERROR: 'generarKakuro' ha generado un Kakuro con una casilla blanca aislada");
                } else {
                    utiles.clearScreen();
                    utiles.printNiceWithSpaces("ERROR: 'generarKakuro' no ha generado un Kakuro connexo");
                }
            }
        }
    }
}
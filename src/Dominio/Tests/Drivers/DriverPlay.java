/** @file DriverPlay.java
 @brief Especificación del DriverPlay
 */

package Dominio.Tests.Drivers;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;
import Dominio.Controladores.CtrlPlay;

import java.io.IOException;
import java.util.Scanner;

/** @class DriverPlay
    @brief Driver dedicado al testeo de la funcionalidad de juego.

    Hecha por David Marín

 Contiene diversas funciones que son capaces de testear las funciones de juego por consola y por fichero.
 */
public class DriverPlay
{

    /** @brief Variable utiles que nos ayuda a simplificar la impresión por pantalla y la lectura de ficheros*/
    static Utiles utiles = new Utiles();

    /** @brief Menu del driver de testeo de todas las funciones de CtrlPlay
    \pre <em>Cierto</em>
    \post El menu ha llamado a ciertas funciones y se ha ejecutado hasta que el usuario ha decidido terminar y salir.
     */
    public static void main(String[] args)
    {
        int opcion = 0;

        while(opcion != 3) {
            opcion = 0;
            while (opcion>=0 && opcion<3) {

                utiles.printNice("MENU DRIVER PLAY");
                System.out.println(utiles.wellTabed("|     Escriba el numero de la tarea que desea hacer:", "|"));
                System.out.println(utiles.wellTabed("|          [1]:  Testear la funcionalidad 'play' de la clase CrtlPlay con Kakuro por fichero", "|"));
                System.out.println(utiles.wellTabed("|          [2]:  Testear la funcionalidad 'play' de la clase CrtlPlay con Kakuro por consola", "|"));
                System.out.println(utiles.wellTabed("|          [3]:  Salir", "|"));

                utiles.printLineEqual();
                Scanner e = new Scanner(System.in);
                opcion = e.nextInt();

                if(opcion==1) testPlayFichero();
                else if(opcion==2) testPlayCons();
            }
        }

    }

    /** @brief Función que testea la funcionalidad de juego de un Kakuro introducido por fichero
    \pre <em>Kakuro en formato estándar</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    private static void testPlayFichero()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'play' con Kakuro por fichero");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverPlay/");
        if (ret == 0) {
            CtrlPlay p = new CtrlPlay(kActual[0]);
            try {
                p.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** @brief Función que testea la funcionalidad de juego de un Kakuro introducido por consola
    \pre <em>Kakuro en formato estándar</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    public static void testPlayCons()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'play' con Kakuro por consola");
        System.out.println(utiles.wellTabed("|          Introduce el Kakuro por consola en el formato estándar/oficial", "|"));
        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
        utiles.printLineEqual();
        Scanner e = new Scanner(System.in);
        StringBuilder ks = new StringBuilder(e.nextLine() + "\n");
        int x = utiles.treatInput(ks.toString());
        if (x == 0) {
            String[] v = ks.toString().split(",");
            int rows = Integer.parseInt(v[0]);
            for (int i = 0; i < rows; ++i) {
                ks.append(e.nextLine()).append("\n");
            }
            Kakuro k = new Kakuro();
            k.readKakuroFromString(ks.toString()); // ahora tenemos en k el Kakuro que se ha introducido por consola

            CtrlPlay p = new CtrlPlay(k);
            try {
                p.play();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        else utiles.clearScreen();
    }
}

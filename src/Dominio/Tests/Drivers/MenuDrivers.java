/** @file MenuDrivers.java
 @brief Especificación de la classe MenuDrivers
 */

package Dominio.Tests.Drivers;

import Dominio.Clases.Utiles;

/** @class MenuDrivers
    @brief Nos sirve de menu para acceder a los posibles drivers de las diferentes funcionalidades

    Hecha por Hasnain Shafqat
 */
public class MenuDrivers
{

    /** @brief Variable utiles que nos ayuda a simplificar la impresión por pantalla y la lectura de ficheros*/
    static Utiles utiles = new Utiles();

    /** @brief Menu principal de los drivers
    \pre <em>Cierto</em>
    \post El menu ha llamado a ciertas funciones y se ha ejecutado hasta que el usuario ha decidido terminar y salir
     */
    public static void main (String[] args)
    {
        utiles.clearScreen();
        int opcion = 0;
        String [] arg = new String[0];
        while(opcion != 5) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 5) {
                    utiles.printNice("MENU PRINCIPAL DRIVERS");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Testear Driver Play", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Testear Driver Generate", "|"));
                    System.out.println(utiles.wellTabed("|          [3]: Testear Driver Solve", "|"));
                    System.out.println(utiles.wellTabed("|          [4]: Testear Driver Verify", "|"));
                    System.out.println(utiles.wellTabed("|          [5]: Salir", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                if (opcion != 5) utiles.clearScreen();
                if (opcion == 1) DriverPlay.main(arg);
                else if (opcion == 2) DriverGenerate.main(arg);
                else if (opcion == 3) DriverSolve.main(arg);
                else if (opcion == 4) DriverVerify.main(arg);
            }
            catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }
}
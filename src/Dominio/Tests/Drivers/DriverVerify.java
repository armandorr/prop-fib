/** @file DriverVerify.java
 @brief Especificación del Driver Verify
 */

package Dominio.Tests.Drivers;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;
import Dominio.Controladores.CtrlVerify;

import java.util.Scanner;

/** @class DriverVerify
 @brief Driver dedicado al testeo de la verificación de Kakuros.

 Hecha por Armando Rodríguez

 Contiene diversas funciones que son capaces de testear la funcionalidad de verificación tanto por entrada de fichero
 como para entrada por consola de un Kakuro en formato estándar.
 */
public class DriverVerify
{

    /** @brief Variable utiles que nos ayuda a simplificar la impresión por pantalla y la lectura de ficheros*/
    static Utiles utiles = new Utiles();

    /** @brief Menu del driver de testeo de la funcionalidd de verify
    \pre <em>Cierto</em>
    \post El menu ha llamado a ciertas funciones y se ha ejecutado hasta que el usuario ha decidido terminar y salir.
     */
    public static void main(String[] args)
    {
        int opcion = 0;
        while(opcion != 3) {
            utiles.printNice("MENU DRIVER VERIFY");
            System.out.println(utiles.wellTabed("|     Escriba el numero de la tarea que desea probar:", "|"));
            System.out.println(utiles.wellTabed("|          [1]:  Testear la funcionalidad 'verify' de la clase CtrlVerify con un Kakuro de un fichero", "|"));
            System.out.println(utiles.wellTabed("|          [2]:  Testear la funcionalidad 'verify' de la clase CtrlVerify con un Kakuro introducido por consola", "|"));
            System.out.println(utiles.wellTabed("|          [3]:  Salir", "|"));
            utiles.printLineEqual();

            opcion = utiles.readInt();

            if(opcion==1) testVerifyFile();
            else if(opcion==2) testVerifyConsole();
        }
        utiles.clearScreen();
    }

    /** @brief Función que testea la funcionalidad de verificación de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    public static void testVerifyFile()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'verify' con Kakuro por fichero");

        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverVerify/");
        if(ret != -1) {
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            System.out.println(utiles.Centered("¿Cuantas soluciones debería tener el Kakuro? Introduce un único número.", " "));
            System.out.println(utiles.Centered("0 -> No tiene solución, 1 -> Solución única, 2 -> Más de una solución", " "));
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();

            int expected = utiles.readInt();
            if (expected == 0 || expected == 1 || expected == 2) {
                CtrlVerify v = new CtrlVerify(kActual[0]);

                utiles.clearScreen();
                utiles.printNice("Kakuro a verificar");
                System.out.println(kActual[0].kakuroToStringNice());
                utiles.printNice("VERIFICANDO...");
                Kakuro[] solucion = new Kakuro[1];
                int actual = v.verify(solucion);
                if (actual == -1) actual = 1; //Si ya esta completo el kakuro suponemos que tiene una solución
                utiles.clearScreen();

                boolean correct = (actual == expected);
                utiles.printLineEqual();
                System.out.println(utiles.Centered(" ", " "));
                if (correct) {
                    System.out.println(utiles.Centered("'verify' funcionó correctamente", " "));
                } else {
                    System.out.println(utiles.Centered("ERROR: 'verify' NO funcionó correctamente", " "));
                    System.out.println(utiles.Centered("ACTUAL:" + actual + " <----> " + "EXPECTED:" + expected, " "));
                }
                System.out.println(utiles.Centered(" ", " "));
                utiles.printLineEqual();
            }
        }
    }

    /** @brief Función que testea la funcionalidad de verificación de un Kakuro introducido por consola
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    public static void testVerifyConsole()
    {
        utiles.clearScreen();
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'verify' con Kakuro por consola");

        System.out.println(utiles.wellTabed("|          Introduce el Kakuro por consola en el formato estándar/oficial", "|"));
        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
        utiles.printLineEqual();
        Scanner e = new Scanner(System.in);
        StringBuilder kakuroString = new StringBuilder(e.nextLine() + "\n");
        int x = utiles.treatInput(kakuroString.toString());
        if (x == 0){
            String[] v = kakuroString.toString().split(",");
            int rows = Integer.parseInt(v[0]);
            for (int i = 0; i < rows; ++i) kakuroString.append(e.nextLine()).append("\n");
            Kakuro kActual = new Kakuro();
            kActual.readKakuroFromString(kakuroString.toString()); // ahora tenemos en k el Kakuro que se ha introducido por consola
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            System.out.println(utiles.Centered("¿Cuantas soluciones debería tener el Kakuro? Introduce un único número.", " "));
            System.out.println(utiles.Centered("0 -> No tiene solución, 1 -> Solución única, 2 -> Más de una solución", " "));
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            int expected = e.nextInt();
            if (expected == 0 || expected == 1 || expected == 2) {
                CtrlVerify ver = new CtrlVerify(kActual);
                utiles.clearScreen();
                utiles.printNice("Kakuro a verificar");
                System.out.println(kActual.kakuroToStringNice());
                utiles.printNice("VERIFICANDO...");
                Kakuro[] solucion = new Kakuro[1];
                int actual = ver.verify(solucion);
                if (actual == -1) actual = 1;
                utiles.clearScreen();

                boolean correct = (actual == expected);

                utiles.printLineEqual();
                System.out.println(utiles.Centered(" ", " "));
                if (correct) {
                    System.out.println(utiles.Centered("'verify' funcionó correctamente", " "));
                } else {
                    System.out.println(utiles.Centered("ERROR: 'verify' NO funcionó correctamente", " "));
                    System.out.println(utiles.Centered("ACTUAL:" + actual + " <----> " + "EXPECTED:" + expected, " "));
                }
                System.out.println(utiles.Centered(" ", " "));
                utiles.printLineEqual();
            }
        }
        else utiles.clearScreen();
    }
}
/** @file DriverSolve.java
 @brief Especificación del DriverSolve
 */

package Dominio.Tests.Drivers;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;
import Dominio.Clases.WhiteTile;
import Dominio.Controladores.CtrlSolve;

import java.util.ArrayList;

/** @class DriverSolve
 @brief Driver dedicado al testeo del solucionador de Kakuros.

 Hecha por David Marin

 Contiene diversas funciones que son capaces de testear las funciones que ayudan a la resolución de Kakuros y a la
 própia funcionalidad de resolución.
 */
public class DriverSolve
{

    /** @brief Variable utiles que nos ayuda a simplificar la impresión por pantalla y la lectura de ficheros*/
    static Utiles utiles = new Utiles();

    /** @brief Menu del driver de testeo de todas las funciones de CtrlSolve
    \pre <em>Cierto</em>
    \post El menu ha llamado a ciertas funciones y se ha ejecutado hasta que el usuario ha decidido terminar y salir.
     */
    public static void main(String[] args)
    {
        int opcion = 0;
        while(opcion != 9) {
            try {
                utiles.printNice("MENU DRIVER SOLVE");
                System.out.println(utiles.wellTabed("|     Escriba el numero de la tarea que desea hacer:", "|"));
                System.out.println(utiles.wellTabed("|          [1]:  Testear la funcionalidad 'solve' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [2]:  Testear la funcionalidad 'putAllWhiteTilesPossibilities' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [3]:  Testear la funcionalidad 'getAllCombinations' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [4]:  Testear la funcionalidad 'putPossibilitiesBottom' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [5]:  Testear la funcionalidad 'putPossibilitiesRight' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [6]:  Testear la funcionalidad 'howManySpacesBottom' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [7]:  Testear la funcionalidad 'howManySpacesRight' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [8]:  Testear la funcionalidad 'possibleCombination' de la clase Solve", "|"));
                System.out.println(utiles.wellTabed("|          [9]:  Salir", "|"));

                utiles.printLineEqual();
                opcion = utiles.readInt();
                utiles.clearScreen();

                if (opcion == 1) testSolve();
                else if (opcion == 2) testPutAllWhiteTilesPossibilities();
                else if (opcion == 3) testGetAllCombinations();
                else if (opcion == 4) testPutPossibilitiesBottom();
                else if (opcion == 5) testPutPossibilitiesRight();
                else if (opcion == 6) testHowManySpacesBottom();
                else if (opcion == 7) testHowManySpacesRight();
                else if (opcion == 8) testPossibleCombination();
            }
            catch(NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Función que testea la funcionalidad de resolución de un Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la funcionalidad
     */
    public static void testSolve()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'testSolve'");
        Kakuro[] kActual = new Kakuro[1];
        String[] expected = new String[1];
        expected[0] = "";
        int ret = utiles.getIO(kActual, expected,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            s.solve_recursivo();
            String actual = s.getKakuroSolved().kakuroToStringStandard(true);
            String[] p = actual.split("\n");
            StringBuilder realActual = new StringBuilder();
            for(int i=0; i<=Integer.parseInt(p[0].split(",")[0]); ++i) {
                realActual.append(p[i]).append("\n");
            }

            boolean correct = (realActual.toString().equals(expected[0]));
            utiles.clearScreen();
            if (correct) utiles.printNiceWithSpaces("'solve' funcinó correctamente");
            else {
                utiles.printNiceWithSpaces("ERROR: 'solve' NO funcinó correctamente");
                utiles.printNice("ACTUAL:");
                System.out.println(realActual);
                utiles.printNice("EXPECTED:");
                System.out.println(expected[0]);
            }
        }
    }

    /** @brief Función que testea la función putAllWhiteTilesPossibilities con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testPutAllWhiteTilesPossibilities()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutAllWhiteTilesPossibilities'");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutAllWhiteTilesPossibilities'");
            utiles.printNice("El kakuro introducido es:");
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));

            s.putAllWhiteTilesPossibilities();

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla blanca, con el siguiente formato: i j");
            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            if (nums.length > 0) {
                WhiteTile wt = (WhiteTile) s.getKakuroSolved().getTile(nums[0], nums[1]);
                ArrayList<Integer> actual = wt.getPossibilities();
                utiles.printLineEqual();
                System.out.println(utiles.Centered(" ", " "));
                System.out.println(utiles.Centered("¿Qué posibilidades debería tener el vector?", " "));
                System.out.println(utiles.Centered("Indícalas en cualquier orden, sin repeticiones", " "));
                System.out.println(utiles.Centered(" ", " "));
                utiles.printLineEqual();
                line = utiles.readLine();
                int[] nums2 = utiles.convertStringtoIntArrayNums(line);
                ArrayList<Integer> expected = new ArrayList<>();
                for (int j : nums2) expected.add(j);
                boolean correct = equalSets(actual, expected);
                utiles.clearScreen();
                utiles.clearScreen();
                utiles.printLineEqual();
                System.out.println(utiles.Centered(" ", " "));
                if (correct) {
                    System.out.println(utiles.Centered("'testPutAllWhiteTilesPossibilities' funcinó correctamente", " "));
                } else {
                    System.out.println(utiles.Centered("ERROR: 'testPutAllWhiteTilesPossibilities' NO funcinó correctamente", " "));
                    System.out.println(utiles.Centered("ACTUAL: " + actual, " "));
                    System.out.println(utiles.Centered("EXPECTED: " + expected, " "));
                }
                System.out.println(utiles.Centered(" ", " "));
                utiles.printLineEqual();
                System.out.println();
            }
        }
    }

    /** @brief Función que testea la función possibleCombination con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testPossibleCombination()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'possibleCombination'");
        CtrlSolve s = new CtrlSolve();

        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        System.out.println(utiles.Centered("Introduce una posible combinación de sumandos", " "));
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();

        ArrayList<Integer> comb = new ArrayList<>();
        String line = utiles.readLine();
        int[] num = utiles.convertStringtoIntArrayNums(line);
        if(num.length>0) {
            utiles.printNice("Introduce el valor del número a comprobar (n) y el índice (k) con el formato:" +
                    " n k");
            line = utiles.readLine();
            int[] nk = utiles.convertStringtoIntArrayNums(line);

            for(int j : num) comb.add(j);
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            System.out.println(utiles.Centered("¿Se cumple que el elemento vec[k] no se repite en todo el vector y"," "));
            System.out.println(utiles.Centered("que, si k es == "+ (comb.size()-1) +", la suma del vector es igual a "+nk[0]+"?"," "));
            System.out.println(utiles.Centered("que, si k es != "+ (comb.size()-1) +", la suma del vector es menor a "+nk[0]+"?"," "));
            System.out.println(utiles.Centered("Sí[1]/No[0]"," "));
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            int a = utiles.readInt();

            boolean expected;
            expected = a == 1;

            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if(expected==s.publicPossibleCombination(comb,nk[0],nk[1])) {
                System.out.println(utiles.Centered("'possibleCombination' funcinó correctamente"," "));
            }
            else {
                System.out.println(utiles.Centered("ERROR: 'possibleCombination' NO funcinó correctamente"," "));
                System.out.println(utiles.Centered("ACTUAL: " + s.publicPossibleCombination(comb,nk[0],nk[1])," "));
                System.out.println(utiles.Centered("EXPECTED: " + expected," "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
        }
    }

    /** @brief Función que testea la función getAllCombinatios con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testGetAllCombinations()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'testGetAllCombinations'");
        CtrlSolve s = new CtrlSolve();

        utiles.printNice("Introduce el valor del número (n) y la cantidad de sumandos para obtener n así:" +
                " n cantidad_de_sumandos");
        String line = utiles.readLine();
        int[] nums = utiles.convertStringtoIntArrayNums(line);
        utiles.clearScreen();

        ArrayList<Integer> combinations = new ArrayList<>();
        for(int i = 0; i < nums[1]; ++i) combinations.add(0);
        ArrayList<Boolean> aux = new ArrayList<>();
        for(int i=0; i<9; ++i) aux.add(false);
        s.getAllCombinations(combinations, nums[0], nums[1], 0, aux);
        ArrayList<Integer> actual = new ArrayList<>();
        for(int j = 0; j<aux.size(); ++j) {
            if(aux.get(j)) actual.add(j+1);
        }
        ArrayList<Integer> expected = new ArrayList<>();

        utiles.printLineEqual();
        System.out.println(utiles.Centered("Introduce cuál debería ser el output correcto: qué números pueden formar parte" +
                " de la suma de " + nums[0] + " en " + nums[1] + " casillas.", " "));
        utiles.printLineEqual();

        line = utiles.readLine();
        nums = utiles.convertStringtoIntArrayNums(line);
        for (int num : nums) expected.add(num);

        boolean correct = equalSets(actual,expected);
        utiles.clearScreen();
        utiles.printLineEqual();
        System.out.println(utiles.Centered(" ", " "));
        if(correct) {
            System.out.println(utiles.Centered("'testGetAllCombinations' funcinó correctamente"," "));
        }
        else {
            System.out.println(utiles.Centered("ERROR: 'testGetAllCombinations' NO funcinó correctamente"," "));
            System.out.println(utiles.Centered("ACTUAL: " + actual," "));
            System.out.println(utiles.Centered("EXPECTED: " + expected," "));
        }
        System.out.println(utiles.Centered(" ", " "));
        utiles.printLineEqual();
        System.out.println("\n");
    }

    /** @brief Función que testea la función putPossibilitiesBottom con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testPutPossibilitiesBottom()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutPossibilitiesBottom'");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutPossibilitiesBottom'");
            utiles.printNice("El kakuro introducido es:");
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla negra con suma inferior, con el siguiente formato: i j");

            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            if (!s.getKakuroSolved().getTile(nums[0], nums[1]).isBlack()) {
                utiles.clearScreen();
                utiles.printNice("ERROR: No es una casilla negra");
                return;
            } else if (s.getKakuroSolved().getBottomValueBlackTile(nums[0], nums[1]) == -1) {
                utiles.clearScreen();
                utiles.printNice("ERROR: No es una casilla negra con valor hacia abajo");
                return;
            }

            ArrayList<Boolean> poss = new ArrayList<>();
            for (int l = 0; l < 9; ++l) poss.add(true);

            s.publicPutPossibilitiesBottom(nums[0], nums[1], poss,0);

            utiles.printLineEqual();
            System.out.println(utiles.Centered("Ahora, escoge qué vector quieres que esté en las casillas de abajo", " "));
            utiles.printLineEqual();


            ArrayList<Boolean> vec = new ArrayList<>();
            for (int l = 0; l < 9; ++l) vec.add(false);
            line = utiles.readLine();
            int[] nums2 = utiles.convertStringtoIntArrayNums(line);
            for (int i : nums2) vec.set(i - 1, true);
            s.publicPutPossibilitiesBottom(nums[0], nums[1], vec,0);
            WhiteTile wt = (WhiteTile) s.getKakuroSolved().getTile(nums[0] + 1, nums[1]);

            ArrayList<Integer> vecNum = new ArrayList<>();
            for (int l = 0; l < vec.size(); ++l) {
                if (vec.get(l)) vecNum.add(l + 1);
            }

            boolean correct = equalSets(vecNum, wt.getPossibilities());
            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if (correct) {
                System.out.println(utiles.Centered("'testPutPossibilitiesBottom' funcinó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testPutPossibilitiesBottom' NO funcinó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + wt.getPossibilities(), " "));
                System.out.println(utiles.Centered("EXPECTED: " + vecNum, " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            System.out.println("\n");
        }
    }

    /** @brief Función que testea la función putPossibilitiesRight con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testPutPossibilitiesRight()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutPossibilitiesRight'");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutPossibilitiesRight'");
            utiles.printNice("El kakuro introducido es:");
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y columna (j) de una casilla negra con suma hacia la derecha, con el siguiente formato: i j");

            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            if (!s.getKakuroSolved().getTile(nums[0], nums[1]).isBlack()) {
                utiles.clearScreen();
                utiles.printNice("ERROR: No es una casilla negra");
                return;
            } else if (s.getKakuroSolved().getRightValueBlackTile(nums[0], nums[1]) == -1) {
                utiles.clearScreen();
                utiles.printNice("ERROR: No es una casilla negra con valor hacia la derecha");
                return;
            }

            ArrayList<Boolean> poss = new ArrayList<>();
            for (int l = 0; l < 9; ++l) poss.add(true);

            s.publicPutPossibilitiesBottom(nums[0], nums[1], poss,0);

            utiles.printLineEqual();
            System.out.println(utiles.Centered("Ahora, escoge qué vector quieres que esté en las casillas de la derecha", " "));
            utiles.printLineEqual();

            ArrayList<Boolean> vec = new ArrayList<>();
            for (int l = 0; l < 9; ++l) vec.add(false);
            line = utiles.readLine();
            int[] nums2 = utiles.convertStringtoIntArrayNums(line);
            for (int i : nums2) vec.set(i - 1, true);
            s.publicPutPossibilitiesRight(nums[0], nums[1], vec,0);
            WhiteTile wt = (WhiteTile) s.getKakuroSolved().getTile(nums[0], nums[1] + 1);

            ArrayList<Integer> vecNum = new ArrayList<>();
            for (int l = 0; l < vec.size(); ++l) {
                if (vec.get(l)) vecNum.add(l + 1);
            }

            boolean correct = equalSets(vecNum, wt.getPossibilities());
            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if (correct) {
                System.out.println(utiles.Centered("'testPutPossibilitiesRight' funcinó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testPutPossibilitiesRight' NO funcinó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + wt.getPossibilities(), " "));
                System.out.println(utiles.Centered("EXPECTED: " + vecNum, " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            System.out.println("\n");
        }
    }

    /** @brief Función que testea la función howManySpacesBottom con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testHowManySpacesBottom()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'howManySpacesBottom'");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'testPutPossibilitiesBottom'");
            utiles.printNice("El kakuro introducido es:");
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla con suma inferior, con el siguiente formato: i j");
            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            int actual = s.howManySpacesBottom(nums[0], nums[1]);

            utiles.printNice("¿Cuántas casillas blancas hay debajo de la casilla que has seleccionado?");

            line = utiles.readLine();
            nums = utiles.convertStringtoIntArrayNums(line);

            boolean correct = (actual == nums[0]);
            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if (correct) {
                System.out.println(utiles.Centered("'testPutPossibilitiesBottom' funcinó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'testPutPossibilitiesBottom' NO funcinó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + actual, " "));
                System.out.println(utiles.Centered("EXPECTED: " + nums[0], " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            System.out.println("\n");
        }
    }

    /** @brief Función que testea la función howManySpacesRight con Kakuro introducido por fichero
    \pre <em>Cierto</em>
    \post Se ha ejecutado el testeo de la función
     */
    public static void testHowManySpacesRight()
    {
        utiles.printNice("TEST DE LA FUNCIONALIDAD 'howManySpacesRight'");
        Kakuro [] kActual = new Kakuro[1];
        int ret = utiles.getI(kActual,"data/Drivers/DriverSolve/");
        if (ret == 0) {
            CtrlSolve s = new CtrlSolve(kActual[0]);
            utiles.printNice("TEST DE LA FUNCIONALIDAD 'howManySpacesRight'");
            utiles.printNice("El kakuro introducido es:");
            System.out.println(utiles.KakuroToCentred(kActual[0].kakuroToStringNice()));

            utiles.printNice("Escoge la fila (i) y la columna (j) de la casilla con suma a la derecha, con el siguiente formato: i j");
            String line = utiles.readLine();
            int[] nums = utiles.convertStringtoIntArrayNums(line);
            int actual = s.howManySpacesRight(nums[0], nums[1]);

            utiles.printNice("¿Cuántas casillas blancas hay a la derecha de la casilla que has seleccionado?");

            line = utiles.readLine();
            nums = utiles.convertStringtoIntArrayNums(line);

            boolean correct = (actual == nums[0]);
            utiles.clearScreen();
            utiles.printLineEqual();
            System.out.println(utiles.Centered(" ", " "));
            if (correct) {
                System.out.println(utiles.Centered("'howManySpacesRight' funcinó correctamente", " "));
            } else {
                System.out.println(utiles.Centered("ERROR: 'howManySpacesRight' NO funcinó correctamente", " "));
                System.out.println(utiles.Centered("ACTUAL: " + actual, " "));
                System.out.println(utiles.Centered("EXPECTED: " + nums[0], " "));
            }
            System.out.println(utiles.Centered(" ", " "));
            utiles.printLineEqual();
            System.out.println("\n");
        }
    }

    /** @brief Función auxiliar que comprueba que dos sets de arrayLists sean iguales

    Sets en el sentido de que no importa el orden de los elementos del contenido de v1 ni v2
    \pre <em>Cierto</em>
    \post Devuelve cierto sii los sets de arrayLists són iguales
     */
    private static boolean equalSets(ArrayList<Integer> l1, ArrayList<Integer> l2)
    {
        // Primero comprobamos que l1 está contenido en l2
        boolean c1 = true;
        for(int i=0; i<l1.size() && c1; ++i) {
            c1 = l2.contains(l1.get(i));
        }
        // Ahora comprobamos que l2 está contenido en l1
        boolean c2 = true;
        for(int i=0; i<l2.size() && c2; ++i) {
            c2 = l1.contains(l2.get(i));
        }
        return c1 && c2;
    }
}
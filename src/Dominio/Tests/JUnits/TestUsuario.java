/** @file TestUsuario.java
 @brief Especificación de la clase TestUsuario
 */

package Dominio.Tests.JUnits;

import org.junit.Test;
import static org.junit.Assert.*;

import Dominio.Clases.Usuario;

/** @class TestUsuario
 @brief Tests JUnit de la clase Usuario

 Hecha por Marco Patiño
 */
public class TestUsuario
{

    @Test
    /** @brief Test de la consultora getUsername donde se prueba que devuelva el valor correctamente */
    public void getUsername()
    {
        Usuario u = new Usuario("Pepito", "contraseña");
        assertEquals("getUsername failed because", "Pepito", u.getUsername());
    }

    @Test
    /** @brief Test de la consultora getPassword donde se prueba que devuelva el valor correctamente */
    public void getPassword()
    {
        Usuario u = new Usuario("Pepito", "contraseña");
        assertEquals("getUsername failed because", "contraseña", u.getPassword());
    }

    @Test
    /** @brief Test de la función setUserName donde se prueba que se asigne correctamente el nuevo username*/
    public void setUserName()
    {
        Usuario u = new Usuario("Pepito", "contraseña");
        u.setUserName("Pepe");
        assertEquals("getUsername failed because", "Pepe", u.getUsername());
    }

    @Test
    /** @brief Test de la función setUserPass donde se prueba que se asigne correctamente la nueva contraseña*/
    public void setUserPass()
    {
        Usuario u = new Usuario("Pepito", "contraseña");
        u.setUserPass("contra");
        assertEquals("getUsername failed because", "contra", u.getPassword());
    }
}
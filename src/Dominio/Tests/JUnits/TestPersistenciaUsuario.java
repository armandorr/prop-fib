/** @file TestPersistenciaUsuario.java
 @brief Especificación de la clase TestPersistenciaUsuario
 */

package Dominio.Tests.JUnits;

import Persistencia.PersistenciaUsuario;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.Scanner;

/** @class TestPersistenciaUsuario
 @brief Tests JUnit de la clase Persistencia Usuario

 Hecha por David Marin
 */
public class TestPersistenciaUsuario
{

    @Test
    /** @brief Test de la función CreaUsuario*/
    public void testCreaUsuario()
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        pu.creaUsuarioForJUNIT("userTest", "1234");
        File f = new File("data/JUnits/testsPersistencia/Users");
        String[] pathnames = f.list();

        boolean found = false;
        for (String pathname : Objects.requireNonNull(pathnames))
            if (pathname.equals("userTest")) {
                found = true;
                break;
            }
        assertTrue("testCreaUsuario failed", found);
    }

    @Test
    /** @brief Test de la función LoginUsuario*/
    public void testLoginUsuario()
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        pu.creaUsuarioForJUNIT("userTest", "1234");
        boolean ok = pu.loginUsuarioForJUNIT("userTest", "1234");
        assertTrue("testloginUsuario failed", ok);
    }

    @Test
    /** @brief Test de la función EliminarUsuario*/
    public void testEliminarUsuario()
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        pu.creaUsuarioForJUNIT("userTest2", "12345");

        pu.eliminarUsuarioForJUNIT("userTest2");

        // esto puede ser activado si queremos que no nos quede el nombre de userTest2 en el Estado.txt, pero no tiene ningún efecto en el test
        pu.eliminarUsuarioEstadoForJUNIT("userTest2");

        File f = new File("data/JUnits/testsPersistencia/Users");
        String[] pathnames = f.list();

        boolean found = false;
        for (String pathname : Objects.requireNonNull(pathnames))
            if (pathname.equals("userTest2")) {
                found = true;
                break;
            }
        assertFalse("testEliminarUsuario failed", found);
    }

    @Test
    /** @brief Test de la función GetPass*/
    public void testGetPass()
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        String expected = "1234";
        pu.creaUsuarioForJUNIT("userTest", "1234");
        String actual = pu.getPasswordForJUNIT("userTest");

        assertEquals("testGetPass failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función EliminarUsuarioEstado*/
    public void testEliminarUsuarioEstado() throws FileNotFoundException
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        pu.creaUsuarioForJUNIT("userTest3", "12345");

        pu.eliminarUsuarioEstadoForJUNIT("userTest3");
        // esto puede ser activado si queremos que no nos quede la carpeta de userTest3, pero no tiene ningún efecto en el test
        pu.eliminarUsuarioForJUNIT("userTest3");

        File f = new File("data/JUnits/testsPersistencia/Estado.txt");
        Scanner scan = new Scanner(f);
        scan.nextLine();
        boolean found = false;
        while(scan.hasNextLine()) {
            String lin = scan.nextLine();
            if(lin.contains("usertTest")) found = true;
        }
        assertFalse("testEliminarUsuarioEstado failed", found);
    }

    @Test
    /** @brief Test de la función ChangePass*/
    public void testChangePass() throws FileNotFoundException
    {
        PersistenciaUsuario pu = new PersistenciaUsuario();
        pu.creaUsuarioForJUNIT("userTest4", "1234");
        String expected = "abcd";

        pu.changePassForJUNIT("userTest4", expected);

        File f = new File("data/JUnits/testsPersistencia/Users/userTest4/infoUser/info.txt");
        Scanner scan = new Scanner(f);
        String actual = scan.nextLine();

        assertEquals("testGetPass failed because",expected,actual);
    }
}
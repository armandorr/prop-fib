/** @file TestKakuro.java
 @brief Especificación de la clase TestKakuro
 */

package Dominio.Tests.JUnits;

import Dominio.Clases.BlackTile;
import Dominio.Clases.Kakuro;
import Dominio.Clases.Tile;
import Dominio.Clases.WhiteTile;
import Dominio.Controladores.CtrlDominio;

import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.*;

/** @class TestKakuro
    @brief Tests JUnit de la clase Kakuro

    Hecha por Marco Patiño
 */
public class TestKakuro
{

    @Test
    /** @brief Test de la función copyConstructor donde se prueba que obtenga bien la copia*/
    public void TestKakuroCopyConstructor()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        Kakuro k2 = new Kakuro(k);
        boolean actual = k.isEqualWhiteTiles(k2);
        assertTrue("Test copy constructor failed because", actual);
    }

    @Test
    /** @brief Test de la consultora getDificultad donde se prueba que devuelva el valor correctamente */
    public void TestGetDificultad()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        assertEquals("Test get dificultad failed", 4, k.getDificultad());
    }

    @Test
    /** @brief Test de la consultora getId donde se prueba que devuelva el valor correctamente */
    public void TestGetId()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        assertEquals("Test get id failed", 45, k.getId());
    }

    @Test
    /** @brief Test de la función isEqualWhiteTiles donde se prueba que dos Kakuros sean iguales respecto a los valores en las casillas blancas*/
    public void TestIsEqualWhiteTiles()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        Kakuro k2;
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k2 = CD.getKakuroDominio();
        boolean actual = k.isEqualWhiteTiles(k2);
        assertTrue("Test equal kakuros failed because", actual);
    }

    @Test
    /** @brief Test de la función getTile donde se prueba que se obtenga una blackTile*/
    public void TestGetBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        BlackTile bt = (BlackTile) k.getTile(0,1);

        int btR = bt.getRightVal();
        int btB = bt.getBottomVal();

        assertEquals("Test get black tile failed because", -1, btR);
        assertEquals("Test get black tile failed because", 41, btB);
    }

    @Test
    /** @brief Test de la función getTile donde se prueba que se obtenga una whiteTile*/
    public void TestGetWhiteTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10_sol.txt");
        k = CD.getKakuroDominio();
        WhiteTile wt = (WhiteTile)k.getTile(1,1);

        int wtV = wt.getValue();

        assertEquals("Test get white tile failed because", 9, wtV);
    }

    @Test
    /** @brief Test de la función getKakuroRows donde se prueba que se obtenga correctamente el número de filas de un Kakuro*/
    public void TestGetKakuroRows()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        int actual = k.getKakuroRows();
        assertEquals("Test get rows failed because", 12, actual);
    }

    @Test
    /** @brief Test de la función getKakuroCols donde se prueba que se obtenga correctamente el número de columnas de un Kakuro*/
    public void TestGetKakuroCols()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        int actual = k.getKakuroCols();
        assertEquals("Test get cols failed because", 10, actual);
    }

    @Test
    /** @brief Test de la función getValueTile donde se prueba que se obtenga correctamente el número de una casilla cualquiera*/
    public void TestGetValueTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10_sol.txt");
        k = CD.getKakuroDominio();
        int actual = k.getValueTile(1,3);
        assertEquals("Test get value white tile failed because", 2,actual);
        actual = k.getValueTile(0,0);
        assertEquals("Test get value black tile failed because", -1,actual);
    }

    @Test
    /** @brief Test de la función getRightValueBlackTile donde se prueba que se obtenga correctamente el número de la derecha de una casilla negra*/
    public void TestGetRightValueBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        int actual = k.getRightValueBlackTile(1,0);
        assertEquals("Test get right value black tile failed because", 19, actual);
        actual = k.getRightValueBlackTile(0,1);
        assertEquals("Test get right value white tile failed because", -1, actual);
    }

    @Test
    /** @brief Test de la función getBotttomValueBlackTile donde se prueba que se obtenga correctamente el número de debajo de una casilla negra*/
    public void TestGetBottomValueBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        int actual = k.getBottomValueBlackTile(1,0);
        assertEquals("Test get bottom value black tile failed because", -1, actual);
        actual = k.getBottomValueBlackTile(0,1);
        assertEquals("Test get bottom value black tile failed because", 41, actual);
    }

    @Test
    /** @brief Test de la función isBlackKakuroTile donde se prueba que se obtenga correctamente si una casilla es negra o blanca*/
    public void TestIsBlackKakuroTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        boolean actual = k.isBlackKakuroTile(0, 0);
        assertTrue("Test is black tile in black tile failed because", actual);
        actual = k.isBlackKakuroTile(1,1);
        assertFalse("Test is black tile in white tile failed because", actual);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test
    /** @brief Test de la función dfs donde se prueba que se obtenga correctamente si se han visitado de las casillas connexas o no recursivamente*/
    public void TestDfs()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        boolean[][] vis= new boolean[12][10];
        for(int x=0; x<12; ++x){ for (int z=0; z<10; ++z) {vis[x][z] = false;}}
        boolean[][] expected = new boolean[12][10];
        for(int x=0; x<12; ++x) for (int z=0; z<10; ++z){
            expected[x][z] = !k.isBlackKakuroTile(x, z);
        }
        k.dfsPublic(11,9,vis);
        Arrays.deepEquals(vis, expected);
        assertArrayEquals("Test 1 DFS failed because", expected, vis);

        CD.readKakuroFromFile("data/JUnits/KakuroNoConexo12_10.txt");
        k = CD.getKakuroDominio();
        for(int x=0; x<12; ++x){ for (int z=0; z<10; ++z) {vis[x][z] = false;}}
        expected = new boolean[12][10];
        for(int x=0; x<12; ++x) for (int z=0; z<10; ++z){
            expected[x][z] = !k.isBlackKakuroTile(x, z);
        }
        expected[1][1] = false; expected[1][2] = false; expected[1][3] = false;
        expected[2][1] = false; expected[2][2] = false;
        k.dfsPublic(11,9,vis);
        Arrays.deepEquals(vis, expected);
        assertArrayEquals("Test 2 DFS failed because", expected, vis);
    }

    @Test
    /** @brief Test de la función connectedKakuro donde se prueba que se obtenga correctamente si un Kakuro es o no connexo*/
    public void TestConnectedKakuro()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNoConexo12_10.txt");
        k = CD.getKakuroDominio();
        boolean check = k.connectedKakuro();
        assertFalse("Test 1 connectivity in disconnected Kakuro failed because", check);

        Kakuro k2;
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k2 = CD.getKakuroDominio();

        check = k2.connectedKakuro();
        assertTrue("Test 2 connectivity in connected Kakuro failed because", check);
    }

    @Test
    /** @brief Test 1 de la función SetID donde se prueba que se asigne correctamente el nuevo ID*/
    public void TestSetID()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setID(5);

        assertEquals("TestSetID failed because",5,k.getId());
    }

    @Test
    /** @brief Test 1 de la función setWhiteTile donde se prueba que se coloque correctamente una casilla blanca*/
    public void TestSetWhiteTile1()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        WhiteTile wt = new WhiteTile(5);
        k.setWhiteTile(1,2,wt);
        int actual =  k.getValueTile(1,2);

        assertEquals("Test 1 set white tile failed because",5,actual);
    }

    @Test
    /** @brief Test 2 de la función setWhiteTile donde se prueba que se coloque correctamente una casilla blanca*/
    public void TestSetWhiteTile2()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(1,2);
        int actual =  k.getValueTile(1,2);

        assertEquals("Test 2 set white tile failed because",0,actual);
    }

    @Test
    /** @brief Test 3 de la función setWhiteTile donde se prueba que se coloque correctamente una casilla blanca*/
    public void TestSetWhiteTile3()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(1,2,5);
        int actual =  k.getValueTile(1,2);

        assertEquals("Test 3 set white tile failed because", 5,actual);
    }

    @Test
    /** @brief Test de la función setBlackTile donde se prueba que se coloque correctamente una casilla negra*/
    public void TestSetBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setBlackTile(1,2);
        int actual =  k.getValueTile(1,2);
        BlackTile bt = new BlackTile((BlackTile)k.getTile(1,2));
        int btRActual = bt.getRightVal();
        int btBActual = bt.getBottomVal();

        assertEquals("Test 1.1 set black tile failed because", -1,actual);
        assertEquals("Test 1.2 set black tile failed because", -1,btRActual);
        assertEquals("Test 1.3 set black tile failed because", -1,btBActual);
    }

    @Test
    /** @brief Test de la función setBottomBlackTile donde se prueba que se coloque correctamente el valor bottom en una casilla negra*/
    public void TestSetBottomBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setBottomBlackTile(0,1, 15);
        int actual =  k.getValueTile(0,1);
        BlackTile bt = new BlackTile((BlackTile)k.getTile(0,1));
        int btRActual = bt.getRightVal();
        int btBActual = bt.getBottomVal();

        assertEquals("Test 2.1 set bottom black tile failed because", -1,actual);
        assertEquals("Test 2.2 set bottom black tile failed because", -1,btRActual);
        assertEquals("Test 2.3 set bottom black tile failed because", 15,btBActual);
    }

    @Test
    /** @brief Test de la función setBRightBlackTile donde se prueba que se coloque correctamente el valor right en una casilla negra*/
    public void TestSetRightBlackTile()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setRightBlackTile(0,1, 15);
        int actual =  k.getValueTile(0,1);
        BlackTile bt = new BlackTile((BlackTile)k.getTile(0,1));
        int btRActual = bt.getRightVal();
        int btBActual = bt.getBottomVal();

        assertEquals("Test 3.1 set right black tile failed because", -1,actual);
        assertEquals("Test 3.2 set right black tile failed because", 15,btRActual);
        assertEquals("Test 3.3 set right black tile failed because", 41,btBActual);
    }

    @Test
    /** @brief Test de la función cleanKakuro donde se prueba que se limpien de valores las casillas blancas de un Kakuro*/
    public void TestCleanKakuro()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.cleanKakuro(10,12);
        Kakuro k2 = new Kakuro(10, 12);
        boolean actual = k.isEqualWhiteTiles(k2);

        assertTrue("Test clean kakuro failed because", actual);
    }

    @Test
    /** @brief Test de la función cleanKakuro donde se prueba que se limpien de valores las casillas blancas de un Kakuro*/
    public void TestIsKakuroCompleted()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        Kakuro k2;
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10_sol.txt");
        k2 = CD.getKakuroDominio();

        assertTrue("Test TestIsKakuroCompleted failed because", k2.isKakuroCompleted());
        assertFalse("Test TestIsKakuroCompleted failed because", k.isKakuroCompleted());
    }

    @Test
    /** @brief Test 1 de la función tryToWrite donde se prueba si se puede poner una casilla blanca con un determiando valor*/
    public void TestTryToWrite1()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        boolean actual = (k.tryToWrite(2,3,5,1)==0);
        assertTrue("Test try to write in white tile failed because", actual);
    }

    @Test
    /** @brief Test 2 de la función tryToWrite donde se prueba si se puede poner una casilla blanca con un determiando valor*/
    public void testTryToWrite2()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        boolean actual = (k.tryToWrite(0,0,5,1)==0);
        assertFalse("Test try to write in black tile failed because", actual);
    }

    @Test
    /** @brief Test 3 de la función tryToWrite donde se prueba si se puede poner una casilla blanca con un determiando valor*/
    public void testTryToWrite3()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        k.tryToWrite(2,3,5,1);
        int actual = k.getValueTile(2,3);
        int expected = 5;
        assertEquals("Test try to write in white tile also writes failed because",expected,actual);
    }

    @Test
    /** @brief Test 4 de la función tryToWrite donde se prueba si se puede poner una casilla blanca con un determiando valor*/
    public void testTryToWrite4()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        boolean actual = (k.tryToWrite(15,15,5,1)==0);
        assertFalse("Test try to write out of the board failed because", actual);
    }

    @Test
    /** @brief Test 1 de la función columnCheck con modo 0 donde se prueba si se puede poner una casilla
        blanca con un determiando valor sin que haya repeticiones en la columna y se cumplan restricciones definidas por el modo*/
    public void TestColumnCheckMode1()
    {
        int modo = 0; //resolviendo-> repetidas + mira si la suma se pasa lo antes posible
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test column check mode 0 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertFalse("Test column check mode 0 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test column check mode 0 value correct failed because", actual);
    }

    @Test
    /** @brief Test 2 de la función columnCheck con modo 0 donde se prueba si se puede poner una casilla
    blanca con un determiando valor sin que haya repeticiones en la columna y se cumplan restricciones definidas por el modo*/
    public void TestColumnCheckMode2()
    {
        int modo = 1; //jugando-> repetidas + mira si la suma diferente al final
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test column check mode 1 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertTrue("Test column check mode 1 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test column check mode 1 value correct failed because", actual);
        k.setWhiteTile(6,5,3);
        actual = k.columnCheck(7, 5, 5, modo)==0;
        assertFalse("Test column check mode 1 value different at the final failed because", actual);
    }

    @Test
    /** @brief Test 3 de la función columnCheck con modo 0 donde se prueba si se puede poner una casilla
    blanca con un determiando valor sin que haya repeticiones en la columna y se cumplan restricciones definidas por el modo*/
    public void TestColumnCheckMode3()
    {
        int modo = 2; //creando -> repetidas
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test column check mode 2 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertTrue("Test column check mode 2 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test column check mode 2 value correct failed because", actual);
        k.setWhiteTile(6,5,3);
        actual = k.columnCheck(7, 5, 5, modo)==0;
        assertTrue("Test column check mode 2 value different at the final failed because", actual);
    }

    @Test
    /** @brief Test 1 de la función rowCheck con modo 0 donde se prueba si se puede poner una casilla
    blanca con un determiando valor sin que haya repeticiones en la fila y se cumplan restricciones definidas por el modo*/
    public void TestRowCheckMode1()
    {
        int modo = 0; //resolviendo-> repetidas + mira si la suma se pasa lo antes posible
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test row check mode 0 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertFalse("Test row check mode 0 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test row check mode 0 value correct failed because", actual);
    }

    @Test
    /** @brief Test 2 de la función rowCheck con modo 0 donde se prueba si se puede poner una casilla
    blanca con un determiando valor sin que haya repeticiones en la fila y se cumplan restricciones definidas por el modo*/
    public void TestRowCheckMode2()
    {
        int modo = 1; //jugando-> repetidas + mira si la suma diferente al final
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test row check mode 1 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertTrue("Test row check mode 1 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test row check mode 1 value correct failed because", actual);
        k.setWhiteTile(6,5,3);
        actual = k.columnCheck(7, 5, 5, modo)==0;
        assertFalse("Test row check mode 1 value different at the final failed because", actual);
    }

    @Test
    /** @brief Test 3 de la función rowCheck con modo 0 donde se prueba si se puede poner una casilla
    blanca con un determiando valor sin que haya repeticiones en la fila y se cumplan restricciones definidas por el modo*/
    public void TestRowCheckMode3()
    {
        int modo = 2; //creando -> repetidas
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();
        k.setWhiteTile(5,5,2);
        boolean actual = k.columnCheck(6, 5, 2, modo)==0;
        assertFalse("Test row check mode 2 repeated failed because", actual);
        actual = k.columnCheck(6, 5, 9, modo)==0;
        assertTrue("Test row check mode 2 value excedeed failed because", actual);
        actual = k.columnCheck(6, 5, 1, modo)==0;
        assertTrue("Test row check mode 2 value correct failed because", actual);
        k.setWhiteTile(6,5,3);
        actual = k.columnCheck(7, 5, 5, modo)==0;
        assertTrue("Test row check mode 2 value different at the final failed because", actual);
    }

    @Test
    /** @brief Test de la función convertToTile donde se prueba que la conversión a Tile desde un string sea correcta*/
    public void TestConvertToTileForJUNIT()
    {
        String Blanca = "?";
        String Negra = "*";
        String Col = "C5";
        String Row = "F27";
        String ColRow = "C18F2";

        Kakuro k = new Kakuro();

        WhiteTile TileBlanca = (WhiteTile) k.convertToTileForJUNIT(Blanca,-1,-1);
        BlackTile TileNegra = (BlackTile) k.convertToTileForJUNIT(Negra,-1,-1);
        BlackTile TileCol = (BlackTile) k.convertToTileForJUNIT(Col,-1,-1);
        BlackTile TileRow = (BlackTile) k.convertToTileForJUNIT(Row,-1,-1);
        BlackTile TileColRow = (BlackTile) k.convertToTileForJUNIT(ColRow,-1,-1);

        assertEquals("Test convert string to white tile failed because", 0, TileBlanca.getValue());
        assertEquals("Test 1 convert string to complete black tile failed because", -1, TileNegra.getBottomVal());
        assertEquals("Test 2 convert string to complete black tile failed because", -1, TileNegra.getRightVal());
        assertEquals("Test 1 convert string to black column failed because", 5, TileCol.getBottomVal());
        assertEquals("Test 2 convert string to black column failed because", -1, TileCol.getRightVal());
        assertEquals("Test 1 convert string to black row failed because", -1, TileRow.getBottomVal());
        assertEquals("Test 2 convert string to black row failed because", 27, TileRow.getRightVal());
        assertEquals("Test 1 convert string to black with col&row failed because", 18, TileColRow.getBottomVal());
        assertEquals("Test 2 convert string to black with col&row failed because", 2, TileColRow.getRightVal());
    }

    @Test
    /** @brief Test de la función readKakuro donde se prueba que el Kakuro se haya leído correctamente*/
    public void TestReadKakuro()
    {
        ArrayList<ArrayList<Tile>> b = new ArrayList<>();
        ArrayList<Tile> b1 = new ArrayList<>();
        b1.add(new BlackTile());
        b1.add(new BlackTile(-1, 3));
        b1.add(new BlackTile(-1, 3));
        ArrayList<Tile> b2 = new ArrayList<>();
        b2.add(new BlackTile(3,-1));
        b2.add(new WhiteTile());
        b2.add(new WhiteTile());
        ArrayList<Tile> b3 = new ArrayList<>();
        b3.add(new BlackTile(3,-1));
        b3.add(new WhiteTile());
        b3.add(new WhiteTile());
        b.add(b1); b.add(b2); b.add(b3);
        Kakuro k = new Kakuro(b);

        Kakuro k2;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroTrivial3_3.txt");
        k2 = CD.getKakuroDominio();


        boolean actual = k.isEqualWhiteTiles(k2);

        assertTrue("Test read Kakuro from a file failed because", actual);
    }

    @Test
    /** @brief Test de la función printKakuro donde se prueba que el Kakuro se haya dibujado correctamente en nuestro formato*/
    public void TestPrintKakuro()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroTrivial3_3.txt");
        k = CD.getKakuroDominio();

        String s = "      0     1     2\n" +
                "   -------------------\n" +
                "0  |#####|3  ↓ |3  ↓ |\n" +
                "   -------------------\n" +
                "1  |3  → |     |     |\n" +
                "   -------------------\n" +
                "2  |3  → |     |     |\n" +
                "   -------------------\n";


        String s2 = k.kakuroToStringNice();
        assertEquals("Test print Kakuro with personalized view failed because", s2, s);
    }

    @Test
    /** @brief Test de la función doMatrixFromKakuro donde se prueba que el Kakuro se se haya convertido correctamente a una matriz de strings*/
    public void TestDoMatrixFromKakuro()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroTrivial3_3.txt");
        k = CD.getKakuroDominio();

        String[][] s = k.doMatrixFromKakuro();
        String[][] expected = new String[3][3];
        expected [0][0] =  "#";
        expected [0][1] = "3  ↓ ";
        expected [0][2] = "3  ↓ ";
        expected [1][0] = "3  → ";
        expected [1][1] = "";
        expected [1][2] = "";
        expected [2][0] = "3  → ";
        expected [2][1] = "";
        expected [2][2] = "";

        boolean eq = true;
        for(int i=0; i<s.length; ++i) {
            for(int j=0; j<s[0].length; ++j)
                if (!s[i][j].equals(expected[i][j])) {
                    eq = false;
                    break;
                }
        }

        assertTrue("Test print Kakuro with personalized view failed because", eq);
    }

    @Test
    /** @brief Test de la función printKakuroStandard donde se prueba que el Kakuro se haya dibujado correctamente en formato oficial*/
    public void TestPrintKakuroStandard()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroTrivial3_3.txt");
        k = CD.getKakuroDominio();
        String s = "3,3\n" +
                "*,C3,C3\n" +
                "F3,?,?\n" +
                "F3,?,?\n" +
                "0\n" +
                "45\n";
        String s2 = k.kakuroToStringStandard(true);

        assertEquals("Test print Kakuro with standard view failed because", s, s2);
    }

    @Test
    /** @brief Test de la función readKakuroFromString donde se prueba que el Kakuro se ha leído correctamente de un string*/
    public void TestReadKakuroFromString()
    {
        String actStr = "3,3\n" +
                "*,C3,C3\n" +
                "F3,?,?\n" +
                "F3,?,?\n" +
                "-1\n" +
                "-1";
        Kakuro actual = new Kakuro();
        actual.readKakuroFromString(actStr);

        ArrayList<ArrayList<Tile>> b = new ArrayList<>();
        ArrayList<Tile> b1 = new ArrayList<>();
        b1.add(new BlackTile());
        b1.add(new BlackTile(-1, 3));
        b1.add(new BlackTile(-1, 3));
        ArrayList<Tile> b2 = new ArrayList<>();
        b2.add(new BlackTile(3,-1));
        b2.add(new WhiteTile());
        b2.add(new WhiteTile());
        ArrayList<Tile> b3 = new ArrayList<>();
        b3.add(new BlackTile(3,-1));
        b3.add(new WhiteTile());
        b3.add(new WhiteTile());
        b.add(b1); b.add(b2); b.add(b3);
        Kakuro expected = new Kakuro(b);

        assertEquals("Test read Kakuro from String failed because", expected.kakuroToStringStandard(true), actual.kakuroToStringStandard(true));
    }

    @Test
    /** @brief Test de la función CalculaDificultad donde se prueba que el Kakuro calcule correctamente su dificultad estimada*/
    public void TestCalculaDificultad()
    {
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/KakuroTrivial3_3.txt");
        k = CD.getKakuroDominio();

        assertEquals("TestCalculaDificultad failed because", 1, k.calculaDificultad());

        CD.readKakuroFromFile("data/JUnits/KakuroNormal12_10.txt");
        k = CD.getKakuroDominio();

        assertEquals("TestCalculaDificultad failed because", 2, k.calculaDificultad());
    }
}
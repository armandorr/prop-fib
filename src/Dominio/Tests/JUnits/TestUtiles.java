/** @file TestUtiles.java
 @brief Especificación de la clase TestUtiles
 */

package Dominio.Tests.JUnits;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;
import Dominio.Controladores.CtrlDominio;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.Scanner;

/** @class TestUtiles
    @brief Tests JUnit de la clase utiles

    Hecha por Hasnain Shafqat
 */
public class TestUtiles
{

    /** @brief Variable de la clase utiles para facilitar tareas*/
    static Utiles utiles = new Utiles();

    @Test
    /** @brief Test de la función treatInput donde se prueba que obtenga bien un número*/
    public void TesttreatInputNum()
    {
        try {
            File f = new File("data/JUnits/TreatInputNum.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            int x = utiles.treatInput(line);
            assertEquals("Test treat Input Num failed beacause", 0, x);
        }
        catch(FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }

    @Test
    /** @brief Test de la función treatInput donde se prueba que obtenga bien la Q (usada para salir)*/
    public void TesttreatInputQ()
    {
        try {
            File f = new File("data/JUnits/TreatInputQ.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            int x = utiles.treatInput(line);
            assertEquals("Test treat Input Q failed beacause", -1, x);
        }
        catch(FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }

    @Test
    /** @brief Test de la función treatInput donde se prueba que obtenga bien la R (usada para refrescar)*/
    public void TesttreatInputR()
    {
        try {
            File f = new File("data/JUnits/TreatInputR.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            int x = utiles.treatInput(line);
            assertEquals("Test treat Input R failed beacause", -2, x);
        }
        catch(FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }

    @Test
    /** @brief Test de la función treatInput donde se prueba que obtenga bien una letra cualquiera no útil*/
    public void TesttreatInputABC()
    {
        try {
            File f = new File("data/JUnits/TreatInputABC.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            int x = utiles.treatInput(line);
            assertEquals("Test treat Input ABC failed beacause", -3, x);
        }
        catch(FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }
        
    @Test
    /** @brief Test de la función wellTabed donde se prueba que se obtenga un buen formato de escriutura*/
    public void TestwellTabed()
    {
        try {
            File f = new File("data/JUnits/WellTabed.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            String expect = "|   Prueba                                                                                                             |";
            String actual = utiles.wellTabed(line, "|");
            assertEquals("Test Well Tabed failed beacause", expect, actual);
        }
        catch(FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }

    @Test
    /** @brief Test de la función centered donde se prueba que se obtenga un buen formato de escriutura centrado*/
    public void TestCentered()
    {
        try {
            String expected = "|                                                 Tiene solución única                                                 |";
            File f = new File("data/JUnits/Centered.txt");
            Scanner lect = new Scanner(f);
            String line = lect.nextLine();
            String actual = utiles.Centered(line, " ");
            assertEquals("Test Centered failed beacause", expected, actual);
        }
        catch (FileNotFoundException e) {
            utiles.clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(utiles.wellTabed("|     ERROR:", "|"));
            System.out.println(utiles.wellTabed("|        Archivo no encontrado", "|"));
            System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
    }

    @Test
    /** @brief Test de la función kakuroToCentered donde se prueba que se obtenga un buen formato de escriutura de un Kakuro centrado*/
    public void TestKakurotoCentered()
    {
        String expected =   "|                                    0     1     2     3     4     5     6     7     8                                 |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              0  |#####|#####|19 ↓ |12 ↓ |#####|#####|#####|7  ↓ |10 ↓ |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              1  |#####|14 → |     |     |4  ↓ |11 ↓ |17  4|     |     |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              2  |#####|7  36|     |     |     |     |     |     |     |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              3  |12 → |     |     |10 → |     |     |     |25 ↓ |14 ↓ |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              4  |3  → |     |     |20 ↓ |11 20|     |     |     |     |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              5  |17 → |     |     |     |     |8  ↓ |6  → |     |     |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              6  |#####|11 ↓ |7  13|     |     |     |4  10|     |     |                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              7  |28 → |     |     |     |     |     |     |     |#####|                              |\n" +
                            "|                                 -------------------------------------------------------                              |\n" +
                            "|                              8  |6  → |     |     |#####|#####|8  → |     |     |#####|                              |\n" +
                            "|                                 -------------------------------------------------------                              |";
        Kakuro k;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/0_Kakuro_9_9_MEDIUM_1SOL.txt");
        k = CD.getKakuroDominio();

        String actual = utiles.KakuroToCentred(k.kakuroToStringNice());
        assertEquals("Test Centered failed beacause", expected, actual);
    }

    @Test
    /** @brief Test de la función getI
    Se comprueba que la función que utilizamos para leer un Kakuro como entrada en un sistema de fichero sea correcta
     */
    public void TestGetI()
    {
        Kakuro[] k = new Kakuro[1];
        k[0]= new Kakuro();
        String direc = "data/JUnits/";
        getICopia(k,direc);
        String actual = k[0].kakuroToStringNice();

        Kakuro k2;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/0_Kakuro_9_9_MEDIUM_1SOL.txt");
        k2 = CD.getKakuroDominio();

        String expected = k2.kakuroToStringNice();
        assertEquals("Test TestGetI failed because",expected, actual);
    }

    @Test
    /** @brief Test de la función getIO
    Se comrueba que la función que utilizamos para leer un Kakuro como entrada en un sistema de fichero
    y seleccionar un output deseado sea correcta
     */
    public void TestGetIO()
    {
        Kakuro[] k = new Kakuro[1];
        k[0]= new Kakuro();
        String direc = "data/JUnits/";
        String[] out = {""};
        getIOCopia(k, out, direc);
        String in = k[0].kakuroToStringNice();

        Kakuro k2;
        CtrlDominio CD = new CtrlDominio();
        CD.readKakuroFromFile("data/JUnits/0_Kakuro_9_9_MEDIUM_1SOL.txt");
        k2 = CD.getKakuroDominio();
        String expected = k2.kakuroToStringNice();

        Kakuro k3 = new Kakuro();
        k3.readKakuroFromString(out[0]);
        k3.kakuroToStringStandard(true);
        String kakOut = k3.kakuroToStringNice();
        assertEquals("Test TestGetIO in failed because", expected, in);
        assertEquals("Test TestGetIO out failed because", expected, kakOut);
    }

    /** @brief Función getI copiada y modificada para el test
        Hemos tenido que hacer eso para que no imprima por pantalla y elija un kakuro predefinido.*/
    public static int getICopia(Kakuro[] kActual, String direc)
    {
        int ret = 0;
        String[] pathnames = null;
        boolean refresca = true;
        int num = 0;
        while (refresca) {
            refresca = false;
            File f = new File(direc);
            pathnames = f.list();
            num = 0; //cogemos el primer archivo
        }
        if (num < Objects.requireNonNull(pathnames).length)
        {
            CtrlDominio CD = new CtrlDominio();
            CD.readKakuroFromFile(direc + pathnames[num]);
            kActual[0] = CD.getKakuroDominio();
        }
        return ret;
    }

    /** @brief Función getIO copiada y modificada para el test
    Hemos tenido que hacer eso para que no imprima por pantalla y elija un kakuro predefinido.*/
    public static int getIOCopia(Kakuro[] kActual, String[] expected, String direc)
    {
        int ret = 0;
        try {
            boolean refresca = true;
            while (refresca) {
                refresca = false;
                String[] pathnames;
                File f = new File(direc);
                pathnames = f.list();
                int num = 0; //cogemos el primer archivo
                if (num < Objects.requireNonNull(pathnames).length){
                    CtrlDominio CD = new CtrlDominio();
                    CD.readKakuroFromFile(direc + pathnames[num]);
                    kActual[0] = CD.getKakuroDominio();
                    refresca = true;
                    while (refresca) {
                        refresca = false;
                        num = 0; //cogemos el primer archivo
                        File out = new File(direc + pathnames[num]);
                        Scanner scan = new Scanner(out);
                        while (scan.hasNext()) {
                            expected[0] += scan.nextLine() + "\n";
                        }
                    }
                    refresca = false;
                }
            }
        }
        catch(FileNotFoundException ignored) {}
        return ret;
    }
}
/** @file TestPersistenciaKakuro.java
 @brief Especificación de la clase TestPersistenciaKakuro
 */

package Dominio.Tests.JUnits;

import Persistencia.PersistenciaKakuro;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/** @class TestPersistenciaKakuro
 @brief Tests JUnit de la clase persistencia Kakuro

 Hecha por David Marin
 */
public class TestPersistenciaKakuro
{

    @Test
    /** @brief Test de la función ReadKakuro*/
    public void testReadKakuro()
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        String actual = pk.readKakuro("data/JUnits/testsPersistencia/testReadKakuro.txt");
        String expected = "12,10\n" +
                "*,C41,C29,C3,*,C23,C21,*,C9,C11\n" +
                "F19,?,?,?,C3F9,?,?,F3,?,?\n" +
                "F21,?,?,?,?,?,?,C21F11,?,?\n" +
                "F12,?,?,C4F28,?,?,?,?,?,*\n" +
                "F7,?,?,?,*,C7F15,?,?,C41,C29\n" +
                "F6,?,?,?,C22F16,?,?,?,?,?\n" +
                "F9,?,?,C13F11,?,?,?,C7F3,?,?\n" +
                "F34,?,?,?,?,?,F23,?,?,?\n" +
                "*,*,C6F4,?,?,C13,C14F7,?,?,?\n" +
                "*,C14F15,?,?,?,?,?,C8F13,?,?\n" +
                "F11,?,?,F38,?,?,?,?,?,?\n" +
                "F6,?,?,F8,?,?,F10,?,?,?\n" +
                "4\n" +
                "45\n";
        assertEquals("testReadKakuro failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función GuardarKakuroEnRepositorioNoSol 1 */
    public void testGuardarKakuroEnRepositorioNoSol1() throws FileNotFoundException
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        String expected = "6,4\n" +
                "*,C27,C31,C26\n" +
                "F13,?,?,?\n" +
                "F21,?,?,?\n" +
                "F15,?,?,?\n" +
                "F20,?,?,?\n" +
                "F15,?,?,?\n" +
                "1\n" +
                "63\n";
        pk.guardarKakuroEnRepositorioForJUNIT("actualGuardarNoSol", expected, false);
        File f = new File("data/JUnits/testsPersistencia/Repositorio/actualGuardarNoSol.txt");
        Scanner scan = new Scanner(f);
        StringBuilder actual = new StringBuilder();
        while(scan.hasNextLine()) actual.append(scan.nextLine()).append("\n");

        assertEquals("testGuardarKakuroEnRepositorioNoSol failed because",expected, actual.toString());
    }

    @Test
    /** @brief Test de la función GuardarKakuroEnRepositorioNoSol 2*/
    public void testGuardarKakuroEnRepositorioNoSol2() throws FileNotFoundException
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        String expected = "3,3\n" +
                "*,C3,C3\n" +
                "F3,1,2\n" +
                "F3,2,1\n" +
                "1\n" +
                "46\n";
        pk.guardarKakuroEnRepositorioForJUNIT("actualGuardarEsSol", expected, true);
        File f = new File("data/JUnits/testsPersistencia/Repositorio/actualGuardarEsSol.sol");
        Scanner scan = new Scanner(f);
        StringBuilder actual = new StringBuilder();
        while(scan.hasNextLine()) actual.append(scan.nextLine()).append("\n");

        assertEquals("testGuardarKakuroEnRepositorioNoSol failed because",expected, actual.toString());
    }

    @Test
    /** @brief Test de la función SetLastKakuroID*/
    public void testSetLastKakuroID() throws FileNotFoundException
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        int expected = 69;
        pk.setLastKakuroIDForJUNIT(expected);
        File f = new File("data/JUnits/testsPersistencia/Estado.txt");
        Scanner scan = new Scanner(f);
        int actual = Integer.parseInt(scan.nextLine());

        assertEquals("testSetLastKakuroID failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función LeerTiempoPartidaActual*/
    public void testLeerTiempoPartidaActual()
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        int expected = 28000;
        int actual = pk.leerTiempoPartidaActual("data/JUnits/testsPersistencia/testLeerTiempo.txt");

        assertEquals("testLeerTiempoPartidaActual failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función GetLastKakuroID*/
    public void testGetLastKakuroID() throws FileNotFoundException
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        File f = new File("data/JUnits/testsPersistencia/Estado.txt");
        Scanner scan = new Scanner(f);
        int expected = Integer.parseInt(scan.nextLine());

        int actual = pk.getLastKakuroIDForJUnit();

        assertEquals("testLeerTiempoPartidaActual failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función GetPuntosGameAcabado*/
    public void testGetPuntosGameAcabado()
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        int expected = 6969;
        int actual = pk.getPuntosGameAcabado("data/JUnits/testsPersistencia/testGetPuntos.txt");

        assertEquals("testLeerTiempoPartidaActual failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función SetDataToKakuro*/
    public void testSetDataToKakuro() throws IOException
    {
        StringBuilder old = new StringBuilder();
        PersistenciaKakuro pk = new PersistenciaKakuro();
        ArrayList<Integer> expected = new ArrayList<>();
        expected.add(3); expected.add(70);
        pk.setDataToKakuro(expected.get(1), expected.get(0), "data/JUnits/testsPersistencia/testSetData.txt");
        ArrayList<Integer> actual = new ArrayList<>(2);
        File f = new File("data/JUnits/testsPersistencia/testSetData.txt");
        Scanner scan = new Scanner(f);
        old.append(scan.nextLine()).append("\n");
        int n = Integer.parseInt(old.toString().split(",")[0]);
        for(int i=0; i<n; ++i) old.append(scan.nextLine()).append("\n");
        actual.add(Integer.parseInt(scan.nextLine()));
        actual.add(Integer.parseInt(scan.nextLine()));

        FileWriter fw = new FileWriter(f);
        fw.write(old.toString());
        fw.close();

        assertEquals("testSetDataToKakuro failed because",expected,actual);
    }

    @Test
    /** @brief Test de la función GuardarKakuroFormatoStandard*/
    public void testGuardarKakuroFormatoStandard() throws FileNotFoundException
    {
        PersistenciaKakuro pk = new PersistenciaKakuro();
        String expected = "5,4\n" +
                "*,*,*,*\n" +
                "*,C21,C9,C14\n" +
                "F13,?,?,?\n" +
                "F18,?,?,?\n" +
                "F13,?,?,?\n" +
                "8\n" +
                "51\n" +
                "36000\n";
        pk.guardarKakuroFormatoStandard("data/JUnits/testsPersistencia/", "testGuardarStd1", expected);
        StringBuilder actual = new StringBuilder();
        File f = new File("data/JUnits/testsPersistencia/testGuardarStd1.std");
        Scanner scan = new Scanner(f);
        while(scan.hasNextLine()) actual.append(scan.nextLine()).append("\n");

        assertEquals("testGuardarKakuroFormatoStandard failed because",expected, actual.toString());
    }
}
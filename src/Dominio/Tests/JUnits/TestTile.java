/** @file TestTile.java
 @brief Especificación de la clase TestTile
 */

package Dominio.Tests.JUnits;

import Dominio.Clases.WhiteTile;

import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Test;

/** @class TestTile
    @brief Tests JUnit de la clase WhiteTile (hemos considerado que las clases Tile y BlackTile no tenían funciones no triviales)

    Hecha por Armando Rodríguez Ramos
 */
public class TestTile
{
    @Test
    /** @brief Test 1 de la intersección de vectores */
    public void testIntersectVectors1()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v1 = new ArrayList<>();
        v1.add(1); v1.add(2); v1.add(3); v1.add(5); v1.add(7); v1.add(8); v1.add(9);
        ArrayList<Integer> v2 = new ArrayList<>();
        v2.add(1); v2.add(2); v2.add(4); v2.add(6); v2.add(8);

        ArrayList<Integer> actual = wt.publicIntersectVectors(v1,v2);
        ArrayList<Integer> expected = new ArrayList<>();
        expected.add(1); expected.add(2); expected.add(8);

        assertEquals("Test intersection of vectors with different sizes failed because", expected, actual);
    }

    @Test
    /** @brief Test 2 de la intersección de vectores */
    public void testIntersectVectors2()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v1 = new ArrayList<>();
        ArrayList<Integer> v2 = new ArrayList<>();
        v2.add(1); v2.add(4); v2.add(6); v2.add(8);

        ArrayList<Integer> actual = wt.publicIntersectVectors(v1,v2);
        ArrayList<Integer> expected = new ArrayList<>();

        assertEquals("Test intersection of vectors with one of them empty failed because", expected, actual);
    }

    @Test
    /** @brief Test 3 de la intersección de vectores */
    public void testIntersectVectors3()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v1 = new ArrayList<>();
        ArrayList<Integer> v2 = new ArrayList<>();

        ArrayList<Integer> actual = wt.publicIntersectVectors(v1,v2);
        ArrayList<Integer> expected = new ArrayList<>();

        assertEquals("Test intersection of vectors with both of them empty failed because", expected, actual);
    }

    @Test
    /** @brief Test 1 de la inversión de vectores */
    public void testInverseVector1()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v = new ArrayList<>();
        v.add(1); v.add(2); v.add(3); v.add(5); v.add(7); v.add(8); v.add(9);

        ArrayList<Integer> actual = wt.publicInverseVector(v);
        ArrayList<Integer> expected = new ArrayList<>();
        expected.add(9); expected.add(8); expected.add(7);
        expected.add(5); expected.add(3); expected.add(2); expected.add(1);

        assertEquals("Test inverse vector with regular size ordered failed because", expected, actual);
    }

    @Test
    /** @brief Test 2 de la inversión de vectores */
    public void testInverseVector2()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v = new ArrayList<>();
        v.add(1); v.add(9); v.add(13); v.add(2); v.add(17); v.add(13); v.add(5);

        ArrayList<Integer> actual = wt.publicInverseVector(v);
        ArrayList<Integer> expected = new ArrayList<>();
        expected.add(5); expected.add(13); expected.add(17);
        expected.add(2); expected.add(13); expected.add(9); expected.add(1);

        assertEquals("Test inverse vector not ordered failed because", expected, actual);
    }

    @Test
    /** @brief Test 3 de la inversión de vectores */
    public void testInverseVector3()
    {
        WhiteTile wt = new WhiteTile();

        ArrayList<Integer> v = new ArrayList<>();

        ArrayList<Integer> actual = wt.publicInverseVector(v);
        ArrayList<Integer> expected = new ArrayList<>();

        assertEquals("Test inverse empty vector failed because", expected, actual);
    }

    @Test
    public void testSetValuePointer()
    {
        WhiteTile wt = new WhiteTile();
        ArrayList<Integer> possibilities = new ArrayList<>();
        possibilities.add(5); possibilities.add(6); possibilities.add(8);
        wt.putPossibilities(possibilities);
        wt.incPointer();
        wt.setValuePointer();
        assertEquals("Set value pointer failed ", 5, wt.getValue());
        wt.incPointer();
        wt.setValuePointer();
        assertEquals("Set value pointer failed ", 6, wt.getValue());
    }
}
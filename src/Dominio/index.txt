---------------------------------------
|       Directorio \src\Dominio       |
---------------------------------------

Este es el directorio donde tenemos los package de Clases, Controladores y Tests.

DIRECTORIOS:

·Clases:	contiene las diferentes clases que usamos en nuestro proyecto (Tile, Kakuro, etc.)
·Controladores: contiene los diferentes controladores de las diveras funcionalidades de nuestro proyecto
·Tests:		contiene tanto los JUnits como los Drivers


FICHEROS: no hay


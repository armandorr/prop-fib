/** @file CtrlDominio.java
 @brief Especificación del controlador CtrlDominio
 */

package Dominio.Controladores;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Usuario;
import Dominio.Clases.Utiles;
import Dominio.Clases.WhiteTile;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

/** @class CtrlDominio
 @brief Controlador de la capa de dominio

 Hecha por Marco Patiño

 Contiene las funciones necesárias para obtener la información que necesita transportar a la capa de presentación
 */
public class CtrlDominio
{
    /** @brief Kakuro de dominio*/
    Kakuro kakuro;
    /** @brief Controlador de play*/
    CtrlPlay game;
    /** @brief Controlador de persistencia*/
    CtrlPersistencia ctrlPersistencia;
    /** @brief Usuario actual*/
    Usuario usuario;
    /** @brief Nombre partida actual*/
    String nombrePartida;

    /**@brief Creadora por defecto
    \pre <em>Cierto</em>
    \post Se ha generado un objeto del controlador de dominio
    */
    public CtrlDominio()
    {
        kakuro = new Kakuro();
        ctrlPersistencia = new CtrlPersistencia();
        usuario = new Usuario();
        nombrePartida = null;
    }

    /**@brief Creadora donde pasamos un kakuro
    \pre <em>Cierto</em>
    \post Se ha generado un objeto del controlador de dominio con el kakuro k
     */
    public CtrlDominio(Kakuro k)
    {
        this.kakuro = k;
        ctrlPersistencia = new CtrlPersistencia();
        usuario = new Usuario();
        nombrePartida = null;
    }

    /**@brief Operación consultora que nos devuelve el kakuro del parámetro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto el kakuro del controlador de dominio
     */
    public Kakuro getKakuroDominio()
    {
        return kakuro;
    }

    /**@brief Operación modificadora para cambiar el nombre de partida del controlador de dominio
    \pre <em>Cierto</em>
    \post Hemos cambiado el nombre de partida por el string s
     */
    public void setNombrePartida(String s)
    {
        nombrePartida = s;
    }

    /**@brief Operación consultora para obtener el nombre de partida del controlador de dominio
    \pre <em>Cierto</em>
    \post Hemos devuelto el nombre de partida del parámetro implícito
     */
    public String getNombrePartida()
    {
        return nombrePartida;
    }

    /**@brief Operación consultora para obtener el nombre de usuario del atributo Usuario u
    \pre <em>Cierto</em>
    \post Hemos devuelto el nombre de usuario del usuario u
     */
    public String getUsername()
    {
        return usuario.getUsername();
    }

    /**@brief Operación modificador que dado un string user(nombre de usuario), un string pass(contraseña)
     * nos crean una cuenta con nombre de usuario = user, y password = pass.
    \pre <em>Cierto</em>
    \post Hemos creado una cuenta con los parametros introducidos y devolvemos true, en caso contrario devolvemos false
     */
    public boolean crearCuenta(String user, String pass)
    {
        boolean crearCorrecto = ctrlPersistencia.creaUsuario(user, pass);
        if(crearCorrecto) usuario = new Usuario(user, pass);
        return crearCorrecto;
    }

    /**@brief Operación que dado un string user(nombre de usuario), un string pass(contraseña)
     * nos inicia sesión en la cuenta con nombre de usuario = user, y password = pass.
    \pre <em>Cierto</em>
    \post Hemos iniciado sesión en la cuenta con los parametros introducidos y devolvemos true, en caso de que no se
        pueda, devolvemos false
     */
    public boolean loginCuenta(String user, String pass)
    {
        boolean loginCorrecto = ctrlPersistencia.loginUsuario(user, pass);
        if(loginCorrecto) usuario = new Usuario(user, pass);
        return loginCorrecto;
    }

    /**@brief Operación que dada una posición en el tablero y un valor, intenta escribir el valor en esa casilla
    \pre <em>Cierto</em>
    \post Devuelve 0 si se puede escribir el valor porque es correcto i no se repite
          Devuelve 1 si el valor value no puede ir en una casilla blanca (value > 9 or value < 0)
          Devuelve 2 si el valor value esta fuera del tamaño del Kakuro
          Devuelve 3 si la casilla es negra
          Devuelve 4 si se repite el valor en la fila
          Devuelve 5 si se la suma es completa en la fila y no es correcta
          Devuelve 6 si se repite el valor en la columna
          Devuelve 7 si se la suma es completa en la columna y no es correcta
     */
    public int intentaModificarKakuro(int i, int j, int value)
    {
        return kakuro.tryToWrite(i,j,value,1);
    }

    /** @brief Operación generadora de kakuros
    \pre <em>n > 0, m > 0, 1 <= prob <= 3</em>
    \post Nos genera un Kakuro con n filas, m columnas y con una dificultad entre 1,2 o 3
     */
    public void generateKakuro(int n, int m, double prob)
    {
        CtrlGenerate gen = new CtrlGenerate();
        kakuro = gen.generarKakuro(n, m, prob);
    }

    /** @brief Operación de conversión Kakuro a string
    \pre <em>El Kakuro k del p.i no es vacío</em>
    \post Convierte el Kakuro k del p.i en una string, siguiendo un estilo y formato bonito
     */
    public String kakuroToStringNice()
    {
        String s = kakuro.kakuroToStringNice();
        Utiles utiles = new Utiles();
        return utiles.KakuroToCentred(s);
    }

    /** @brief Operación jugar
    \pre <em>El Kakuro k del p.i es correcto y jugable (no vacío, etc.)</em>
    \post Se empieza a jugar el Kakuro k del p.i
     */
    public void playGame(String nombrePartida)
    {
        game = new CtrlPlay(kakuro, nombrePartida, usuario.getUsername());
        try {
            game.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación de lectura de Kakuro por archivo
    \pre <em>archivo es una dirección válida que apunta a un file que contiene un Kakuro en el formato oficial</em>
    \post El Kakuro k del p.i pasa a ser el Kakuro que contiene el archivo de la dirección pasada por parámetro
     */
    public void readKakuroFromFile(String path)
    {
        kakuro = new Kakuro();
        if(kakuro.readKakuroFromString(ctrlPersistencia.readKakuro(path))) {                 // si entramos aqui es que el kakuro NO tenia dificultad ni ID en el .txt y lo estamos leyendo del repositorio
            int id = ctrlPersistencia.nextKakuroID(true);                           // entonces NO incrementamos la ID -> ponemos inc=false
            ctrlPersistencia.setDataToKakuro(id, kakuro.calculaDificultad(),path);           // tambien hay que ir al txt y meterle su ID y su dif
            kakuro.setID(id);
        }
    }

    /**@brief Operación que dado un string kak, nos modifica el kakuro del control de dominio con el kakuro que se
     * encuentra en el string kak.
    \pre <em>Cierto</em>
    \post Hemos modificado el kakuro k con el string kak
     */
    public void readKakuroFromString(String kak)
    {
        kakuro = new Kakuro();
        kakuro.readKakuroFromString(kak);
    }

    /** @brief Operación resolvedora de kakuros
    \pre <em>Cierto</em>
    \post Intenta resolver el Kakuro k del p.i. Retorna cierto sii éste tiene solución (y lo resuelve).
     En caso contrario retorna falso
     */
    public boolean solveKakuro()
    {
        CtrlSolve sol = new CtrlSolve(kakuro);
        return sol.solve_recursivo();
    }

    /** @brief Operación verificadora de kakuros
    \pre <em>Cierto</em>
    \post Devuelve el número de soluciones que tiene el Kakuro. 0->No tiene solución, 1->Solución única, 2->Más de una solución
     */
    public int verifyKakuro()
    {
        CtrlVerify ver = new CtrlVerify(kakuro);
        Kakuro[] solucion = new Kakuro[1];
        int sols = ver.verify(solucion);
        if(sols != 0) kakuro = new Kakuro(solucion[0]);
        return sols;
    }

    /** @brief Operación impresora de kakuros
    \pre <em>Cierto</em>
    \post Nos imprime el Kakuro k del p.i en un formato bonito
     */
    public String printKakuroCD()
    {
        return kakuroToStringNice();
    }

    /** @brief Operación que devuelve un string que contiene el kakuro del parámetro implícito en formato estandard sin dificultad ni id.
    \pre <em>Cierto</em>
    \post Hemos devuelto string que contiene el kakuro del p.i en formato estandard sin dificultad ni id.
     */
    public String getKakuroStandradWithoutIdAndDiff()
    {
        return kakuro.kakuroToStringStandard(false);
    }

    /** @brief Función que nos guarda un Kakuro pasado como string en el repositorio
    \pre <em>name ha de ser un nombre de fichero válido y el Kakuro del p.i. tiene que haber sido inicializado y no ser vacío</em>
    \post Nos guarda el Kakuro de la clase en el directorio correspondiente y con el nombre que le hemos indicado
     */
    public void guardarKakuroEnRepositorio(String name, boolean isSol)
    {
        kakuro.setID(ctrlPersistencia.nextKakuroID(true));
        String strKak = kakuro.kakuroToStringStandard(true);
        ctrlPersistencia.guardarKakuroEnRepositorio(name, strKak, isSol);
    }

    /**@brief Operación consultora que dado un path s, nos devuelve la lista de los nombres de los archivos
    \pre <em>Cierto</em>
    \post Hemos devuelto la lista de los nombres de los archivos del path s
     */
    public String[] getPathNames(String s)
    {
        return ctrlPersistencia.getPathNames(s);
    }

    /**@brief Operación consultora que dado un path s, nos devuelve el tiempo del kakuro referenciado por el path
    \pre <em>El archivo referenciado por el path s tiene un tiempo</em>
    \post Hemos devuelto el tiempo del kakuro contenido en el archivo referenciado por el path s
     */
    public int leerTiempoPartidaActual(String s)
    {
        return ctrlPersistencia.leerTiempoPartidaActual(s);
    }

    /**@brief Operación modificadora que dado un kakuro en string, y un path s, guarda el estado actual de la partida en el
     * path s
    \pre <em>Cierto</em>
    \post Hemos guardado el estado actual de la partida en el path indicado
     */
    public void guardarPartidaAuxiliar(String kak, String s)
    {
        ctrlPersistencia.guardarGame(kak,s);
    }

    /**@brief Operación modificadora que dado el nombre de usuario de un user, se borra toda la información relaciondada
     * con el usuario
    \pre <em>Debe existir el usuario referenciado por el string user</em>
    \post Hemos borrado el usuario de nuestro sistema
     */
    public void eliminarUsuario(String user)
    {
        ctrlPersistencia.eliminarUsuario(user);
    }

    /**@brief Operación consultora que dado el nombre de usuario de un user, se devuelve la contraseña del user
    \pre <em>Debe existir el usuario referenciado por el string user</em>
    \post Hemos devuelto la contraseña del usuario inidcado por el string user
     */
    public String getPassword(String user)
    {
        return ctrlPersistencia.getPassword(user);
    }

    /**@brief Operación modificadora que dado un tiempo, guarda la partida definida en el control de dominio, en la carpeta
     * de partidas acabads del usuario actual. El tiempo pasado como parametro nos sirve para calcular los puntos que le
     * daremos al usuario por completar el kakuro en cuestión
    \pre <em>Cierto</em>
    \post Hemos guardado la partida como acabada en la carpeta de partidas acabadas del usuario
     */
    public void guardarPartidaAcabada(double tiempo)
    {
        String username = usuario.getUsername();
        if (username!=null) {
            int id = kakuro.getId();
            if (id <= 0) {
                id = nextKakuroID(true);
                kakuro.setID(id);
            }
            String save = kakuro.kakuroToStringStandard(true);
            save += (int)(tiempo*1000);
            save += "\n";
            int puntos;
            int siz = kakuro.getKakuroCols()*kakuro.getKakuroRows();

            if(siz<=4*4)              puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),1.0) * Math.pow(kakuro.getKakuroCols(),1.0) * kakuro.getDificultad()) / (tiempo / 60)) / 10, 8);
            else if(siz<=8*8)         puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),1.2) * Math.pow(kakuro.getKakuroCols(),1.2) * kakuro.getDificultad()) / (tiempo / 60)), 12);
            else if(siz<=12*12)       puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),1.5) * Math.pow(kakuro.getKakuroCols(),1.5) * kakuro.getDificultad()) / (tiempo / 60)),30);
            else if(siz<=16*16)       puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),1.85) * Math.pow(kakuro.getKakuroCols(),1.85) * kakuro.getDificultad()) / (tiempo / 60)),200);
            else if(siz<=20*20)       puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),2.1) * Math.pow(kakuro.getKakuroCols(),2.1) * kakuro.getDificultad()) / (tiempo / 60)),900);
            else if(siz<=24*24)       puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),2.2) * Math.pow(kakuro.getKakuroCols(),2.2) * kakuro.getDificultad()) / (tiempo / 60)),1500);
            else if(siz<=28*28)       puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),2.25) * Math.pow(kakuro.getKakuroCols(),2.25) * kakuro.getDificultad()) / (tiempo / 60)),300);
            else                      puntos = Math.max((int) ((Math.pow(kakuro.getKakuroRows(),2.3) * Math.pow(kakuro.getKakuroCols(),2.3) * kakuro.getDificultad()) / (tiempo / 60)),5500);

            save += puntos;
            ctrlPersistencia.guardarGameAcabado(id,puntos,username,save);
        }
        if (nombrePartida != null){
            if (username!= null){
                String path = "data/Users/" + username + "/partidasUser/" + nombrePartida+".txt";
                ctrlPersistencia.borrarFichero(path);
            }
        }
    }

    /**@brief Operación modificadora que dado un usuario, nos lo borra del archivo estado.txt
    \pre <em>El usuario indicado por el string user debe existir</em>
    \post Hemos borrado el usuario indicado del archivo estado.txt
     */
    public void eliminarUsuarioDelEstado(String user)
    {
        ctrlPersistencia.eliminarUsuarioDeEstado(user);
    }

    /**@brief Operación consultora que nos devuelve la siguiente id que puede usar un kakuro
    \pre <em>Cierto</em>
    \post Nos devuelve la siguiente id que podemos asignar a un kakuro
     */
    public int nextKakuroID(boolean inc)
    {
        return ctrlPersistencia.nextKakuroID(inc);
    }

    /**@brief Operación consultora que nos devuelve el kakuro del parámetro implícito como una matriz de strings
    \pre <em>Cierto</em>
    \post Nos devuelve el kakuro en formato de matriz de strings
     */
    public String[][] getMatrixFromKakuro()
    {
        return kakuro.doMatrixFromKakuro();
    }

    /**@brief Operación consultora que nos devuelve true si el kakuro del parámetro implícito esta completado, en caso
     * contrario devuelve falso
    \pre <em>Cierto</em>
    \post Nos devuelve true si el kakuro del parámetro implícito esta completado. En caso contrario, retorna falso
     */
    public boolean isKakuroCompleted()
    {
        return kakuro.isKakuroCompleted();
    }

    /**@brief Operación modificadora que dado un kakuro en formato matriz de strings, lee el kakuro del parámetro implícito
     * des de la matriz de strings
    \pre <em>Cierto</em>
    \post Hemos leido un kakuro a partir de una matriz de strings
     */
    public void readKakuroFromMatrixString(String[][] kak)
    {
        int rows = kak.length;
        int cols = kak[0].length;
        kakuro = new Kakuro(rows, cols, kakuro.getDificultad(), kakuro.getId());
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                String stringTile = kak[i][j];
                if (stringTile.equals("")) { //white tile empty
                    kakuro.setWhiteTile(i, j, new WhiteTile());
                } else if (stringTile.length() == 1) {
                    if (stringTile.equals("#")) {
                        kakuro.setBlackTile(i, j);
                    } else {
                        int x = Integer.parseInt(stringTile);
                        kakuro.setWhiteTile(i, j, x);
                    }
                } else {
                    kakuro.setBlackTile(i, j);
                    String substringFirstPart = stringTile.substring(0, 2);
                    String substringSecondPart = stringTile.substring(2, 5);
                    substringFirstPart = substringFirstPart.replace(" ", "");
                    substringSecondPart = substringSecondPart.replace(" ", "");
                    int firstNumber = Integer.parseInt(substringFirstPart);
                    if (substringSecondPart.equals("→")) {
                        kakuro.setRightBlackTile(i, j, firstNumber);
                    } else if (substringSecondPart.equals("↓")) {
                        kakuro.setBottomBlackTile(i, j, firstNumber);
                    } else {
                        kakuro.setBottomBlackTile(i, j, firstNumber);
                        kakuro.setRightBlackTile(i, j, Integer.parseInt(substringSecondPart));
                    }
                }
            }
        }
    }

    /**@brief Operación consultora que nos devuelve el ranking actual de los jugadores
    \pre <em>Cierto</em>
    \post Hemos devuelto el ranking de los usuarios en un string
     */
    public String getRanking()
    {
        return ctrlPersistencia.getRanking();
    }

    /**@brief Operación consultora del ID del kakuro de parametro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto el ID del kakuro
     */
    public int getIdKakuro()
    {
        return kakuro.getId();
    }

    /**@brief Operación consultora que dado un entero op, nos devuelve una lista de nombres de kakuros
    \pre <em>El parámetro op ha de estar entre 1 y 3(incluidos)</em>
    \post Si op vale 1, nos devuelve los nombres de los kakuros de la carpeta partidasUser del usuario actual
          Si op vale 2, nos devuelve los nombres de los kakuros de la carpeta PartidasAcabadas del usuario actual
          Si op vale 3, nos devuelve los nombres de los kakuros del Repositorio general
     */
    public String[] getKakuroNames(int op)
    {
        String username = usuario.getUsername();
        if (op == 1 || op == 2) {
            String carpeta = "/partidasUser";
            if (op == 2) carpeta = "/partidasAcabadas";
            String userDir = "data/Users/" + username + carpeta;
            File kakuros = new File(userDir);
            String[] lista = kakuros.list();
            ArrayList<String> newList = new ArrayList<>();
            for (String stringList : Objects.requireNonNull(lista)) {
                String extension = stringList.substring(stringList.length() - 4);
                if (!extension.equals(".std")) newList.add(stringList);
            }
            String[] listaADevolver = new String[newList.size()];
            return newList.toArray(listaADevolver);
        }
        else if (op == 3) {
            String repoDir = "data/Repositorio";
            File kakuros = new File(repoDir);
            String[] lista = kakuros.list();
            ArrayList<String> newList = new ArrayList<>();
            for (String stringList : Objects.requireNonNull(lista)) {
                String extension = stringList.substring(stringList.length() - 4);
                if (!extension.equals(".std")) newList.add(stringList);
            }
            String[] listaADevolver = new String[newList.size()];
            return newList.toArray(listaADevolver);
        }
        return new String[0];
    }

    /**@brief Operación modificadora que dado un nombre de usuario y una contraseña, modifica el usuario del parámetro implícito
    \pre <em>El usuario indicado por el parámetro nameUser debe existir(incluidos)</em>
    \post Hemos modificado el usuario del p.i con los atributos que hemos pasado como parámetros
     */
    public void setUser(String nameUser, String pass)
    {
        usuario.setUserName(nameUser);
        usuario.setUserPass(pass);
    }

    /**@brief Operación modificadora que dado un tiempo, guarda la partida definida en el control de dominio, en la carpeta
     * de partidas empezadoas del usuario actual. El tiempo pasado como parametro sirve para que cuando se vuelva a cargar la partida
     * en algun otro momento, se sepa de donde volver a empezar a contar el tiempo
    \pre <em>Cierto</em>
    \post Hemos guardado la partida en la carpeta de partidas empezadas del usuario con el tiempo jugado hasta ahora
     */
    public void guardarPartida(double time)
    {
        if(kakuro.getId()<=0) kakuro.setID(nextKakuroID(true));
        String sav = kakuro.kakuroToStringStandard(true);

        sav += (int)(time*1000);

        guardarPartidaAuxiliar(sav,"data/Users/" + usuario.getUsername()+"/partidasUser/"+nombrePartida+".txt");
    }

    /**@brief Operación modificadora que dado un nombre de usuario user y una contraseña pass, cambia la contraseña del usuario user
     * por la nueva contraseña pass
    \pre <em>El usuario user debe existir y pass no debe ser una contraseña vacío</em>
    \post Hemos cambiado la contraseña del usuario con nombre de usuario user
     */
    public void cambiarContrasenya(String user, String pass)
    {
        ctrlPersistencia.cambiarContrasenya(user, pass);
    }

    /**@brief Operación consultora que nos devuelve la dificultado del kakuro del parámetro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto la dificultad del kakuro del p.i
     */
    public int getDificultad()
    {
        return kakuro.getDificultad();
    }

    /**@brief Operación consultora que dado un path a un kakuro acabado, nos devuelve los puntos que tiene el kakuro acabado
    \pre <em>El path debe apuntar a un kakuro que ya esta completado por el usuario</em>
    \post Hemos devuelto los puntos del kakuro en cuestión
     */
    public int getPuntosKakuro(String absolutePath)
    {
        return ctrlPersistencia.getPuntosKakuro(absolutePath);
    }

    /**@brief Operación consultora que dados un nombre de archivo, y un path a una carpeta nos devuelve true si el fichero esta
     * en la carpeta. En caso contrario, devuelve falso
    \pre <em>Cierto</em>
    \post Hemos devuelto true si la carpeta contiene el fichero con ese nombre, en caso contrario devolvemos falso
     */
    public boolean fileDontExist(String fichero, String carpeta)
    {
        return ctrlPersistencia.fileDontExist(fichero, carpeta);
    }

    /**@brief Operación que dado un path, nos guarda el kakuro del parámetro implícito en formato estandard en la carpeta
     * indicada por el path
    \pre <em>Debe ser un path correcto</em>
    \post Hemos guardado el kakuro en formato estandard en el path indicado
     */
    public void guardarKakuroFormatoStandard(String path)
    {
        ctrlPersistencia.guardarKakuroFormatoStandard(path,nombrePartida,getKakuroStandradWithoutIdAndDiff());
    }
}
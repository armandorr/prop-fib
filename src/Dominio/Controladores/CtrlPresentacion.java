/** @file CtrlPresentacion.java
    @brief Especificación de la classe CtrlPresentacion
 */

package Dominio.Controladores;

import Dominio.Clases.Utiles;
import Presentacion.*;

import java.util.Objects;
import java.util.Scanner;

/** @class CtrlPresentacion
    @brief Controlador de presentacion

    Hecha por todos

    Contiene diferentes funciones que interactuan con el usuario y obtienen resultados del controlador de dominio associado
 */

public class CtrlPresentacion
{

    /** @brief Relacion con la clase CtrlDominio.*/
    static CtrlDominio CD = new CtrlDominio();
    /** @brief Relacion con la clase utiles.*/
    static Utiles utiles = new Utiles();

    /** @brief Operación de iniciar presentación
    \pre <em>cierto</em>
    \post Se crea la vista inicial del programa ejecutando la música si startMusic es cierto
                                                sin ejecutar la música si startMusic es falso
     */
    public static void iniciarPresentacion(boolean startMusic)
    {
        if(startMusic) startingView.startMusic();
        new startingView();
    }

    /** @brief Operación de mostrar la vista de login
    \pre <em>cierto</em>
    \post Se pasa a la vista con las opciones de iniciar sesion (introducir username, contraseña...)
     */
    public static void mostrarVistaLogin()
    {
        new loginView();
    }

    /** @brief Operación de mostrar la vista de ranking
    \pre <em>cierto</em>
    \post Se pasa a la vista que muestra el ranking de los usuarios
     */
    public static void mostrarVistaRanking()
    {
        new rankingView();
    }

    /** @brief Operación de mostrar la vista de crear usuario
    \pre <em>cierto</em>
    \post Se pasa a la vista con las opciones de crear usuario (introducir username, contraseña, repetir contraseña...)
     */
    public static void mostrarVistaCrearCuenta()
    {
        new signUpView();
    }

    /** @brief Operación de mostrar el menú principal de usuario
    \pre <em>cierto</em>
    \post Se pasa a la vista del menú principal que contiene las opciones para usuarios loggeados o alternativamente una fracción de éstas (para invitados)
     */
    public static void mostrarVistaMenuUsuario()
    {
        new userMenuView();
    }

    /** @brief Operación de mostrar la vista de introducir un Kakuro manualmente
    \pre <em>op vale 1 o 2</em>
    \post Si op vale 1, entonces se hace introducir un Kakuro manualmente al usuario para que se verifique y se guarde; si op vale 2, entonces el Kakuro introducido es para ser jugado
     */
    public static void mostrarVistaRepositorioOManualKakuro(int op)
    {
        new repoManualOrGenerateView(op);
    }

    /** @brief Operación de mostrar la vista de jugar un Kakuro nuevo o cargar partida
    \pre <em>cierto</em>
    \post Si invitado vale cierto, se muestran solo las opciones que tienen sentido para los invitados, de lo contrario se muestran todas las opciones para los usuarios registrados
     */
    public static void mostrarVistaNuevoOCargarKakuro(boolean invitado)
    {
        new newOrLoadView(invitado);
    }

    /** @brief Operación de mostrar la vista de editar un usuario
    \pre <em>cierto</em>
    \post Se pasa a la vista con las opciones de crear usuario (cambiar password, eliminar usuario...)
     */
    public static void mostrarVistaEditUser()
    {
        new editUserView();
    }

    /** @brief Operación de mostrar la vista de eliminar usuario
    \pre <em>cierto</em>
    \post Se pasa a la vista de eliminar usuario (donde hay que confirmar, introducir la contraseña...)
     */
    public static void mostrarVistaDeleteUser()
    {
        new deleteUserView();
    }

    /** @brief Operación de cargar un Kakuro a través de su representación en String
    \pre <em>la String kakuro contiene la representación oficial de un Kakuro (y opcionalmente puede contener parámetros extra que nosotros hemos definido: dificultad e ID)</em>
    \post El Kakuro del CtrlDominio CD pasa a ser el representado por la String. Si la String contenía parámetros extra (no oficiales), se le asignan al Kakuro, de lo contrario, se calculan y se asignan
     */
    public static void cargarKakuroDeString(String kakuro)
    {
        CD.readKakuroFromString(kakuro);
    }

    /** @brief Operación de cargar un Kakuro deste un archivo
    \pre <em>el archivo que se encuentra en absolutePath contiene la representación oficial de un Kakuro (y opcionalmente puede contener parámetros extra que nosotros hemos definido: dificultad e ID)</em>
    \post El Kakuro del CtrlDominio CD pasa a ser el representado por el archivo. Si el archivo contenía parámetros extra (no oficiales), se le asignan al Kakuro, de lo contrario, se calculan y se asignan
     */
    public static void cargarKakuroDeArchivo(String absolutePath)
    {
        CD.readKakuroFromFile(absolutePath);
    }

    /** @brief Operación de cargar un Kakuro representado a través de una matriz de Strings
    \pre <em>kak contiene la representación oficial de un Kakuro, pero carácter a carácter, donde cada posición de kak es una String de tamaño 1 que guarda un carácter</em>
    \post El Kakuro del CtrlDominio del p.i pasa a ser el representado por kak
     */
    public static void cargarKakuroDeMatrixString(String[][] kak)
    {
        CD.readKakuroFromMatrixString(kak);
    }

    /** @brief Operación de transformar un Kakuro en una matriz de Strings
    \pre <em>el Kakuro del CtrlDominio del p.i no es vacío</em>
    \post Retorna la representación oficial del Kakuro del CtrlDominio CD pero en formato de matriz de Strings
     */
    public static String[][] getKakuroStringMatrixFormat()
    {
        return CD.getMatrixFromKakuro();
    }

    /** @brief Operación de transformar un Kakuro en una matriz de Strings
    \pre <em>n>0, m>0, 1<=dif<=3</em>
    \post Genera un Kakuro de tamaño n×m y con dificultad dif, hace que el Kakuro del CtrlDominio del p.i pase a ser ese Kakuro generado y retorna su representación en matriz de Strings
     */
    public static String[][] generarKakuro(int n, int m, int dif)
    {
        CD.generateKakuro(n,m,dif);
        return CD.getMatrixFromKakuro();
    }

    /** @brief Operación de mostrar la vista que representa el submenú de la funcionalidad generar
    \pre <em>cierto</em>
    \post Se pasa a la vista de seleccionar las opciones del kakuro a generar.
     */
    public static void mostrarVistaOpcionesGenerar(int op)
    {
        new generateOptionsView(op);
    }

    /** @brief Operación de mostrar la vista que presenta un kakuro recientemente generado
    \pre <em>cierto</em>
    \post Se pasa a la vista de mostrar el kakuro generado.
     */
    public static void mostrarVistaKakuroGenerado(int op, String[][] kak)
    {
        new generatedKakuroView(op,kak);
    }

    /** @brief Operación de mostrar la vista de la funcionalidad de Play para usuario
    \pre <em>cierto</em>
    \post Se pasa a la vista de Play para usuario (donde guardamos el tiempo, puntuación)
     */
    public static void mostrarVistaJugarKakuro(String[][] kak, double time, String[][] kakSolved, int numPistas)
    {
        new playingView(kak,time,kakSolved,numPistas);
    }

    /** @brief Operación de mostrar la vista de la funcionalidad de verificar
    \pre <em>cierto</em>
    \post Se pasa a la vista de verificar kakuro.
     */
    public static void mostrarVistaKakuroVerificar(String nombre, boolean manual)
    {
        new verifyAndSolveView(1, nombre, manual);
    }

    /** @brief Operación de mostrar la vista de la funcionalidad Solve
    \pre <em>cierto</em>
    \post Se pasa a la vista de Solve
     */
    public static void mostrarVistaKakuroSolucionar()
    {
        new verifyAndSolveView(2,null,false);
    }

    /** @brief Operación de mostrar la vista de un kakuro recien verificado.
    \pre <em>cierto</em>
    \post Se pasa a la vista que muestra el kakuro verificado.
     */
    public static void mostrarVistaKakuroVerificado(int resultat, boolean manual, String[][] kakOriginal)
    {
        new verifyFinishedView(resultat,manual,kakOriginal);
    }

    /** @brief Operación de mostrar la vista de un kakuro solucionado.
    \pre <em>cierto</em>
    \post Se pasa a la vista que muestra el kakuro solucionado.
     */
    public static void mostrarVistaKakuroSolucionado(boolean hasSol)
    {
        new solveFinishedView(hasSol);
    }

    /**@brief Operación que dado un string user(nombre de usuario), un string pass(contraseña)
     * nos inicia sesión en la cuenta con nombre de usuario = user, y password = pass.
    \pre <em>Cierto</em>
    \post Hemos iniciado sesión en la cuenta con los parametros introducidos y devolvemos true, en caso de que no se
    pueda, devolvemos false
     */
    public static boolean accederCuenta(String username, String pass)
    {
        return CD.loginCuenta(username, pass);
    }

    /**@brief Operación modificador que dado un string user(nombre de usuario), un string pass(contraseña)
     * nos crean una cuenta con nombre de usuario = user, y password = pass.
    \pre <em>Cierto</em>
    \post Hemos creado una cuenta con los parametros introducidos y devolvemos true, en caso contrario devolvemos false
     */
    public static boolean crearCuenta(String username, String password)
    {
        return CD.crearCuenta(username, password);
    }

    /**@brief Operación que dada una posición en el tablero y un valor, intenta escribir el valor en esa casilla en el Kakuro de CtrlDominio
    \pre <em>Cierto</em>
    \post Si no hay ningún problema, devuelve una matriz de strings representando el kakuro de CtrlDominio. En otro caso:
    Devuelve 1 si el valor value no puede ir en una casilla blanca (value > 9 or value < 0)
    Devuelve 2 si el valor value esta fuera del tamaño del Kakuro
    Devuelve 3 si la casilla es negra
    Devuelve 4 si se repite el valor en la fila
    Devuelve 5 si se la suma es completa en la fila y no es correcta
    Devuelve 6 si se repite el valor en la columna
    Devuelve 7 si se la suma es completa en la columna y no es correcta
     */
    public static String[][] ponerValorEnKakuro(int rowSelected, int colSelected, int valueIn)
    {
        int res = CD.intentaModificarKakuro(rowSelected,colSelected,valueIn);
        if(res == 0) return CD.getMatrixFromKakuro();
        if(res == 1) return new String[][]{{"1"}};
        if(res == 2) return new String[][]{{"2"}};
        if(res == 3) return new String[][]{{"3"}};
        if(res == 4) return new String[][]{{"4"}};
        if(res == 5) return new String[][]{{"5"}};
        if(res == 6) return new String[][]{{"6"}};
        else return new String[][]{{"7"}};
    }

    /** @brief Operación de mostrar la vista para generar o cargar un Kakuro ya existente
    \pre <em>cierto</em>
    \post Se pasa a la vista de donde tenemos la opción de generar un Kakuro de desde cero o bien cargar un Kakuro existente
     */
    public static void mostrarVistaGenerarOCargarKakuro()
    {
        new generateOrLoadView();
    }

    /** @brief Operación para obtener si el Kakuro del CtrlDominio del p.i está acabado
    \pre <em>cierto</em>
    \post Retorna cierto sii el Kakuro de CtrlDominio está acabado
     */
    public static boolean getIfKakuroCompleted()
    {
        return CD.isKakuroCompleted();
    }

    /** @brief Operación de mostrar la vista de un kakuro recien acabado.
    \pre <em>cierto</em>
    \post Se pasa a la vista que muestra el kakuro recien acabado.
     */
    public static void mostrarVistaKakuroAcabado(String[][] kak, double tiempo)
    {
        CD.guardarPartidaAcabada(tiempo);
        new completedKakuroView(kak,tiempo);
    }

    /** @brief Operación de pausar la partida
    \pre <em>se está jugando actualmente una partida (con sus datos en CD) y no está pausada</em>
    \post Se pausa la partida actual y se oculta el tablero de juego al usuario
     */
    public static void pausarPartida(double time, String[][] kakSolved,int numPistas)
    {
        new pauseView(time,kakSolved,numPistas);
    }

    /** @brief Operación de reanudar la partida
    \pre <em>se está jugando actualmente una partida (con sus datos en CD) y está pausada</em>
    \post Se reanuda previamente
     */
    public static void reanudarPartida(double time,String[][] kakSolved,int numPistas)
    {
        new playingView(CD.getMatrixFromKakuro(),time,kakSolved,numPistas);
    }

    /** @brief Función para saber si el Kakuro tiene solución
    \pre <em>el Kakuro del CtrlDominio del p.i no está vacío</em>
    \post Retorna cierto sii el Kakuro del CtrlDominio del p.i tiene solución
     */
    public static boolean getHasSolutionKakuro()
    {
        return CD.solveKakuro();
    }

    /** @brief Operación de obtener la solución del Kakuro
    \pre <em>cierto</em>
    \post Retorna la solución del Kakuro del CtrlDominio del p.i en formato de matriz de Strings
     */
    public static String[][] getSolutionKakuro(String[][] kak)
    {
        CD.solveKakuro();
        String[][] solucion = CD.getMatrixFromKakuro();
        CD.readKakuroFromMatrixString(kak); //cargamos el nuestro para no perderlo de dominio
        return solucion;
    }

    /** @brief Operación verificadora de kakuros
    \pre <em>Cierto</em>
    \post Devuelve el número de soluciones que tiene el Kakuro. 0->No tiene solución, 1->Solución única, 2->Más de una solución
     */
    public static int getVerificationKakuro()
    {
        return CD.verifyKakuro();
    }

    /**@brief Operación consultora que nos devuelve el ranking actual de los jugadores
    \pre <em>Cierto</em>
    \post Hemos devuelto el ranking de los usuarios en un string
     */
    public static String getRanking()
    {
        return CD.getRanking();
    }

    /**@brief Operación consultora del ID del kakuro de controlDominio
    \pre <em>Cierto</em>
    \post Hemos devuelto el ID del kakuro
     */
    public static int getIDkakuro()
    {
        return CD.getIdKakuro();
    }

    /**@brief Operación consultora del siguiente ID disponible
    \pre <em>Cierto</em>
    \post Hemos devuelto el siguiente ID disponible
     */
    public static int nextKakuroID()
    {
        return CD.nextKakuroID(false);
    }

    /** @brief Operación de mostrar la vista de cuando se pide una pista.
    \pre <em>cierto</em>
    \post Se pasa a la vista que la vista de cuando se pide una pista.
     */
    public static void mostrarVistaPista(String[][] kak, double time, String[][] kakSolved, int numPistas)
    {
        new hintView(kak,time,kakSolved,numPistas);
    }

    /** @brief Operación de mostrar la vista de cuando se elige un kakuro.
    \pre <em>cierto</em>
    \post Se pasa a la vista que la vista de cuando se elige un kakuro.
     */
    public static void mostrarVistaKakuroElegido(String path, Integer tiempo)
    {
        new playRepoChoosedOptionsView(path, tiempo);
    }

    /** @brief Operación de obtener el usuario actual
    \pre <em>cierto</em>
    \post Retorna el username del usuario que está loggeado actualmente (null si es invitado)
     */
    public static String getUserName()
    {
        return CD.getUsername();
    }

    /** @brief Operación de obtener la contraseña de un usuario
    \pre <em>user es el username de un usuario que existe</em>
    \post Retorna la contraseña de user
     */
    public static String getPassword(String user)
    {
        return CD.getPassword(user);
    }

    /** @brief Operación de eliminar usuario
    \pre <em>user es el username de un usuario que existe</em>
    \post Todas las carpetas del usuario user son borradas de nuestra carpeta de data, como si nunca hubiera existido
     */
    public static void eliminarUsuario(String user)
    {
        CD.eliminarUsuario(user);
    }

    /** @brief Operación de eliminar usuario
    \pre <em>user es el username de un usuario que existe</em>
    \post Elimina al ususario user del Estado.txt
     */
    public static void eliminarUsuarioEstado(String user)
    {
        CD.eliminarUsuarioDelEstado(user);
    }

    /** @brief Operación de establecer usuario
    \pre <em>user es el username de un usuario que existe y pass es su contraseña</em>
    \post El usuario y la contraseña del CtrlDominio del p.i pasan a ser user y pass, respectivamente
     */
    public static void setUser(String us, String pass)
    {
        CD.setUser(us, pass);
    }

    /** @brief Operación de establecer el nombre de la partida
    \pre <em>cierto/em>
    \post El nombre de la partida actual (del CtrlDominio del p.i) pasa a ser name
     */
    public static void setNombrePartida(String name)
    {
        CD.setNombrePartida(name);
    }

    /** @brief Operación de obtener el nombre de la partida
    \pre <em>cierto/em>
    \post Retorna el nombre de la partida actual (del CtrlDominio del p.i). Si no tiene retorna null
     */
    public static String getNombrePartida()
    {
        return CD.getNombrePartida();
    }

    /**@brief Operación modificadora que dado un tiempo, guarda la partida definida en el control de dominio, en la carpeta
     * de partidas empezadoas del usuario actual. El tiempo pasado como parametro sirve para que cuando se vuelva a cargar la partida
     * en algun otro momento, se sepa de donde volver a empezar a contar el tiempo
    \pre <em>Cierto</em>
    \post Hemos guardado la partida en la carpeta de partidas empezadas del usuario con el tiempo jugado hasta ahora
     */
    public static void guardarPartida(double tiempo)
    {
        CD.guardarPartida(tiempo);
    }

    /**@brief Operación que dado un path, nos guarda el kakuro del parámetro implícito en formato estandard en la carpeta
     * indicada por el path
    \pre <em>Debe ser un path correcto</em>
    \post Hemos guardado el kakuro en formato estandard en el path indicado
     */
    public static void guardarKakuroFormatoStandard(String path)
    {
        CD.guardarKakuroFormatoStandard(path);
    }

    /** @brief Función que nos guarda un Kakuro pasado como string en el repositorio
    \pre <em>name ha de ser un nombre de fichero válido y el Kakuro del p.i. tiene que haber sido inicializado y no ser vacío</em>
    \post Nos guarda el Kakuro de la clase en el directorio correspondiente y con el nombre que le hemos indicado
     */
    public static void guardarKakuroEnRepositorio(String name, boolean isSol)
    {
        CD.guardarKakuroEnRepositorio(name,isSol);
    }

    /**@brief Operación consultora que dado un path s, nos devuelve el tiempo del kakuro referenciado por el path
    \pre <em>El archivo referenciado por el path s tiene un tiempo</em>
    \post Hemos devuelto el tiempo del kakuro contenido en el archivo referenciado por el path s
     */
    public static Integer getTiempoKakuro(String absolutePath)
    {
        return CD.leerTiempoPartidaActual(absolutePath);
    }

    /**@brief Operación consultora que dado un path a un kakuro acabado, nos devuelve los puntos que tiene el kakuro acabado
    \pre <em>El path debe apuntar a un kakuro que ya esta completado por el usuario</em>
    \post Hemos devuelto los puntos del kakuro en cuestión
     */
    public static int getPuntosKakuro(String absolutePath)
    {
        return CD.getPuntosKakuro(absolutePath);
    }

    /** @brief Operación de mostrar la vista del repositorio personal del usuario.
    \pre <em>cierto</em>
    \post Se pasa a la vista del repositorio personal del usuario.
     */
    public static void mostrarVistaRepositorioPersonal()
    {
        new repoPersonalView();
    }

    /** @brief Operación de mostrar la vista que permite mostrar la lista de kakuros en una carpeta. .
    \pre <em>cierto</em>
    \post Se pasa a la vista del repositorio personal del usuario.
    <ul>
    <li>Si op = 2, el titulo de la vista es "Kakuros acabados</li>
    <li>Si op = 3, el titulo de la vista es "Kakuros del repositorio</li>
    <li>En otro caso, el titulo de la vista es "Kakuros empezados</li>
    </ul> */
    public static void mostrarVistaRepoFiles(int op)
    {
        new repoFilesView(op);
    }

    /**@brief Operación consultora que dado un entero op, nos devuelve una lista de nombres de kakuros
    \pre <em>El parámetro op ha de estar entre 1 y 3(incluidos)</em>
    \post Si op vale 1, nos devuelve los nombres de los kakuros de la carpeta partidasUser del usuario actual
    Si op vale 2, nos devuelve los nombres de los kakuros de la carpeta PartidasAcabadas del usuario actual
    Si op vale 3, nos devuelve los nombres de los kakuros del Repositorio general
     */
    public static String[] getKakurosNames(int op)
    {
        return CD.getKakuroNames(op);
    }

    /** @brief Operación de mostrar la vista que permite meter un kakuro de forma manual.
    \pre <em>cierto</em>
    \post Se pasa a la vista que permite meter un kakuro de forma manual.
     */
    public static void mostrarVistaInsertarKakuroManual(int op)
    {
        new insertManualView(op);
    }

    /**@brief Operación consultora que nos devuelve la dificultado del kakuro del parámetro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto la dificultad del kakuro del p.i
     */
    public static int getDificultad()
    {
        return CD.getDificultad();
    }

    /** @brief Operación de mostrar la vista que muestra informacion del kakuro.
    \pre <em>cierto</em>
    \post Se pasa a la vista que muestra informacion del kakuro.
     */
    public static void mostrarVistaInfoKakuro(String camino, int op, String nombre)
    {
        new infoKakuroView(camino, op, nombre);
    }

    /** @brief Operación de mostrar la vista que permite cambiar la contraseña del usuario.
    \pre <em>cierto</em>
    \post Se pasa a la vista que permite cambiar la contraseña del usuario.
     */
    public static void mostrarVistaCambiarContrasena()
    {
        new changePasswordView();
    }

    /**@brief Operación modificadora que dado un nombre de usuario user y una contraseña pass, cambia la contraseña del usuario user
     * por la nueva contraseña pass
    \pre <em>El usuario user debe existir y pass no debe ser una contraseña vacío</em>
    \post Hemos cambiado la contraseña del usuario con nombre de usuario user
     */
    public static void cambiarContra(String user, String pass)
    {
        CD.cambiarContrasenya(user, pass);
    }

    /**@brief Operación consultora que dados un nombre de archivo, y un path a una carpeta nos devuelve true si el fichero esta
     * en la carpeta. En caso contrario, devuelve falso
    \pre <em>Cierto</em>
    \post Hemos devuelto true si la carpeta contiene el fichero con ese nombre, en caso contrario devolvemos falso
     */
    public static boolean noExisteEsteFichero(String fichero, String carpeta)
    {
        return CD.fileDontExist(fichero, carpeta);
    }

    /** @brief Operación para iniciar el menú
    Nos da las opciones de jugar, generar, resolver, verificar o, incluso, salir
    \pre <em>Cierto</emQ>
    \post Se abre el menú general que nos ofrece todas las funcionalidades disponibles
    y nos da el output por el canal estándar de salida
     */
    public static void iniciarPresentacionConsola()
    {
        utiles.clearScreen();
        int opcion = 0;
        while(opcion != 5) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 5) {
                    utiles.printNice("MENU PRINCIPAL");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Jugar", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Generar", "|"));
                    System.out.println(utiles.wellTabed("|          [3]: Resolver", "|"));
                    System.out.println(utiles.wellTabed("|          [4]: Verificar", "|"));
                    System.out.println(utiles.wellTabed("|          [5]: Salir", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                if (opcion != 5) utiles.clearScreen();
                if (opcion == 1) menuJugarConsola();
                else if (opcion == 2) menuGenerarConsola();
                else if (opcion == 3) menuResolverConsola();
                else if (opcion == 4) menuVerificarConsola();
            }
            catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Menú de jugar
    Nos da las opciones de jugar un Kakuro existente, crear uno nuevo, o salir de éste menú
    \pre <em>Cierto</em>
    \post El menú ha realizado las funciones deseadas por el usuario y al acabar volvemos al menú principal
     */
    public static void menuJugarConsola()
    {
        utiles.clearScreen();
        int opcion = 0;
        while (opcion != 3) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 4) {
                    utiles.printNice("JUGAR");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Jugar un Kakuro nuevo", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Jugar un Kakuro del Repositorio", "|"));
                    System.out.println(utiles.wellTabed("|          [3]: Salir de JUGAR", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                utiles.clearScreen();
                if (opcion == 1) {
                    boolean chosen = false;
                    int x = 0;
                    boolean valida = true;
                    while (!chosen) {
                        utiles.clearScreen();
                        utiles.printNice("JUGAR UN KAKURO NUEVO");
                        System.out.println(utiles.wellTabed("|     Introduce los datos del Kakuro el siguiente formato:", "|"));
                        System.out.println(utiles.wellTabed("|          [numFilas numColumnas Dificultad]: Kakuro numFilas x numColumnas con dificultad [1..3]", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]                              : Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        x = utiles.treatInput(s);
                        if (x == 0) {
                            valida = true;
                            double[] nums = convertStringtoDoubleArrayNums(s);
                            if (nums.length == 3) {
                                utiles.clearScreen();
                                utiles.printNice("GENERANDO KAKURO...");
                                CD.generateKakuro((int) nums[0], (int) nums[1], nums[2]);
                                utiles.clearScreen();
                                utiles.printNice("Kakuro Generado");
                                System.out.println(CD.printKakuroCD());

                            }
                            else {
                                System.out.println("Entrada no valida");
                                valida = false;
                            }
                            while (x < 1 || x > 3) {
                                utiles.printNice("JUGAR UN KAKURO NUEVO");
                                utiles.printNice("Escribe el número de la tarea que deseas hacer");
                                if (valida) System.out.println(utiles.wellTabed("|          [1]: Jugar el Kakuro generado", "|"));
                                System.out.println(utiles.wellTabed("|          [2]: Crear otro Kakuro", "|"));
                                System.out.println(utiles.wellTabed("|          [3]: Salir de JUGAR UN KAKURO NUEVO", "|"));
                                utiles.printLineEqual();
                                x = utiles.readInt();
                            }
                        }
                        utiles.clearScreen();
                        if (x == 1 || x == 3 || x == -1) chosen = true;
                    }
                    if (x == 1 && valida) {
                        utiles.clearScreen();
                        CD.playGame(null);
                    }
                }
                else if (opcion == 2) {
                    boolean refresca = true;
                    while (refresca) {
                        utiles.clearScreen();
                        refresca = false;
                        utiles.printNice("JUGAR UN KAKURO DEL REPOSITORIO");
                        System.out.println(utiles.wellTabed("|  Escribe el numero del archivo del kakuro a jugar", "|"));
                        System.out.println(utiles.wellTabed("|     Ficheros del repositorio:", "|"));

                        //aqui hay que coger de ctrlPersistencia //En principio done
                        String[] pathnames = CD.getPathNames("data/Repositorio/");
                        int tam = Objects.requireNonNull(pathnames).length;
                        for (int i = 0; i < tam; ++i) {
                            String x = "|          ["+i + "]: " + pathnames[i];
                            System.out.println(utiles.wellTabed(x, "|"));
                        }
                        System.out.println(utiles.Centered("", " "));
                        System.out.println(utiles.wellTabed("|          [R]: Refrescar", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        int x = utiles.treatInput(s);
                        if (x == 0) {
                            x = Integer.parseInt(s);
                            CD.readKakuroFromFile("data/Repositorio/"+pathnames[x]);
                            CD.playGame(null);
                        }
                        else if (x == -2) {
                            refresca = true;
                        }
                        else utiles.clearScreen();
                    }
                }

                else if (opcion == 3 && CD.getUsername()!=null){
                    //aqui mostramos las partidas empezadas del usuario
                    String username = CD.getUsername();
                    boolean refresca = true;
                    while (refresca) {
                        utiles.clearScreen();
                        refresca = false;
                        utiles.printNice("CONTINUAR UNA PARTIDA");
                        System.out.println(utiles.wellTabed("|  Escribe el numero del archivo del kakuro a jugar", "|"));
                        System.out.println(utiles.wellTabed("|     Ficheros del repositorio:", "|"));
                        //aqui lo cogemos de persistencia
                        String[] pathnames = CD.getPathNames("data/Users/" + username + "/partidasUser" );

                        int tam = Objects.requireNonNull(pathnames).length;
                        for (int i = 0; i < tam; ++i) {
                            String x = "|          ["+i + "]: " + pathnames[i];
                            System.out.println(utiles.wellTabed(x, "|"));
                        }
                        System.out.println(utiles.Centered("", " "));
                        System.out.println(utiles.wellTabed("|          [R]: Refrescar", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        int x = utiles.treatInput(s);
                        if (x == 0) {
                            x = Integer.parseInt(s);
                            CD.readKakuroFromFile("data/Users/" + username + "/partidasUser/" +pathnames[x]);
                            CD.playGame(pathnames[x]);
                        }
                        else if (x == -2) {
                            refresca = true;
                        }
                        else utiles.clearScreen();
                    }
                }
            }
            catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Menú de generar
    Nos da las opciones de generar un Kakuro desde cero o salir de éste menú
    \pre <em>Cierto</em>
    \post El menú ha realizado las funciones deseadas por el usuario y al acabar volvemos al menú principal
     */
    public static void menuGenerarConsola()
    {
        utiles.clearScreen();
        int opcion = 0;
        while (opcion != 2) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 3) {
                    utiles.printNice("GENERAR");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Generar un Kakuro nuevo", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Salir de GENERAR", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                utiles.clearScreen();
                if (opcion == 1) {
                    boolean chosen = false;
                    System.out.println();
                    int x = 0;
                    boolean valida = true;
                    while (!chosen) {
                        utiles.clearScreen();
                        utiles.printNice("GENERAR UN KAKURO NUEVO");
                        System.out.println(utiles.wellTabed("|     Introduce los datos del Kakuro el siguiente formato:", "|"));
                        System.out.println(utiles.wellTabed("|          [numFilas numColumnas Dificultad]: Kakuro numFilas x numColumnas con dificultad [1..3]", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]                              : Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        x = utiles.treatInput(s);
                        if (x == 0) {
                            valida = true;
                            double[] nums = convertStringtoDoubleArrayNums(s);
                            if (nums.length == 3) {
                                utiles.clearScreen();
                                utiles.printNice("GENERANDO KAKURO...");
                                CD.generateKakuro((int) nums[0], (int) nums[1], nums[2]);
                                utiles.clearScreen();
                                utiles.printNice("Kakuro Generado");
                                System.out.println(CD.printKakuroCD());
                            }
                            else {
                                System.out.println("Entrada no valida");
                                valida = false;
                            }
                            while (x < 1 || x > 5) {
                                utiles.printNice("GENERAR UN KAKURO NUEVO");
                                utiles.printNice("Escribe el número de la tarea que deseas hacer");
                                if (valida) {
                                    System.out.println(utiles.wellTabed("|          [1]: Resolver el Kakuro generado", "|"));
                                    System.out.println(utiles.wellTabed("|          [2]: Verficar el Kakuro generado", "|"));
                                }
                                System.out.println(utiles.wellTabed("|          [3]: Guardar el Kakuro generado", "|"));
                                System.out.println(utiles.wellTabed("|          [4]: Crear otro Kakuro", "|"));
                                System.out.println(utiles.wellTabed("|          [5]: Salir de GENERAR UN KAKURO NUEVO", "|"));
                                utiles.printLineEqual();
                                x = utiles.readInt();
                            }
                        }
                        utiles.clearScreen();
                        if (x == 1 || x == 2 || x == 3 || x == 5 || x == -1) chosen = true;
                    }
                    if (x == 1 && valida) {
                        utiles.clearScreen();
                        utiles.printNice("Kakuro Generado");
                        System.out.println(CD.printKakuroCD());
                        utiles.printNice("RESOLVIENDO...");
                        CD.solveKakuro();
                        utiles.clearScreen();
                        utiles.printNice("SOLUCIÓN KAKURO GENERADO");
                        System.out.println(CD.printKakuroCD());
                    }
                    else if (x == 2 && valida) {
                        utiles.clearScreen();
                        utiles.printNice("Kakuro Generado");
                        System.out.println(CD.printKakuroCD());
                        utiles.printNice("VERIFICANDO...");
                        int ver = CD.verifyKakuro();
                        utiles.clearScreen();
                        utiles.printNice("VERIFICACIÓN KAKURO GENERADO");
                        System.out.println(utiles.Centered(" ", " "));
                        if (ver==1 || ver==-1) {
                            System.out.println(utiles.Centered("Tiene solución única", " "));
                            System.out.println(utiles.Centered("¿Quieres guardar la solución de este Kakuro? Sí[1]/No[0]", " "));
                            System.out.println(utiles.Centered(" ", " "));
                            utiles.printLineEqual();
                            int save = utiles.readInt();
                            if (save == 1){
                                utiles.printNiceWithSpaces("¿Que nombre quieres darle a tu archivo? [no utilizar carácteres como: /,:,*,?,<,>,|]");
                                Scanner e = new Scanner(System.in);
                                String name = e.nextLine();
                                CD.guardarKakuroEnRepositorio(name,false);
                            }
                        }
                        else if (ver==0) System.out.println(utiles.Centered("No tiene solución", " "));
                        else System.out.println(utiles.Centered("Tiene más de una posible solución"," "));
                        System.out.println(utiles.Centered(" ", " "));
                        utiles.printLineEqual();
                    }
                    if (x == 3){
                        utiles.printNiceWithSpaces("¿Que nombre quieres darle a tu archivo? [no utilizar carácteres como: /,:,*,?,<,>,|]");
                        Scanner e = new Scanner(System.in);
                        String name = e.nextLine();
                        CD.guardarKakuroEnRepositorio(name,false);
                    }
                }
            }
            catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Menú de resolver
    Nos da las opciones de resolver un Kakuro nuevo, resolver uno existente, o salir de éste menú
    \pre <em>Cierto</em>
    \post El menú ha realizado las funciones deseadas por el usuario y al acabar volvemos al menú principal
     */
    public static void menuResolverConsola()
    {
        utiles.clearScreen();
        int opcion = 0;
        while (opcion != 3) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 3) {
                    utiles.printNice("RESOLVER");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Resolver un Kakuro nuevo", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Resolver un Kakuro del Repositorio", "|"));
                    System.out.println(utiles.wellTabed("|          [3]: Salir de RESOLVER", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                utiles.clearScreen();
                if (opcion == 1) {
                    boolean chosen = false;
                    int x = 0;
                    boolean valida = true;
                    while (!chosen) {
                        utiles.clearScreen();
                        utiles.printNice("RESOLVER UN KAKURO NUEVO");
                        System.out.println(utiles.wellTabed("|     Introduce los datos del Kakuro el siguiente formato:", "|"));
                        System.out.println(utiles.wellTabed("|          [numFilas numColumnas Dificultad]: Kakuro numFilas x numColumnas con dificultad [1..3]", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]                              : Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        x = utiles.treatInput(s);
                        if (x == 0) {
                            valida = true;
                            double[] nums = convertStringtoDoubleArrayNums(s);
                            if (nums.length == 3) {
                                utiles.clearScreen();
                                utiles.printNice("GENERANDO KAKURO...");
                                CD.generateKakuro((int) nums[0], (int) nums[1], nums[2]);
                                utiles.clearScreen();
                                utiles.printNice("Kakuro Generado");
                                System.out.println(CD.printKakuroCD());
                            }
                            else {
                                System.out.println("Entrada no valida");
                                valida = false;
                            }
                            while (x < 1 || x > 3) {
                                utiles.printNice("RESOLVER UN KAKURO NUEVO");
                                utiles.printNice("Escribe el número de la tarea que deseas hacer");
                                if (valida) System.out.println(utiles.wellTabed("|          [1]: Resolver el Kakuro generado", "|"));
                                System.out.println(utiles.wellTabed("|          [2]: Crear otro nuevo", "|"));
                                System.out.println(utiles.wellTabed("|          [3]: Menu Resolver", "|"));
                                utiles.printLineEqual();
                                x = utiles.readInt();
                            }
                            utiles.clearScreen();
                            if (x == 1 || x == 3) chosen = true;
                        }
                        else if (x == -1) {
                            utiles.clearScreen();
                            chosen = true;
                        }
                    }
                    if (x == 1 && valida) {
                        utiles.clearScreen();
                        utiles.printNice("Kakuro Generado");
                        System.out.println(CD.printKakuroCD());
                        utiles.printNice("RESOLVIENDO...");
                        boolean sol = CD.solveKakuro();
                        utiles.clearScreen();
                        if(sol) utiles.printNice("SOLUCIÓN KAKURO GENERADO");
                        else utiles.printNice("NO TIENE SOLUCIÓN");
                        System.out.println(CD.printKakuroCD());
                    }

                }
                else if (opcion == 2) {
                    boolean refresca = true;
                    while (refresca) {
                        utiles.clearScreen();
                        refresca = false;
                        utiles.printNice("RESOLVER UN KAKURO DEL REPOSITORIO");
                        System.out.println(utiles.wellTabed("|  Escribe el numero del archivo del kakuro a resolver", "|"));
                        System.out.println(utiles.wellTabed("|     Ficheros del repositorio:", "|"));
                        String[] pathnames = CD.getPathNames("data/Repositorio/");

                        int tam = Objects.requireNonNull(pathnames).length;
                        for (int i = 0; i < tam; ++i) {
                            String x = "|          ["+i + "]: " + pathnames[i];
                            System.out.println(utiles.wellTabed(x, "|"));
                        }
                        System.out.println(utiles.Centered("", " "));
                        System.out.println(utiles.wellTabed("|          [R]: Refrescar", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        int x = utiles.treatInput(s);
                        if(x == 0) {
                            x = Integer.parseInt(s);
                            if (x < tam) {
                                utiles.clearScreen();
                                utiles.printNice("Kakuro "+(pathnames[x]));
                                CD.readKakuroFromFile("data/Repositorio/"+pathnames[x]);
                                System.out.println(CD.printKakuroCD());
                                utiles.printNice("RESOLVIENDO...");
                                boolean sol = CD.solveKakuro();
                                utiles.clearScreen();
                                if(sol) utiles.printNice("SOLUCIÓN " + (pathnames[x]));
                                else utiles.printNice((pathnames[x]) + " NO TIENE SOLUCIÓN");
                                System.out.println(CD.printKakuroCD());
                            }
                        }
                        else if (x == -2) {
                            refresca = true;
                        }
                        else utiles.clearScreen();
                    }
                }

            } catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Menú de verificar
    Nos da las opciones de verificar un Kakuro nuevo, verificar uno existente, o salir de éste menú
    \pre <em>Cierto</em>
    \post El menú ha realizado las funciones deseadas por el usuario y al acabar volvemos al menú principal
     */
    public static void menuVerificarConsola()
    {
        utiles.clearScreen();
        int opcion = 0;
        while (opcion != 3) {
            try {
                opcion = 0;
                while (opcion < 1 || opcion > 3) {
                    utiles.printNice("VERIFICAR");
                    System.out.println(utiles.wellTabed("|     Escribe el número de la tarea que deseas hacer:", "|"));
                    System.out.println(utiles.wellTabed("|          [1]: Verificar un Kakuro nuevo", "|"));
                    System.out.println(utiles.wellTabed("|          [2]: Verificar un Kakuro del Repositorio", "|"));
                    System.out.println(utiles.wellTabed("|          [3]: Salir de VERIFICAR", "|"));
                    utiles.printLineEqual();
                    opcion = utiles.readInt();
                }
                utiles.clearScreen();
                if (opcion == 1) {
                    boolean chosen = false;
                    //Kakuro k = new Kakuro();
                    int x = 0;
                    boolean valida = true;
                    while (!chosen) {
                        utiles.clearScreen();
                        utiles.printNice("VERIFICAR UN KAKURO NUEVO");
                        System.out.println(utiles.wellTabed("|     Introduce los datos del Kakuro el siguiente formato:", "|"));
                        System.out.println(utiles.wellTabed("|          [numFilas numColumnas Dificultad]: Kakuro numFilas x numColumnas con dificultad [1..3]", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]                              : Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        x = utiles.treatInput(s);
                        if (x == 0) {
                            valida = true;
                            double[] nums = convertStringtoDoubleArrayNums(s);
                            if (nums.length == 3) {
                                utiles.clearScreen();
                                utiles.printNice("GENERANDO KAKURO...");
                                CD.generateKakuro((int) nums[0], (int) nums[1], (int)nums[2]);
                                utiles.clearScreen();
                                utiles.printNice("Kakuro Generado");
                                System.out.println(CD.printKakuroCD());
                            }
                            else {
                                System.out.println("Entrada no valida");
                                valida = false;
                            }
                            while (x < 1 || x > 3) {
                                utiles.printNice("VERIFICAR UN KAKURO NUEVO");
                                utiles.printNice("Escribe el número de la tarea que deseas hacer");
                                if (valida) System.out.println(utiles.wellTabed("|          [1]: Verificar el Kakuro generado", "|"));
                                System.out.println(utiles.wellTabed("|          [2]: Crear otro nuevo", "|"));
                                System.out.println(utiles.wellTabed("|          [3]: Menu VERIFICAR", "|"));
                                utiles.printLineEqual();
                                x = utiles.readInt();
                            }
                            utiles.clearScreen();
                            if (x == 1 || x == 3) chosen = true;
                        }
                        else if (x == -1) {
                            utiles.clearScreen();
                            chosen = true;
                        }
                    }
                    if (x == 1 && valida) {
                        utiles.clearScreen();
                        utiles.printNice("Kakuro Generado");
                        System.out.println(CD.printKakuroCD());
                        utiles.printNice("VERIFICANDO...");
                        int ver = CD.verifyKakuro();
                        utiles.clearScreen();
                        utiles.printNice("VERIFICACIÓN KAKURO CREADO");
                        System.out.println(utiles.Centered(" ", " "));
                        if (ver==1 || ver==-1){
                            System.out.println(utiles.Centered("Tiene solución única", " "));
                            System.out.println(utiles.Centered("¿Quieres guardar la solución de este Kakuro? Sí[1]/No[0]", " "));
                            System.out.println(utiles.Centered(" ", " "));
                            utiles.printLineEqual();
                            int save = utiles.readInt();
                            if (save == 1){
                                utiles.printNiceWithSpaces("¿Que nombre quieres darle a tu archivo? [no utilizar carácteres como: /,:,*,?,<,>,|]");
                                Scanner e = new Scanner(System.in);
                                String name = e.nextLine();
                                CD.guardarKakuroEnRepositorio(name,true);
                            }
                        }
                        else if (ver==0) System.out.println(utiles.Centered("No tiene solución", " "));
                        else System.out.println(utiles.Centered("Tiene más de una posible solución"," "));
                        System.out.println(utiles.Centered(" ", " "));
                        utiles.printLineEqual();
                    }
                }
                else if (opcion == 2) {
                    boolean refresca = true;
                    while (refresca) {
                        utiles.clearScreen();
                        refresca = false;
                        utiles.printNice("VERIFICAR UN KAKURO DEL REPOSITORIO");
                        System.out.println(utiles.wellTabed("|  Escribe el numero del archivo del kakuro a verificar", "|"));
                        System.out.println(utiles.wellTabed("|     Ficheros del repositorio:", "|"));
                        String[] pathnames = CD.getPathNames("data/Repositorio/");
                        int tam = Objects.requireNonNull(pathnames).length;
                        for (int i = 0; i < tam; ++i) {
                            String x = "|          ["+i + "]: " + pathnames[i];
                            System.out.println(utiles.wellTabed(x, "|"));
                        }
                        System.out.println(utiles.Centered("", " "));
                        System.out.println(utiles.wellTabed("|          [R]: Refrescar", "|"));
                        System.out.println(utiles.wellTabed("|          [Q]: Salir", "|"));
                        utiles.printLineEqual();
                        String s = utiles.readLine();
                        int x = utiles.treatInput(s);
                        if(x == 0) {
                            x = Integer.parseInt(s);
                            if (x < tam) {
                                utiles.clearScreen();
                                CD.readKakuroFromFile("data/Repositorio/"+pathnames[x]);
                                utiles.printNice("Kakuro "+(pathnames[x]));
                                System.out.println(CD.printKakuroCD());
                                utiles.printNice("VERIFICANDO...");
                                int ver = CD.verifyKakuro();
                                utiles.clearScreen();
                                utiles.printNice("VERIFICACIÓN "+(pathnames[x]));
                                System.out.println(utiles.Centered(" ", " "));
                                if (ver==1 || ver==-1){
                                    System.out.println(utiles.Centered("Tiene solución única", " "));
                                    System.out.println(utiles.Centered("¿Quieres guardar la solución de este Kakuro? Sí[1]/No[0]", " "));
                                    System.out.println(utiles.Centered(" ", " "));
                                    utiles.printLineEqual();
                                    int save = utiles.readInt();
                                    if (save == 1){
                                        utiles.printNiceWithSpaces("¿Que nombre quieres darle a tu archivo? [no utilizar carácteres como: /,:,*,?,<,>,|]");
                                        Scanner e = new Scanner(System.in);
                                        String name = e.nextLine();
                                        CD.guardarKakuroEnRepositorio(name,true);
                                    }
                                }
                                else if (ver==0) System.out.println(utiles.Centered("No tiene solución", " "));
                                else System.out.println(utiles.Centered("Tiene más de una posible solución"," "));
                                if(ver!=1) {
                                    System.out.println(utiles.Centered(" ", " "));
                                    utiles.printLineEqual();
                                }
                            }
                            else System.out.println("No existe el número de archivo indicado");
                        }
                        else if (x == -2) {
                            refresca = true;
                        }
                        else utiles.clearScreen();
                    }
                }

            } catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }
    }

    /** @brief Operación de lectura de lista de números
    \pre <em>s no vacío</em>
    \post Si había tres números, retorna un vector con los tres números. Sinó, retorna un vector vacío
     */
    public static double[] convertStringtoDoubleArrayNums(String s)
    {
        String[] linea = s.split(" ");
        double[] z = new double[0];
        if (linea.length == 3) {
            double[] nums = new double[3];
            nums[0] = Double.parseDouble(linea[0]);
            nums[1] = Double.parseDouble(linea[1]);
            nums[2] = Double.parseDouble(linea[2]);
            return nums;
        }
        return z;
    }
}
/** @file CtrlGenerate.java
 @brief Especificación del controlador CtrlGenerate
 */

package Dominio.Controladores;

import Dominio.Clases.Kakuro;

import java.util.*;

/** @class CtrlGenerate
 @brief Controlador dedicado a la generación de Kakuros.

 Hecha por Armando Rodríguez

 Contiene diversas funciones que son capaces de crear diferentes tipos de Kakuros de distintos tamaños y dificultad.
 */
public class CtrlGenerate
{
    /** @brief Kakuro que será generado para facilitar uso de funciones.*/
    private Kakuro k;

    //Creadoras
    /** @brief Creadora por defecto.
    \pre <em>Cierto</em>
    \post El resultado es un nuevo controlador de generación.
     */
    public CtrlGenerate()
    {
        k = new Kakuro();
    }

    /** @brief Creadora con parametro kakuro.
    \pre <em>Cierto</em>
    \post El resultado es un nuevo controlador de generación con k=in.
     */
    public CtrlGenerate(Kakuro in)
    {
        k = in;
    }

    /** @brief Consultora del Kakuro de la clase.
    \pre <em>Cierto</em>
    \post El resultado es el Kakuro de la clase.
     */
    public Kakuro getKakuro ()
    {
        return k;
    }

    /** @brief Función que indica la primera casilla blanca a partir de ii,jj
    \pre <em>Cierto</em>
    \post Devuelve cierto si hay alguna casilla blanca, las coordenadas de la primera encontrada estarán en ii,jj.
    Devuelve falso si no hay ninguna casilla blanca, ii,jj no tendrá sentido tratarlos
     */
    private boolean firstWhiteTile(int[] ii, int[] jj)
    {
        if (jj[0] == k.getKakuroCols()-1) { ++ii[0]; jj[0] = 0; }
        else ++jj[0];
        while(ii[0] != k.getKakuroRows() && k.isBlackKakuroTile(ii[0],jj[0])) {
            if (jj[0] == k.getKakuroCols()-1) { ++ii[0]; jj[0] = 0;}
            else ++jj[0];
        }
        return ii[0] != k.getKakuroRows();
    }

    /** @brief Función que borra el valor de todas las casillas blancas.
    \pre <em>Cierto</em>
    \post El Kakuro del parametro implicito ha quedado con valor 0 en todas las casillas blancas.
     */
    private void eraseWhiteTileValues()
    {
        for (int i = 0; i < k.getKakuroRows(); ++i) {
            for (int j = 0; j < k.getKakuroCols(); ++j) {
                if (!k.isBlackKakuroTile(i,j)) {
                    k.setWhiteTile(i,j);
                }
            }
        }
    }

    /** @brief Operación que pone una cifra determinada al valor de la derecha de una casilla negra i,j
    \pre <em>Cierto</em>
    \post Se ha puesto en el valor Right de la casilla negra i,j la suma de las casillas blancas de la derecha
     */
    private void setValueToBlackRight(int i, int j)
    {
        int sum = 0;
        for(int l = j+1; l < k.getKakuroCols() && !k.isBlackKakuroTile(i,l); ++l){
            sum += k.getValueTile(i,l);
        }
        if(sum != 0) k.setRightBlackTile(i,j,sum);
    }

    /** @brief Operación que pone una cifra determinada al valor de abajo de una casilla negra i,j
    \pre <em>Cierto</em>
    \post Se ha puesto en el valor Bottom de la casilla negra i,j la suma de las casillas blancas de abajo
     */
    private void setValueToBlackBottom(int i, int j)
    {
        int sum = 0;
        for(int l = i+1; l < k.getKakuroRows() && !k.isBlackKakuroTile(l,j); ++l){
            sum += k.getValueTile(l,j);
        }
        if(sum != 0) k.setBottomBlackTile(i,j,sum);
    }

    /** @brief Operación auxiliar que nos permite poner valores en la casillas negras
    \pre <em>Cierto</em>
    \post Se han metido en todas las casillas negras los valores rigth que pertenecen a las casillas blancas de su derecha
          y los valores bottom que pertenecen a las casillas blancas de abajo
     */
    private void putValuesInBlackTiles()
    {
        for(int i = 0; i < k.getKakuroRows(); ++i){
            for(int j = 0; j < k.getKakuroCols(); ++j){
                if(k.isBlackKakuroTile(i,j)){
                    setValueToBlackRight(i, j);
                    setValueToBlackBottom(i, j);
                }
            }
        }
    }

    /** @brief Operación auxiliar que nos permite determinar si creamos un caso trivial
    \pre <em>dificultad = {1,2,3}</em>
    \post Consideramos trivial el caso en el que haya menos de "dificultad" espacios entre casillas negras.
          Devuelve cierto si se crea un caso trivial al poner una negra en el casilla i,j, devuelve falso si no ocurre.
          Con dificultad 2 se prueba de manera aleatoria espacios entre 1 y 2 y con dificultad 3 siempre con 2 espacios.
     */
    private boolean trivialMaked(int i, int j, int dificultad)
    {
        Random rn = new Random();
        int separation = dificultad; //dif == 1 -> 100% 1

        boolean stop = false, some_white = false;
        if(dificultad == 2){
            if (rn.nextInt(3) > 0) separation = 1; //66% 1 and 33% 2
        }
        if(dificultad == 3){
            if (rn.nextInt(3) == 0) separation = 1; //33% 1 and 66% 2
            else separation = 2;
        }
        if(separation == 2 && i == 2 && !k.isBlackKakuroTile(i-1,j)) return true;
        if(i > separation){
            for(int l = 1; !stop && l <= separation; ++l){
                if(k.isBlackKakuroTile(i-l,j)) stop = true;
                else some_white = true;
            }
            if(some_white){
                if(stop) return true;
                else if (k.isBlackKakuroTile(i - 1 - separation, j)) return true;
            }
        }

        stop = false; some_white = false;
        if(dificultad == 2){
            if (rn.nextInt(3) > 0) separation = 1; //66% 1 and 33% 2
            else separation = 2;
        }
        if(dificultad == 3){
            if (rn.nextInt(3) == 0) separation = 1; //33% 1 and 66% 2
            else separation = 2;
        }
        if(separation == 2 && j == 2 && !k.isBlackKakuroTile(i,j-1)) return true;
        if(j > separation){
            for(int l = 1; !stop && l <= separation; ++l){
                if(k.isBlackKakuroTile(i,j-l)) stop = true;
                else some_white = true;
            }
            if(some_white){
                if(stop) return true;
                else if (k.isBlackKakuroTile(i, j-1-separation)) return true;
            }
        }

        stop = false; some_white = false;
        if(dificultad == 2){
            if (rn.nextInt(3) > 0) separation = 1; //66% 1 and 33% 2
            else separation = 2;
        }
        if(dificultad == 3){
            if (rn.nextInt(3) == 0) separation = 1; //33% 1 and 66% 2
            else separation = 2;
        }
        if(separation == 2 && i == k.getKakuroRows()-2 && !k.isBlackKakuroTile(i+1,j)) return true;
        if(i < k.getKakuroRows()-separation){
            for(int l = 1; !stop && l <= separation; ++l){
                if(k.isBlackKakuroTile(i+l,j)) stop = true;
                else some_white = true;
            }
            if(some_white){
                if(stop) return true;
                else{
                    if (i + separation == k.getKakuroRows() - 1) return true;
                    if (k.isBlackKakuroTile(i + 1 + separation, j)) return true;
                }
            }
        }

        stop = false; some_white = false;
        if(dificultad == 2){
            if (rn.nextInt(3) > 0) separation = 1; //66% 1 and 33% 2
            else separation = 2;
        }
        if(dificultad == 3){
            if (rn.nextInt(3) == 0) separation = 1; //33% 1 and 66% 2
            else separation = 2;
        }
        if(separation == 2 && j == k.getKakuroCols()-2 && !k.isBlackKakuroTile(i,j+1)) return true;
        if(j < k.getKakuroCols()-separation){
            for(int l = 1; !stop && l <= separation; ++l){
                if(k.isBlackKakuroTile(i,j+l)) stop = true;
                else some_white = true;
            }
            if(some_white){
                if(stop) return true;
                else{
                    if (j + separation == k.getKakuroCols() - 1) return true;
                    return k.isBlackKakuroTile(i, j + 1 + separation);
                }
            }
        }
        return false;
    }

    /** @brief Operación auxiliar que nos permite completar con negras las casillas que son triviales (negra <-> blanca <-> negra)
    \pre <em>Cierto</em>
    \post Han desaparecido todos los casos triviales que se han encontardo en orden.
          Importante: Puede provocar que aparezcan nuevos casos triviales.
          Devuelve cierto si ha encontrado algun caso trivial, falso en caso contrario
     */
    private boolean completeUniqueness()
    {
        boolean someUnique = false;
        for (int i = 1; i < k.getKakuroRows(); ++i) {
            for (int j = 1; j < k.getKakuroCols(); ++j) {
                if(!k.isBlackKakuroTile(i,j)) {
                    if (i < k.getKakuroRows()) {
                        if (k.isBlackKakuroTile(i - 1, j) && (i==k.getKakuroRows()-1 || k.isBlackKakuroTile(i + 1, j))) {
                            k.setBlackTile(i, j);
                            someUnique = true;
                        }
                    }
                    if (j < k.getKakuroCols()) {
                        if (k.isBlackKakuroTile(i, j-1) && (j==k.getKakuroCols()-1 || k.isBlackKakuroTile(i, j+1))) {
                            k.setBlackTile(i, j);
                            someUnique = true;
                        }
                    }
                }
            }
        }
        return someUnique;
    }

    /** @brief Operación auxiliar que construye un layout lo mas abierto posible
    \pre <em>Cierto</em>
    \post Se han creado el layout mas general y abierto que ha podido de manera aleatoria
     */
    private void createLayout()
    {
        Random rn = new Random();
        for (int i = 1; i < k.getKakuroRows(); ++i) {
            for (int j = 1; j < k.getKakuroCols(); ++j) {
                boolean[] vec = new boolean[9];
                boolean stop = false, anyPossible = false;
                while(!stop) {
                    int value = rn.nextInt(9) + 1;
                    while(vec[value-1]) value = rn.nextInt(9) + 1;

                    vec[value - 1] = true;

                    boolean rowCheck = k.rowCheck(i, j, value, 2) == 0;
                    boolean colCheck = k.columnCheck(i, j, value, 2) == 0;

                    stop = true;
                    if (rowCheck && colCheck) k.setWhiteTile(i, j, value);
                    else {
                        for (int k = 0; stop && k < 9; ++k) stop = vec[k];
                        anyPossible = stop;
                    }
                }
                if(anyPossible) k.setBlackTile(i,j);
            }
        }
    }

    /** @brief Operacion que rellena el kakuro de casillas negras comprimiendolo lo maximo que puede
    \pre <em>dificultad = {1,2,3}</em>
    \post Se han puesto casillas negras en el kakuro de la clase con respecto a la dificultad,
          el bool ordered indica si hemos realizado la colocación ordenadamente o no.
     */
    private void fillWithBlack(int dificultad, boolean ordered)
    {
        ArrayList<Integer> rows = new ArrayList<>();
        ArrayList<Integer> cols = new ArrayList<>();
        if(!ordered) {
            for (int i = 0; i < k.getKakuroRows(); ++i) rows.add(i);
            Collections.shuffle(rows);
            for (int i = 0; i < k.getKakuroCols(); ++i) cols.add(i);
            Collections.shuffle(cols);
        }
        for (int i = 0; i < k.getKakuroRows(); ++i) {
            int ii = (ordered)? i : rows.get(i);
            for (int j = 0; j < k.getKakuroCols(); ++j) {
                int jj = (ordered)? j : cols.get(j);
                if(!k.isBlackKakuroTile(ii,jj)) {
                    k.setBlackTile(ii,jj);
                    if(trivialMaked(ii,jj,dificultad) || !k.connectedKakuro())
                        k.setWhiteTile(ii,jj);
                }
            }
        }
    }

    /** @brief Estructura de datos con las mejores combinaciones*/
    int[][][] goodCombinations = new int[][][]{
            {{1,2},{1,3},{8,9},{7,9}},
            {{1,2,3},{1,2,4},{6,8,9},{7,8,9}},
            {{1,2,3,4},{1,2,3,5},{5,7,8,9},{6,7,8,9}},
            {{1,2,3,4,5},{1,2,3,4,6},{4,6,7,8,9},{5,6,7,8,9}},
            {{1,2,3,4,5,6},{1,2,3,4,5,7},{3,5,6,7,8,9},{4,5,6,7,8,9}},
            {{1,2,3,4,5,6,7},{1,2,3,4,5,6,8},{2,4,5,6,7,8,9},{3,4,5,6,7,8,9}},
            {{1,2,3,4,5,6,7,8},{1,2,3,4,5,6,7,9},{1,2,3,4,5,6,8,9},
             {1,2,3,4,5,7,8,9},{1,2,3,4,6,7,8,9},{1,2,3,5,6,7,8,9},
             {1,2,4,5,6,7,8,9},{1,3,4,5,6,7,8,9},{2,3,4,5,6,7,8,9}},
            {{1,2,3,4,5,6,7,8,9}}
    };

    /** @brief Estructura de datos con las mejores combinaciones desordenadas*/
    int[][][] goodCombinationsNotOrdered = new int[][][]{
            {{1,2},{1,3},{8,9},{7,9}},
            {{1,3,2},{4,2,1},{6,8,9},{7,9,8}},
            {{1,4,3,2},{3,5,1,2},{9,8,5,7},{6,7,9,8}},
            {{4,3,2,1,5},{1,6,3,2,4},{6,9,4,7,8},{9,6,8,5,7}},
            {{3,6,1,5,4,2},{7,3,5,4,2,1},{3,7,5,6,9,8},{6,9,4,7,8,5}},
            {{7,3,5,6,2,1,4},{1,3,5,4,2,6,8},{8,5,4,6,7,2,9},{3,8,9,6,7,4,5}},
            {{1,3,5,4,2,6,7,8},{7,2,3,1,5,6,4,9},{1,5,2,4,3,6,8,9},
             {9,8,7,5,4,3,1,2},{1,3,8,4,6,7,2,9},{6,2,8,5,1,7,3,9},
             {8,4,1,2,6,7,5,9},{1,3,7,8,6,4,5,9},{2,3,4,5,6,7,8,9}},
            {{8,2,4,1,5,3,7,6,9}}
    };

    /** @brief Operacion que prueba de poner la combinación comb a la derecha de la posicion negra i,j
    \pre <em>i,j ha de ser una casilla negra, comb.size ha de ser el mismo que el número de espacios que haya a la derecha de la casilla i,j</em>
    \post Devuelve cierto si se puede introducir la combinación, devuelve false si no se puede
     */
    private boolean tryPutCombinationRight(int i, int j, int[] comb)
    {
        for(int l = 1; l <= comb.length; ++l){
            if(k.tryToWrite(i,j+l,comb[l-1],2) == 1) return false;
            k.setWhiteTile(i,j+l,comb[l-1]);
        }
        return true;
    }

    /** @brief Operacion que coloca buenos números para la creación de solución única
    \pre <em>Cierto</em>
    \post Devuelve cierto si ha podido poner números correctos y ha completado el kakuro. Devuelve false en caso contrario
     */
    private boolean setGoodNumbers()
    {
        CtrlSolve sol = new CtrlSolve(k);
        Random rn = new Random();
        for(int i = 0; i < k.getKakuroRows(); ++i) {
            for (int j = 0; j < k.getKakuroCols(); ++j) {
                if(k.isBlackKakuroTile(i,j)){
                    int spacesRight  = sol.howManySpacesRight(i, j);
                    if (spacesRight >= 2) {
                        int possibleGoodCombinations;
                        possibleGoodCombinations = goodCombinations[spacesRight-2].length;
                        boolean[] tested = new boolean[possibleGoodCombinations];
                        boolean correct = false;
                        int completed = 0;
                        while (!correct && completed < possibleGoodCombinations) {
                            int place = rn.nextInt(possibleGoodCombinations);
                            while (tested[place]) place = rn.nextInt(possibleGoodCombinations);
                            tested[place] = true;
                            int[] comb;
                            if(k.getKakuroRows() > 12 || k.getKakuroCols() > 12) comb = goodCombinations[spacesRight-2][place];
                            else comb = goodCombinationsNotOrdered[spacesRight-2][place];
                            correct = tryPutCombinationRight(i,j,comb);
                            ++completed;
                        }
                        if(!correct) return false;
                    }
                }
            }
        }
        return true;
    }

    /** @brief Función principal de la clase que devuelve un Kakuro generado
    \pre <em>El parámetro prob indica la dificultad de generación del Kakuro por lo tanto 1 <= prob <= 3</em>
    \post Devuelve el parámetro implícito de la clase una vez se ha sobreescrito con un Kakuro generado de tamaño nxm
    y con una simetria escogida de manera aleatoria (si el Kakuro no es cuadrado no se permiten simetrias diagonales).
    El Kakuro esta listo para ser jugado, todas las casillas blancas están sin ningún valor.
     */
    public Kakuro generarKakuro(int n, int m, double dificultad)
    {
        int sols = 0;
        while(sols != 1) {
            int dif = (int) dificultad;
            k = new Kakuro(n, m, dif, -1);
            boolean stop;
            do {
                k.cleanKakuro(n, m);
                createLayout();
                stop = false;
                for(int intentosCasillasUnicas = 0; !stop && intentosCasillasUnicas < 20; ++intentosCasillasUnicas) {
                    stop = !completeUniqueness();
                }
            } while (!k.connectedKakuro() || !stop);
            fillWithBlack(dif, false);
            fillWithBlack(dif, true);
            for (int intentosSolUnica = 0; sols != 1 && intentosSolUnica < 20; ++intentosSolUnica) {
                boolean correct = setGoodNumbers();
                if(correct) {
                    putValuesInBlackTiles();
                    eraseWhiteTileValues();
                    Kakuro creado = new Kakuro(k);
                    CtrlVerify ver = new CtrlVerify(creado);
                    Kakuro[] solucion = new Kakuro[1];
                    sols = ver.verify(solucion);
                }
            }
        }
        return k;
    }

/*                  |===========================================================|                  */
/*                  | FUNCIONES HECHAS PUBLICAS PARA TESTEAR EL DRIVER GENERATE |                  */
/*                  |===========================================================|                  */
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public boolean firstWhiteTilePublic(int[] ii, int[] jj) {return firstWhiteTile(ii,jj);}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void eraseWhiteTileValuesPublic() {eraseWhiteTileValues();}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void setValueToBlackRightPublic(int i, int j) {setValueToBlackRight(i,j);}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void setValueToBlackBottomPublic(int i, int j) {setValueToBlackBottom(i,j);}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void putValuesInBlackTilesPublic() {putValuesInBlackTiles(); }
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public boolean setGoodNumbersPublic(){return setGoodNumbers();}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public boolean tryPutCombinationRightPublic(int i, int j, int[] comb){return tryPutCombinationRight(i,j,comb);}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void fillWithBlackPublic(int dificultad, boolean ordered) {fillWithBlack(dificultad,ordered); }
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public void createLayoutPublic() {createLayout();}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public boolean completeUniquenessPublic(){ return completeUniqueness();}
    /** @brief Función hecha pública para testear con el driverGenerate*/
    public boolean trivialMakedPublic(int i, int j, int dificultad){ return trivialMaked(i, j, dificultad);}
}
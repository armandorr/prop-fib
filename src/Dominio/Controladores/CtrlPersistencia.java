/** @file CtrlPersistencia.java
 @brief Especificación del controlador CtrlPersistencia
 */

package Dominio.Controladores;

import Persistencia.PersistenciaKakuro;
import Persistencia.PersistenciaUsuario;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/** @class CtrlPersistencia
 @brief Controlador de la capa de persistencia.

 Hecha por David Marin

 Contiene diversas funciones que son capaces de obtener datos, o borrarlos, de diferentes directorios y/o ficheros.
 */
public class CtrlPersistencia
{
    /** @brief Relacion con la clase persistenciaUsuario.*/
    PersistenciaUsuario persistenciaUsuario;
    /** @brief Relacion con la clase persistenciaKakuro.*/
    PersistenciaKakuro persistenciaKakuro;

    /** @brief Creadora por defecto.
    Se ejecuta al declarar un nuevo controlador de Persistencia.
    \pre <em>Cierto</em>
    \post El resultado es un nuevo controlador de persistencia.
     */
    public CtrlPersistencia()
    {
        persistenciaUsuario = new PersistenciaUsuario();
        persistenciaKakuro = new PersistenciaKakuro();
    }

    /** @brief Operación de crear un usuario nuevo
    \pre <em>pass y user no son vacíos</em>
    \post Si ya existe un usuario con nombre user, retorna falso. Sino, retorna cierto y crea todas las carpetas y datos pertinentes correspondientes al nuevo ususario
     */
    public boolean creaUsuario (String user, String pass)
    {
        return persistenciaUsuario.creaUsuario(user,pass);
    }

    /** @brief Operación de iniciar sesión
    \pre <em>cierto</em>
    \post Retorna cierto sii existe un usuario con nombre user y contraseña pass
     */
    public boolean loginUsuario(String user, String pass)
    {
        return persistenciaUsuario.loginUsuario(user, pass);
    }

    /** @brief Operación de leer un Kakuro
    \pre <em>el archivo de location contiene la represntación de un Kakuro</em>
    \post Retorna el contenido de ese fichero en String
     */
    public String readKakuro(String location)
    {
        return persistenciaKakuro.readKakuro(location);
    }

    /** @brief Operación de guardar en el repositorio
    \pre <em>stringKakuro contiene la representación oficial de un Kakuro</em>
    \post Se guarda dicha String en un fichero de texto (cuya extensión dice si es una solución o no) y cuyo nombre es name
     */
    public void guardarKakuroEnRepositorio(String name, String strKak, boolean isSol)
    {
        persistenciaKakuro.guardarKakuroEnRepositorio(name, strKak,isSol);
    }

    /** @brief Consultora de la lista de gicheros en una ubicación
    \pre <em>s es una ubicación válida</em>
    \post Se devuelve un array de strings representado el nombre de los ficheros que habia en la ubicación s.
     */
    public String[] getPathNames(String s)
    {
        File f = new File(s);
        return f.list();
    }

    /** @brief Operación de obtener el tiempo de una partida
    \pre <em>en path hay un fichero que representa un Kakuro que se está jugando</em>
    \post Retorna el tiempo, en ms, que lleva esa partida en juego
     */
    public int leerTiempoPartidaActual(String s)
    {
        return persistenciaKakuro.leerTiempoPartidaActual(s);
    }

    /** @brief Operación de guardar una partida
    \pre <em>kak contiene una representacion válida de la partida que se quiere guardar< y s es una ubicación válida/em>
    \post Se guarda la partida kak en la ubicación s
     */
    public void guardarGame(String kak, String s)
    {
        try {
            FileWriter game = new FileWriter(s);
            game.write(kak);
            game.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación de eliminar usuario
    \pre <em>existe un ususario con nombre user</em>
    \post Se eliminan todas las carpetas del usuario user
     */
    public void eliminarUsuario(String user)
    {
        persistenciaUsuario.eliminarUsuario(user);
    }

    /** @brief Operación de obtener contraseña
    \pre <em>existe un usuario con nombre user</em>
    \post Devuelve la contraseña de user
     */
    public String getPassword(String user)
    {
        return persistenciaUsuario.getPassword(user);
    }

    /** @brief Operación de guardar una partida acabada
    \pre <em>Username es un usuario real, save es una representación válida del kakuro de la partida que se quiere guardar/em>
    \post Si entre las partidas acabadas no habia ninguna con identificador igual a id entonces se guardan la partida junto con sus id y los puntos que aportó al usuario
     En otro caso, solo se guarda la partida si los puntos que aporta al usuario son mayores que los que aportaba la otra partida
     guardad que tenia el mismo id
     */
    public void guardarGameAcabado(int id, int puntos, String username, String save)
    {
        File f = new File("data/Users/" + username + "/partidasAcabadas");
        String[] pathnames = f.list();
        boolean encontrado = false;
        int i = 0;
        while (!encontrado && pathnames!=null && i<pathnames.length){
            if ((id + ".txt").equals(pathnames[i])) encontrado = true;
            else ++i;
        }
        try {
            if (encontrado) //aqui tenemos que mirar si esta partida acabada aporta más o menos puntos
            {
                int puntos2 = persistenciaKakuro.getPuntosGameAcabado("data/Users/" + username + "/partidasAcabadas/" + pathnames[i]);
                if (puntos > puntos2) //sobreescribimos kak con misma id
                {
                    FileWriter paw = new FileWriter("data/Users/" + username + "/partidasAcabadas/" + pathnames[i]);
                    paw.write(save);
                    paw.close();

                    // tambien hay que ir a modificar el score del archivo de estado.txt:
                    File f2 = new File("data/estado.txt");
                    Scanner scan = new Scanner(f2);
                    StringBuilder newEstado = new StringBuilder(scan.nextLine() + "\n");
                    while (scan.hasNextLine()) {
                        String linea = scan.nextLine();
                        String[] par = linea.split(" ");
                        if (par[0].equals(username)) { // cuando encontramos el user
                            newEstado.append(par[0]).append(" ").append(Integer.parseInt(par[1]) + (puntos - puntos2)).append("\n");
                            // le sumamos el incremento de puntos a su puntuacion anterior
                        } else newEstado.append(linea).append("\n");
                    }
                    scan.close(); //Linea anadida 09/12 hacer tests
                    FileWriter newE = new FileWriter(f2);
                    newE.write(newEstado.toString());
                    newE.close();
                    scan.close();
                }
                // no hace falta un else porque no ha habido mejoria
            } else { // es la primera vez que se juega un kakuro con esa ID pues hay que guardarlo tal cual
                FileWriter paw = new FileWriter("data/Users/" + username + "/partidasAcabadas/" + id + ".txt");
                paw.write(save);
                paw.close();

                // tambien hay que ir a modificar el score del archivo de estado.txt:
                File f2 = new File("data/estado.txt");
                Scanner scan = new Scanner(f2);
                StringBuilder newEstado = new StringBuilder(scan.nextLine() + "\n");
                while (scan.hasNextLine()) {
                    String linea = scan.nextLine();
                    String[] par = linea.split(" ");
                    if (par[0].equals(username)) { // cuando encontramos el user
                        newEstado.append(par[0]).append(" ").append(Integer.parseInt(par[1]) + (puntos)).append("\n");
                        // le sumamos el incremento de puntos a su puntuacion anterior
                    } else newEstado.append(linea).append("\n");
                }
                scan.close();
                FileWriter newE = new FileWriter(f2);
                newE.write(newEstado.toString());
                newE.close();
                scan.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** @brief Operación que retorna el siguiente ID que se puede utilizar
    \pre <em>cierto</em>
    \post Devuelve el siguiente id que se puede utilizar, si inc es cierto se aumenta el ultimo id que se puede utilizar y se
     actualiza el fichero estado.txt
     */
    public int nextKakuroID(boolean inc)
    {
        int lastID = persistenciaKakuro.getLastKakuroID();
        if(inc) persistenciaKakuro.setLastKakuroID(lastID+1);
        return lastID+1;
    }

    /** @brief Consultora del ranking
    \pre <em>cierto</em>
    \post Devuelve un string representado el ranking contenido el fichero estado.txt
     */
    public String getRanking()
    {
        StringBuilder ret = new StringBuilder();
        try {
            File f = new File("data/estado.txt");
            Scanner scan = new Scanner(f);
            scan.nextLine();
            ArrayList<String[]> rank = new ArrayList<>();
            while (scan.hasNextLine()) {
                String linea = scan.nextLine();
                String[] par = linea.split(" ");
                rank.add(par);
            }

            scan.close();

            Comparator<String[]> compareByScore = (p1, p2) ->
                    Integer.compare(Integer.parseInt(p2[1]), Integer.parseInt(p1[1]));

            rank.sort(compareByScore);
            for (String[] strings : rank) {
                ret.append(strings[0]).append(" ").append(strings[1]).append("\n");
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ret.toString();
    }

    /** @brief Operación de establecer la info. extra necesaria a un Kakuro
    \pre <em>en archivo hay un fichero de texto que contiene la representación en formato oficial de un Kakuro</em>
    \post Dicho archivo contiene ahora la ID del Kakuro que es ID, y su dificultad, que es dif
     */
    public void setDataToKakuro(int id, int dif, String archivo)
    {
        persistenciaKakuro.setDataToKakuro(id, dif, archivo);
    }

    /** @brief Operación de obtener la puntuación de una partida finalizada
    \pre <em>en path hay un archivo de texto que representa una partida acabada</em>
    \post Retorna la puntuación que jugar esa partida aportó al usuario que la jugó
     */
    public int getPuntosKakuro(String absolutePath)
    {
        return persistenciaKakuro.getPuntosGameAcabado(absolutePath);
    }

    /** @brief Operación de eliminar usuario de Estado
    \pre <em>existe un ususario con nombre user</em>
    \post Se elimina la fila de user de Estado.txt
     */
    public void eliminarUsuarioDeEstado(String user)
    {
        persistenciaUsuario.eliminarUsuarioEstado(user);
    }

    /** @brief Operación de cambiar contraseña
    \pre <em>existe un ususario con nombre user y pass no es vacía</em>
    \post La contraseña de user pasa a ser pass
     */
    public void cambiarContrasenya(String user, String pass)
    {
        persistenciaUsuario.changePass(user, pass);
    }

    /** @brief Operación de cambiar contraseña
    \pre <em>carpeta es una ubicacion valida</em>
    \post Devuelve cierto si no hay ningun archivo con nombre fichero en la ubicación carpeta.
     */
    public boolean fileDontExist(String fichero, String carpeta)
    {
        File f = new File(carpeta);
        String[] pathnames = f.list();
        boolean encontrado = false;
        int i = 0;
        while (i< Objects.requireNonNull(pathnames).length && !encontrado){
            if (pathnames[i].equals(fichero)) encontrado = true;
            ++i;
        }
        return !encontrado;
    }

    /** @brief Operación de guardar Kakuro en su representación en formato oficial
    \pre <em>kakuroStandard contiene la representación oficial de un Kakuro y path es un directorio que existe</em>
    \post Si en path había un archivo con nombre nombrePartida, lo actualiza y el archivo ahora es el contenido de kakuroStandard. Si no existía, lo crea con el mismo contenido
     */
    public void guardarKakuroFormatoStandard(String path, String nombrePartida, String kakuroStandard)
    {
        persistenciaKakuro.guardarKakuroFormatoStandard(path,nombrePartida,kakuroStandard);
    }

    /** @brief Operación para borrar un archivo
    \pre <em>path contiene la ruta de un archivo</em>
    \post El archivo en path se ha borrado
     */
    public void borrarFichero(String path)
    {
        File fileToDelete = new File(path);
        //noinspection ResultOfMethodCallIgnored
        fileToDelete.delete();
    }
}
/** @file CtrlSolve.java
 @brief Especificación del controlador CtrlSolve
 */

package Dominio.Controladores;

import Dominio.Clases.BlackTile;
import Dominio.Clases.Kakuro;
import Dominio.Clases.WhiteTile;

import java.util.*;

/** @class CtrlSolve
 @brief Controlador dedicado a la resolución de Kakuros.

 Hecha por Armando Rodríguez

 Contiene diversas funciones que son capaces de tratar un Kakuro y encontrar, si es possible, una solución.
 */
public class CtrlSolve
{
    /** @brief Kakuro de la clase*/
    private Kakuro k;

    /** @brief Creadora por defecto
     * \pre <em>Cierto</em>
     * \post Se ha creado una instancia de CtrlSolve vacía
     */
    public CtrlSolve()
    {

    }

    /** @brief Creadora con parámetro de entrada
     * \pre <em>Cierto</em>
     * \post Se ha creado una instáncia de CtrlSolve cuyo Kakuro k es el Kakuro pasado como parámetro
     */
    public CtrlSolve(Kakuro in)
    {
        k = in;
    }

    /** @brief Getter del Kakuro del parámetro implícito
     * \pre <em>Cierto</em>
     * \post Retorna el Kakuro k del parámetro implícito
     */
    public Kakuro getKakuroSolved()
    {
        return k;
    }

    /** @brief Función que resuelve el Kakuro de la clase
     * \pre <em>Cierto</em>
     * \post Retorna cierto sii el Kakuro k del parámetro implícito tiene mínimo una solución.
     * Además modifica el Kakuro de la clase resolviendolo si es posible.
     */
    public boolean solve_recursivo()
    {
        putAllWhiteTilesPossibilities();

        int i = 0, j = 0;
        while(i != k.getKakuroRows() && (k.isBlackKakuroTile(i,j) || k.getValueTile(i,j)!=0)) { //found next white Tile
            if (j == k.getKakuroCols() - 1) { ++i; j = 0;}
            else ++j;
        }
        if(i == k.getKakuroRows()) return true; //no white
        int[] numSols = new int[]{0};
        Kakuro[] solucion = new Kakuro[1];

        boolean tiene_sol = solver_verify_recursivo(i,j,numSols,false, solucion);

        if(!tiene_sol){
            for(int n = 0; n < k.getKakuroRows(); ++n){
                for(int m = 0; m < k.getKakuroCols(); ++m){
                    if(!k.isBlackKakuroTile(n,m)) k.setWhiteTile(n,m);
                }
            }
            return false;
        }
        return true;
    }

    /**@brief Función recursiva auxiliar de solve
     * El parametro verifying indica si se esta usando la función en modo de verificación o, en caso contrario, de resolución.
     * \pre <em>La casilla (i,j) es una casilla blanca, numSols[0] contiene el número de soluciones encontradas hasta el momento</em>
     * \post Retorna cierto si ha encontrado una solución cuando veryfing = false.
     * Retorna cierto si ha encontrado una única solución cuando veryfing = true.
     * En caso contrario devuelve falso.
     * Además el parámetro solucion contiene la primera solución que haya encontrado en el caso de que mínimo haya 1.
     */
    public boolean solver_verify_recursivo(int i, int j, int[] numSols, boolean verifying, Kakuro[] solucion)
    {
        boolean found = false;
        if(verifying && numSols[0] == 2) return false;

        WhiteTile wt = (WhiteTile)k.getTile(i,j);

        while(!found && wt.getPointer() + 1 < wt.getSizePossibilities()) {
            wt.incValue();
            k.setWhiteTile(i,j,wt);
            int startValue = wt.getValue();
            if (k.columnCheck(i, j, startValue, 0) == 0 && k.rowCheck(i, j, startValue, 0) == 0) { //0 solving, 1 playing, 2 creating
                int ii = i, jj = j;
                while (ii != k.getKakuroRows() && (k.isBlackKakuroTile(ii,jj) || k.getValueTile(ii,jj) != 0)){ //siguiente blanca vacía
                    if (jj == k.getKakuroCols()-1) { ++ii; jj = 0;}
                    else ++jj;
                }
                if (ii == k.getKakuroRows()){
                    if(verifying){
                        if(++numSols[0] == 2){
                            wt.resetPointer();
                            k.setWhiteTile(i,j,wt);
                            return false;
                        }
                        solucion[0] = new Kakuro(k);
                    }
                    else return true;
                }
                else found = solver_verify_recursivo(ii, jj, numSols, verifying, solucion);
            }
            if (wt.getPointer() + 1 == wt.getSizePossibilities() && !found) {
                if(wt.getSizePossibilities() != 1) {
                    wt.resetPointer();
                    k.setWhiteTile(i, j, wt);
                }
                return false;
            }
        }
        return found;
    }

    /** @brief Función para poner todas las posibilidades en las casillas blancas de un Kakuro
    \pre <em>Las casillas blancas del Kakuro k del p.i tienen su vector de posibilidades vacío</em>
    \post Las casillas del Kakuro k del p.i tienen su vector de posibilidades rellenado con todos los números que teóricamente podrían ir en ellas
          Retorna el numero de casillas blancas del kakuro
     */
    public int putAllWhiteTilesPossibilities()
    {
        int numWhiteTiles = 0;
        for(int i = 0; i < k.getKakuroRows(); ++i){
            for(int j = 0; j < k.getKakuroCols(); ++j){
                if(k.isBlackKakuroTile(i,j)){
                    BlackTile bt = (BlackTile)k.getTile(i,j);
                    int right = bt.getRightVal();
                    int bottom = bt.getBottomVal();
                    if(right != -1){
                        int spacesRight = howManySpacesRight(i,j);
                        ArrayList<Integer> comb = new ArrayList<>();
                        for(int l = 0; l < spacesRight; ++l) comb.add(0);
                        ArrayList<Boolean> result = new ArrayList<>();
                        for(int l = 0; l < 9; ++l) result.add(false);
                        getAllCombinations(comb,right,spacesRight,0,result);
                        putPossibilitiesRight(i,j,result,right);
                    }
                    if(bottom != -1){
                        int spacesBottom = howManySpacesBottom(i,j);
                        ArrayList<Integer> comb = new ArrayList<>();
                        for(int l = 0; l < spacesBottom; ++l) comb.add(0);
                        ArrayList<Boolean> result = new ArrayList<>();
                        for(int l = 0; l < 9; ++l) result.add(false);
                        getAllCombinations(comb,bottom,spacesBottom,0,result);
                        putPossibilitiesBottom(i,j,result,bottom);
                    }
                }
                else if(k.getValueTile(i,j) == 0) ++numWhiteTiles;
            }
        }
        return numWhiteTiles;
    }

    /** @brief Función para obtener todos los posibles sumandos de un término
    \pre <em>spaces > 0, n > 0</em>
    \post result contiene todos los posibles sumandos para sumar n con spaces números en un Kakuro siguiendo las reglas
     */
    public void getAllCombinations(ArrayList<Integer> comb, int n, int spaces, int k, ArrayList<Boolean> result)
    {
        boolean finish = false;
        while (!finish && comb.get(k) < 9){
            comb.set(k,comb.get(k)+1);
            if (possibleCombination(comb,n,k)){
                int j = k+1;
                if(j == spaces){
                    for (Integer num : comb) {
                        result.set(num - 1, true);
                    }
                }
                else getAllCombinations(comb, n, spaces, j, result);
            }
            if (comb.get(k) == 9){
                comb.set(k,0);
                finish = true;
            }
        }
    }

    /** @brief Función que nos indica si una combinación es posible
    \pre <em>n > 0, k >= 0</em>
    \post Retorna cierto sii la suma de todos los elementos de comb es <= n
          y no hay repeticiones en el vector con el elemento de la posición k.
          Además si k és la última posición la suma tiene que ser exactamente n.
     */
    private boolean possibleCombination(ArrayList<Integer> comb, int n, int k)
    {
        int sum = 0;
        for(int i = 0; sum!=-1 && i < comb.size(); ++i){
            if(k != i && comb.get(k).equals(comb.get(i))) sum = -1;
            else sum += comb.get(i);
        }
        if(sum == -1) return false;
        if(k+1 == comb.size()) return sum == n;
        return sum < n;
    }

    /** @brief Función para poner posibilidades en las casillas blancas hacia abajo
    \pre <em>La casilla (i,j) es negra con valor hacia abajo</em>
    \post Las casillas blancas de debajo de (i,j) todas tienen possibilities como su vector de posibles
     */
    private void putPossibilitiesBottom(int i, int j, ArrayList<Boolean> possibilities, int sumBottom)
    {
        boolean stop = false;
        ArrayList<Integer> numberPossibilities = new ArrayList<>();
        for(int k = 0; k < possibilities.size(); ++k){
            if(possibilities.get(k)) numberPossibilities.add(k+1);
        }
        for(int l = i+1; !stop && l < k.getKakuroRows(); ++l){
            if(!k.isBlackKakuroTile(l,j)){
                WhiteTile wt = (WhiteTile)k.getTile(l,j);
                wt.putPossibilities(numberPossibilities);
                wt.setSumCol(sumBottom);
            }
            else stop = true;
        }
    }

    /** @brief Función para poner posibilidades en las casillas blancas hacia la derecha
    \pre <em>La casilla (i,j) es negra con valor hacia la derecha/em>
    \post Las casillas blancas de la derecha de (i,j) todas tienen possibilities como su vector de posibles
     */
    private void putPossibilitiesRight(int i, int j, ArrayList<Boolean> possibilities, int sumRight)
    {
        boolean stop = false;
        ArrayList<Integer> numberPossibilities = new ArrayList<>();
        for(int k = 0; k < possibilities.size(); ++k){
            if(possibilities.get(k)) numberPossibilities.add(k+1);
        }
        for(int l = j+1; !stop && l < k.getKakuroCols(); ++l){
            if(!k.isBlackKakuroTile(i,l)){
                WhiteTile wt = (WhiteTile)k.getTile(i,l);
                wt.putPossibilities(numberPossibilities);
                wt.setSumRow(sumRight);
            }
            else stop = true;
        }
    }

    /** @brief Función ver cuántas casillas blancas hay debajo de una casilla
    \pre <em>La casilla (i,j) existe</em>
    \post Retorna el número de casillas blancas que hay debajo de la casilla (i,j)
     */
    public int howManySpacesBottom(int i, int j)
    {
        int sum = 0;
        boolean stop = false;
        for(int l = i+1; !stop && l < k.getKakuroRows(); ++l){
            if(!k.isBlackKakuroTile(l,j)) ++sum;
            else stop = true;
        }
        return sum;
    }

    /** @brief Función ver cuántas casillas blancas hay a la derecha de una casilla
    \pre <em>La casilla (i,j) existe</em>
    \post Retorna el número de casillas blancas que hay a la derecha de la casilla (i,j)
     */
    public int howManySpacesRight(int i, int j)
    {
        int sum = 0;
        boolean stop = false;
        for(int l = j+1; !stop && l < k.getKakuroCols(); ++l){
            if(!k.isBlackKakuroTile(i,l)) ++sum;
            else stop = true;
        }
        return sum;
    }

    /*                  |==========================================================|                  */
    /*                  |  FUNCIONES HECHAS PUBLICAS PARA TESTEAR EL DRIVER SOLVE  |                  */
    /*                  |==========================================================|                  */

    /** @brief Función hecha pública para testear con el driverSolve*/
    public boolean publicPossibleCombination(ArrayList<Integer> comb, int n, int k){ return possibleCombination(comb, n, k); }
    /** @brief Función hecha pública para testear con el driverSolve*/
    public void publicPutPossibilitiesBottom(int i, int j, ArrayList<Boolean> possibilities, int sumBottom){ putPossibilitiesBottom(i, j, possibilities,sumBottom); }
    /** @brief Función hecha pública para testear con el driverSolve*/
    public void publicPutPossibilitiesRight(int i, int j, ArrayList<Boolean> possibilities, int sumRight){ putPossibilitiesRight(i, j, possibilities,sumRight); }
}
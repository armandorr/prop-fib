/** @file CtrlPlay.java
 @brief Especificación del controlador CtrlPlay
 */

package Dominio.Controladores;

import Dominio.Clases.Kakuro;
import Dominio.Clases.Utiles;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

/** @class CtrlPlay
    @brief Controlador dedicado a la funcionalidad de juego de Kakuros.

    Hecha por Marco Patiño

    Contiene, básicamente, una función que permite jugar un Kakuro determinado
 */
public class CtrlPlay
{

    /** @brief Varibale de la clase utiles para facilitar ciertas tareas. */
    static Utiles utiles = new Utiles();

    /** @brief Representa el tiempo jugado para llegar a la representación actual del tablero. */
    private float time;

    /** @brief Representa el nombre de la partida jugada. */
    private String gameName;

    /** @brief Es el kakuro que se está jugando en la partida. */
    private Kakuro k;

    /** @brief Nombre del usuario. */
    private final String username;

    /** @brief Controlador de dominio. */
    private final CtrlDominio CD;

    //Creadoras
    /** @brief Creadora por defecto
    Se ejecuta al declarar un nuevo objeto de la clase CtrlPlay
    \pre <em>Cierto</em>
    \post El resultado es un nuevo objeto de la clase CtrlPlay
     */
    public CtrlPlay()
    {
        this.gameName = null;
        this.time = 0;
        this.username = "";
        CD = new CtrlDominio();
    }

    /** @brief Creadora con parametro Kakuro
    \pre <em>Cierto</em>
    \post El resultado es un nuevo objeto de la clase CtrlPlay con el parametro Kakuro como el de la clase
     */
    public CtrlPlay(Kakuro k)
    {
        this.k = k;
        this.gameName = null;
        this.time = 0;
        this.username = "";
        CD = new CtrlDominio();
    }

    /** @brief Creadora con parametro Kakuro y nombre
    \pre <em>Cierto</em>
    \post El resultado es un nuevo objeto de la clase CtrlPlay con el parametro Kakuro como el de la clase y con un nombre identificando a la partida
     */
    public CtrlPlay(Kakuro k, String n, String username)
    {
        this.k = k;
        this.gameName = n;
        this.time = 0;
        this.username = username;
        CD = new CtrlDominio();
    }

    /** @brief Función impresora de kakuro para jugar
    \pre <em>Cierto</em>
    \post La función nos imprime el Kakuro kak en un formato adecuado a la funcionalidad de jugar
     */
    public void printKakuroPlay(Kakuro kak)
    {
        String res = kak.kakuroToStringNice();
        System.out.println(utiles.KakuroToCentred(res));
    }

    /** @brief Función de jugar
     *
    La función nos permite rellenar el kakuro, es decir, jugar. Mientras jugamos se va cronometrando el tiempo que tardamos en resolverlo.
    Para poder introducir el número en el kakuro se han de cumplir varias condiciones:
    1-. Las coordenadas del número que queremos introducir están dentro del tablero.
    2-. Las coordenadas corresponden a una whitetile.
    3-. El número que introducimos está entre 0 y 9.
    4-. El número que introducimos no está repetido en la misma fila o columna.
    \pre <em>El Kakuro del p.i no es vacío</em>
    \post Una vez resuelto el kakuro o cuando el usuario quiera salir(Q) se parará el cronómetro y acabará la función.
     */
    public void play () throws IOException
    {
        Kakuro kSolved = new Kakuro(k);
        CtrlSolve s = new CtrlSolve(kSolved);
        utiles.clearScreen();
        utiles.printNice("Kakuro a resolver");
        printKakuroPlay(k);
        utiles.printNice("Preparando Partida...");
        s.solve_recursivo();
        Kakuro ksolved = s.getKakuroSolved();

        int i = 0; int j = 0;
        if (j == k.getKakuroCols()-1) { ++i; j = 0; }
        else ++j;
        while(i != k.getKakuroRows() && (k.isBlackKakuroTile(i,j) || k.getValueTile(i,j)!=0)) {
            if (j == k.getKakuroCols()-1) { ++i; j = 0;}
            else ++j;
        }
        boolean finalizar = (i == k.getKakuroRows()); //no white tiles empty, kakuro solved
        boolean itsSolved = finalizar;
        utiles.clearScreen();
        long timePlaying = new Date().getTime();
        if(gameName!=null) {
            //leer tiempo de persistencia //done
            int tiempoPartida = CD.leerTiempoPartidaActual("data/Users/" + username+"/partidasUser/"+gameName);
            timePlaying -= tiempoPartida;
        }
        while (!itsSolved && !finalizar){
            try {
                utiles.printNice("Kakuro a resolver");
                printKakuroPlay(k);
                utiles.Centered("", " ");
                utiles.printLineEqual();
                System.out.println(utiles.wellTabed("|  Indica que deseas hacer en el formato indicado:", "|"));
                System.out.println(utiles.wellTabed("|          [numFila numColumna Valor]: Kakuro[numFila][numColumna] = Valor", "|"));
                System.out.println(utiles.wellTabed("|          [R]                       : Pista (+10 s) ", "|"));
                System.out.println(utiles.wellTabed("|          [Q]                       : Salir", "|"));
                utiles.printLineEqual();
                String linea = utiles.readLine();
                int x = utiles.treatInput(linea);
                utiles.clearScreen();
                if (x == 0) {
                    int[] nums = utiles.convertStringtoIntArrayNums(linea);
                    if (nums.length == 3) {
                        if (nums[2] > 9 || nums[2] < 0)
                            utiles.printNiceWithSpaces("El valor debe estar entre 0 y 9, por favor intenta de nuevo");
                        else if (k.tryToWrite(nums[0], nums[1], nums[2],1) != 0)
                            utiles.printNiceWithSpaces("Valor no valido, por favor intenta de nuevo");
                        else{
                            i = 0; j = 0;
                            if (j == k.getKakuroCols()-1) { ++i; j = 0; }
                            else ++j;
                            while(i != k.getKakuroRows() && (k.isBlackKakuroTile(i,j) || k.getValueTile(i,j)!=0)) {
                                if (j == k.getKakuroCols()-1) { ++i; j = 0;}
                                else ++j;
                            }
                            itsSolved = (i == k.getKakuroRows()); //no white tiles, kakuro solved
                        }
                    }
                } else if (x == -2) { // pista  PASAMOS DE ENSEÑAR EL KAKURO RESUELTO A DAR UNA PISTA Y SUMAR TIEMPO SI LOGUED USER
                    utiles.printNice("¿Qué valor quieres ver?");
                    System.out.println(utiles.wellTabed("|          [numFila numColumna: Kakuro[numFila][numColumna] ", "|"));
                    utiles.printLineEqual();
                    String inputPista = utiles.readLine();
                    int[] nums = utiles.convertStringtoIntArrayNums(inputPista);
                    if (nums[0] >= 0 && nums[0]<k.getKakuroRows() && nums[1]>=0 && nums[1]<k.getKakuroCols()){
                        if (!k.getTile(nums[0],nums[1]).isBlack()){
                            int value = ksolved.getValueTile(nums[0], nums[1]);
                            k.setWhiteTile(nums[0], nums[1], value);
                            timePlaying -= 10000;
                        }
                        else {
                            utiles.printNiceWithSpaces("La casilla ha de ser blanca, intenta de nuevo");
                        }
                    }
                    else {
                        utiles.printNiceWithSpaces("Valor no valido, por favor intenta de nuevo");
                    }

                } else if (x == -1) {
                    finalizar = true;
                    if(username != null) { // si NO somos invitado
                        if(k.getId()<=0) k.setID(CD.nextKakuroID(true));
                        String sav = k.kakuroToStringStandard(true); // aqui tengo el kakuro en string
                        // aqui metemos las estadisiticas extra que queramos/hagan falta:

                        sav += new Date().getTime() - timePlaying;
                        utiles.printNice("Quieres guardar la partida? [0]no/[1]si");
                        int guardarRes = utiles.readInt();
                        if (guardarRes == 1){
                            if (this.gameName == null){
                                Scanner e = new Scanner(System.in);
                                utiles.printNice("Con que nombre quieres guardar la partida? ");
                                this.gameName = e.nextLine();
                                CD.guardarPartidaAuxiliar(sav,"data/Users/" + username+"/partidasUser/"+gameName+".txt");
                            }
                            else {
                                CD.guardarPartidaAuxiliar(sav, "data/Users/" + username + "/partidasUser/" + gameName);
                            }

                            utiles.printNice("Partida guardada con éxito. ");
                        }
                    }
                }
            }
            catch (NumberFormatException e) {
                utiles.clearScreen();
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println(utiles.wellTabed("|     ERROR:", "|"));
                System.out.println(utiles.wellTabed("|        Has introducido un carácter no valido", "|"));
                System.out.println(utiles.wellTabed("|        "+e.getMessage(),"|"));
                System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
                System.out.println();
            }
        }

        this.time += new Date().getTime() - timePlaying;
        utiles.clearScreen();

        if (!finalizar) {
            utiles.printNiceWithSpaces("¡Felicidades! KAKURO RESUELTO :)  Has tardado " + this.time/1000 + " seconds.");
            //Aqui hemos de guardar el kakuro en User/PartidasAcabadas (si hace falta) y actualizamos el estado (si hace falta)
            //primero miramos si hemos de guardar el kakuro
            CD.guardarPartidaAcabada(time/1000);
        }
    }
}
/** @file CtrlVerify.java
 @brief Especificación del controlador CtrlVerify
 */

package Dominio.Controladores;

import Dominio.Clases.Kakuro;

/** @class CtrlVerify
    @brief Controlador dedicado a la verificación de Kakuros.

Hecha por David Marín

Contiene, básicamente, una función que verifica el número de soluciones de un Kakuro
 */
public class CtrlVerify
{
    /** @brief Copia del Kakuro que será verificado.*/
    Kakuro k;

    /** @brief Creadora por defecto
    \pre <em>Cierto</em>
    \post Se ha creado un objeto de la clase CtrlVerify
     */
    public CtrlVerify()
    {

    }

    /** @brief Creadora con parametro Kakuro
    \pre <em>Cierto</em>
    \post Se ha creado un objeto de la clase CtrlVerify con un Kakuro asignado por parámetro
     */
    public CtrlVerify(Kakuro in)
    {
        k = in;
    }

    /** @brief Función principal de la clase que verifica un Kakuro pasado como atributo
    \pre <em>Cierto</em>
    \post Devuelve el número de soluciones (max 2) que tiene el Kakuro in. 0->No tiene solución, 1->Solución única, 2->Más de una solución
          Además si hay alguna solución podremos obtenerla con el parametro solucion
     */
    public int verify(Kakuro[] solucion)
    {
        CtrlSolve sol = new CtrlSolve(k);
        sol.putAllWhiteTilesPossibilities();
        int i = 0, j = 0;
        while(i != k.getKakuroRows() && (k.isBlackKakuroTile(i,j) || k.getValueTile(i,j) != 0)) { //found next white Tile
            if(j == k.getKakuroCols()-1) { ++i; j = 0; }
            else ++j;
        }
        if(i == k.getKakuroRows()) return 1; //No white tiles
        int[] numSols = new int[]{0}; //pointer of num sols
        sol.solver_verify_recursivo(i,j,numSols,true, solucion);
        if(numSols[0] == 0){
            for(int n = 0; n < k.getKakuroRows(); ++n){
                for(int m = 0; m < k.getKakuroCols(); ++m){
                    if(!k.isBlackKakuroTile(n,m)) k.setWhiteTile(n,m);
                }
            }
        }
        return numSols[0];
    }
}
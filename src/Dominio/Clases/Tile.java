/** @file Tile.java
 @brief Especificación de la classe Tile
 */

package Dominio.Clases;

/** @class Tile
 @brief Representa una casilla de un Kakuro

 Hecha por Armando Rodríguez Ramos

 Clase abstracta que tiene dos hijos, BlackTile y WhiteTile
 */
public abstract class Tile
{
    //Creadora
    /** @brief Creadora por defector de la clase
    \pre <em>Cierto</em>
    \post Se ha creado una Tile vacía
     */
    public Tile()
    {

    }

    //Consultora
    /** @brief Consultora abstracta de si una Tile es una BlackTile
    \pre <em>Cierto</em>
    \post Al ser de la clase BlackTile, nos devuelve true
          Al ser de la clase WhiteTile, nos devuelve false
     */
    public abstract boolean isBlack();

    /** @brief Operación para consultar el valor de un casilla
    \pre <em>Cierto</em>
    \post Nos devuelve el valor de la casilla blanca (ya que la implementa) o devuelve -1 si es negra
     */
    public int getValue()
    {
        return -1;
    }

    //Escritura
    /** @brief Operación abstracta para escribir una casilla
    \pre <em>Cierto</em>
    \post Imprime la representación de una casilla (la implementan los dos hijos)
     */
    public abstract String printTile();

    /** @brief Operación para consultar la posición de la fila de la casilla
    \pre <em>Cierto</em>
    \post Nos devuelve la posición de la fila de la casilla blanca (ya que la implementa) o devuelve -1 si es negra
     */
    public int getPosRow()
    {
        return -1;
    }

    /** @brief Operación para consultar la posición de la columna de la casilla
    \pre <em>Cierto</em>
    \post Nos devuelve la posición de la columna de la casilla blanca (ya que la implementa) o devuelve -1 si es negra
     */
    public int getPosCol()
    {
        return -1;
    }

    /** @brief Operación para consultar el valor de la suma de la fila de la casilla
    \pre <em>Cierto</em>
    \post Nos devuelve el valor de la suma de la fila de la casilla blanca (ya que la implementa) o devuelve -1 si es negra
     */
    public int getSumRow()
    {
        return -1;
    }

    /** @brief Operación para consultar el valor de la suma de la columna de la casilla
    \pre <em>Cierto</em>
    \post Nos devuelve el valor de la suma de la columna de la casilla blanca (ya que la implementa) o devuelve -1 si es negra
     */
    public int getSumCol()
    {
        return -1;
    }
}
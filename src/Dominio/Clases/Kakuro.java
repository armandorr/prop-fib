/**
 * @file Kakuro.java
 * @brief Especificación de la clase Kakuro (tablero)
 */

package Dominio.Clases;

import Dominio.Controladores.CtrlSolve;

import java.util.ArrayList;

/** @class Kakuro
 @brief Representa un tablero de Kakuro

 Hecha por David Marín

 Contiene diversas funciones creadoras, consultoras, modificadoras y funciones de escritura y lectura
 */
public class Kakuro
{
    /** @brief Representación del tablero, como una matriz de Tiles.*/
    private ArrayList<ArrayList<Tile>> board;
    /** @brief Entero que representa la dificultad del kakuro.*/
    private int dificultad;
    /** @brief Entero que representa la identificacion del kakuro.*/
    private int id;

    //Creadoras
    /** @brief Creadora por defecto.
    Se ejecuta al declarar un nuevo kakuro.
    \pre <em>Cierto</em>
    \post El resultado es un nuevo Kakuro con un nuevo board vacío.
     */
    public Kakuro()
    {
        board = new ArrayList<>();
        dificultad = -1;
        id = -1;
    }

    /** @brief Creadora con board explicito (Usada para harcodear un kakura a la hora de hacer los drivers) .
    \pre <em>Cierto</em>
    \post El resultado es un nuevo Kakuro con un board igual al que se le pasa por parametro.
     */
    public Kakuro(ArrayList<ArrayList<Tile>> in)
    {
        board = in;
        dificultad = -1;
        id = -1;
    }

    /** @brief Creadora Kakuor tamaño ixj.
    Se ejecuta al declarar un nuevo kakuro pasando como parametro dos enteros i, j.
    \pre <em>i > 0, j > 0</em>
    \post El resultado es un nuevo Kakuro con un nuevo board de tamaño ixj lleno de WhiteTiles vacías.
     */
    public Kakuro(int i, int j)
    {
        board = new ArrayList<>();
        for (int k = 0; k < i; ++k) {
            ArrayList<Tile> fila = new ArrayList<>();
            for (int t = 0; t < j; ++t) {
                fila.add(new WhiteTile());
            }
            board.add(fila);
        }
        dificultad = -1;
    }

    /** @brief Creadora Kakuor tamaño ixj, dificultad y ID.
    Se ejecuta al declarar un nuevo kakuro pasando como parametro 4 enteros i, j, dificultad, id.
    \pre <em>i > 0, j > 0</em>
    \post El resultado es un nuevo Kakuro con un nuevo board de tamaño ixj lleno de WhiteTiles vacías, con dificultad = dificultad y ID = id.
     */
    public Kakuro(int i, int j, int dificultad, int id)
    {
        board = new ArrayList<>();
        for (int k = 0; k < i; ++k) {
            ArrayList<Tile> fila = new ArrayList<>();
            for (int t = 0; t < j; ++t) {
                fila.add(new WhiteTile());
            }
            board.add(fila);
        }
        this.dificultad = dificultad;
        this.id = id;
    }

    /** @brief Creadora por copia.
    \pre <em>Cierto</em>
    \post El resultado es un nuevo Kakuro exactamente igual al Kakuro que se pasa como parametro.
     */
    public Kakuro(Kakuro k2)
    {
        ArrayList<ArrayList<Tile>> board2 = new ArrayList<>();
        for (int i = 0; i < k2.board.size(); ++i) {
            ArrayList<Tile> row = new ArrayList<>();
            for (int j = 0; j < k2.board.get(i).size(); ++j) {
                if (k2.board.get(i).get(j).isBlack()) row.add(new BlackTile((BlackTile) k2.board.get(i).get(j)));
                else row.add(new WhiteTile((WhiteTile) k2.board.get(i).get(j)));
            }
            board2.add(row);
        }
        this.board = board2;
        this.id = k2.getId();
        this.dificultad = k2.getDificultad();
    }

    //Consultoras
    /** @brief Consultora de la dificultad.
    \pre <em>Cierto</em>
    \post Devuelve un entero representando el valor de la dificultad del kakuro.
     */
    public int getDificultad()
    {
        return dificultad;
    }

    /** @brief Consultora del ID.
    \pre <em>Cierto</em>
    \post Devuelve un entero representando el valor de la ID del kakuro.
     */
    public int getId()
    {
        return id;
    }

    /** @brief Consultora de si un kakuro es igual a otro con respecto a los valores de las whiteTiles.
    \pre <em>El kakuro que se pasa por parámetro ha de tener el mismo tamaño</em>
    \post Devuelve si los valores de las casillas blancas de ambos kakuros es exactamente el mismo.
     */
    public boolean isEqualWhiteTiles(Kakuro k2)
    {
        boolean equal = true;
        int i = 0;
        while (equal && i < board.size()) {
            int j = 0;
            while (equal && j < board.get(i).size()) {
                if (!board.get(i).get(j).isBlack()) {
                    if (board.get(i).get(j).getValue() != k2.board.get(i).get(j).getValue()) equal = false;
                }
                ++j;
            }
            ++i;
        }
        return equal;
    }

    /** @brief Consultora de la casilla i,j.
    \pre <em>i y j dentro del tamaño del tablero implicito</em>
    \post Devuelve el la casilla de la posición i,jnúmero de filas del tablero.
     */
    public Tile getTile(int i, int j)
    {
        return board.get(i).get(j);
    }

    /** @brief Consultora del numero de filas del tablero.
    \pre <em>Cierto</em>
    \post Devuelve el número de filas del tablero.
     */
    public int getKakuroRows()
    {
        return board.size();
    }

    /** @brief Consultora del numero de columnas del tablero .
    \pre <em>Cierto</em>
    \post Devuelve el número de columnas del tablero.
     */
    public int getKakuroCols()
    {
        return board.get(0).size();
    }

    /** @brief Consultora del valor de la casilla blanca i,j.
    \pre <em>La posición i,j es una casilla blanca</em>
    \post Devuelve el valor de la casilla blanca de la posicion i,j.
     */
    public int getValueTile(int i, int j)
    {
        return board.get(i).get(j).getValue();
    }

    /** @brief Consultora del valor right de la casilla negra i,j.
    \pre <em>La posición i,j es una casilla negra</em>
    \post Devuelve el valor right de la casilla negra de la posicion i,j.
     */
    public int getRightValueBlackTile(int i, int j)
    {
        BlackTile bt = (BlackTile) board.get(i).get(j);
        return bt.getRightVal();
    }

    /** @brief Consultora del valor bottom de la casilla negra i,j.
    \pre <em>La posición i,j es una casilla negra</em>
    \post Devuelve el valor bottom de la casilla negra de la posicion i,j.
     */
    public int getBottomValueBlackTile(int i, int j)
    {
        BlackTile bt = (BlackTile) board.get(i).get(j);
        return bt.getBottomVal();
    }

    /** @brief Consultora del valor right que afecta a la casilla blanca i,j.
    \pre <em>La posición i,j es una casilla blanca</em>
    \post Devuelve el valor right que afecta a la casilla blanca i,j.
     */
    public int getSumRow(int i, int j)
    {
        return board.get(i).get(j).getSumRow();
    }

    /** @brief Consultora del valor bottom que afecta a la casilla blanca i,j.
    \pre <em>La posición i,j es una casilla blanca</em>
    \post Devuelve el valor bottom que afecta a la casilla blanca i,j.
     */
    public int getSumCol(int i, int j)
    {
        return board.get(i).get(j).getSumCol();
    }

    /** @brief Modificadora del atributo sumRow de la casilla blanca i,j.
    \pre <em>La posición i,j es una casilla blanca</em>
    \post El atributo sumRow de la casilla blanca i,j pasa a valer sumRow.
     */
    public void setSumRow(int i, int j, int sumRow)
    {
        WhiteTile wt = (WhiteTile) board.get(i).get(j);
        wt.setSumRow(sumRow);
    }

    /** @brief Modificadora del atributo sumCol de la casilla blanca i,j.
    \pre <em>La posición i,j es una casilla blanca</em>
    \post El atributo sumCol de la casilla blanca i,j pasa a valer sumCol.
     */
    public void setSumCol(int i, int j, int sumCol)
    {
        WhiteTile wt = (WhiteTile) board.get(i).get(j);
        wt.setSumRow(sumCol);
    }

    /** @brief Consultora de si una coordenada contiene una casilla negra.
    \pre <em>Las coordenadas están dentro del kakuro</em>
    \post Devuelve si en las coordenadas indicadas hay una casilla negra.
     */
    public boolean isBlackKakuroTile(int i, int j)
    {
        return board.get(i).get(j).isBlack();
    }

    /** @brief DFS.
    \pre <em>Cierto</em>
    \post Se modifica el parametro vis que obtiene recursivamente las casillas visitadas empezando en un cierto i,j.
     */
    private void dfs(int i, int j, boolean[][] vis)
    {
        vis[i][j] = true;
        int ii = 0, jj = 0, iii = board.size() - 1, jjj = board.get(ii).size() - 1;
        if (i > 0) ii = i - 1;
        if (j > 0) jj = j - 1;
        if (i < board.size() - 1) iii = i + 1;
        if (j < board.get(ii).size() - 1) jjj = j + 1;

        if ( !board.get(ii).get(j).isBlack() && !vis[ii][j] ) dfs(ii , j  , vis);
        if (!board.get(iii).get(j).isBlack() && !vis[iii][j]) dfs(iii, j  , vis);
        if ( !board.get(i).get(jj).isBlack() && !vis[i][jj] ) dfs(i  , jj , vis);
        if (!board.get(i).get(jjj).isBlack() && !vis[i][jjj]) dfs(i  , jjj, vis);
    }

    /** @brief Consultora de la conectividad de un Kakuro
    \pre <em>Cierto</em>
    \post Retorna <em>true</em> si el kakuro tiene una sola "componente conexa".
     */
    public boolean connectedKakuro()
    {
        boolean[][] visitado = new boolean[board.size()][board.get(0).size()];
        int num_cc = 0;
        for (int i = 0; i < board.size(); ++i) {
            for (int j = 0; j < board.get(i).size(); ++j) {
                if (!board.get(i).get(j).isBlack() && !visitado[i][j]) {
                    dfs(i, j, visitado);
                    if (++num_cc > 1) return false;
                }
            }
        }
        return true;
    }

    //Modificadoras
    /** @brief Setter del id.
    \pre <em>Cierto</em>
    \post El id del kakuro es igual al parámetro que se ha passado
     */
    public void setID (int id)
    {
        this.id = id;
    }

    /** @brief Pone una WhiteTile pasada por parametro en las coordenadas i,j.
    \pre <em>Las coordenadas están dentro del kakuro</em>
    \post Se ha metido la WhiteTile del parametro en la posicion i,j.
     */
    public void setWhiteTile(int i, int j, WhiteTile wt)
    {
        board.get(i).set(j, wt);
    }

    /** @brief Pone una WhiteTile en las coordenadas i,j.
    \pre <em>Las coordenadas están dentro del kakuro</em>
    \post Se ha metido una WhiteTile vacia en la posicion i,j.
     */
    public void setWhiteTile(int i, int j)
    {
        board.get(i).set(j, new WhiteTile());
    }

    /** @brief Pone una WhiteTile en las coordenadas i,j con valor value.
    \pre <em>Las coordenadas están dentro del kakuro</em>
    \post Se ha metido una WhiteTile en la posicion i,j con valor value.
     */
    public void setWhiteTile(int i, int j, int value)
    {
        board.get(i).set(j, new WhiteTile(value));
    }

    /** @brief Pone una BlackTile en las coordenadas i,j.
    \pre <em>Las coordenadas están dentro del kakuro</em>
    \post Se ha metido una BlacTile en la posicion i,j.
     */
    public void setBlackTile(int i, int j)
    {
        board.get(i).set(j, new BlackTile());
    }

    /** @brief Pone el valor b en el bottomValue de la BlackTile de las coordenadas i,j.
    \pre <em>Las coordenadas están dentro del kakuro y b entero</em>
    \post Se ha metido el valor b en el bottomValue de la BlackTile de la posicion i,j.
     */
    public void setBottomBlackTile(int i, int j, int b)
    {
        BlackTile bt = (BlackTile) board.get(i).get(j);
        bt.setBottomVal(b);
        board.get(i).set(j, bt);
    }

    /** @brief Pone el valor r en el rightValue de la BlackTile de las coordenadas i,j.
    \pre <em>Las coordenadas están dentro del kakuro y r entero</em>
    \post Se ha metido el valor r en el rigthValue de la BlackTile de la posicion i,j.
     */
    public void setRightBlackTile(int i, int j, int r)
    {
        BlackTile bt = (BlackTile) board.get(i).get(j);
        bt.setRightVal(r);
        board.get(i).set(j, bt);
    }

    /** @brief Vacía el tablero del Kakuro implicito y lo modifica para que tenga tamaño nxm.
    \pre <em>Cierto</em>
    \post Ahora el tablero es de tamaño nxm y esta vació (repleto de casillas blancas vacias) con las negras iniciales correctas (izquierda i arriba)
     */
    public void cleanKakuro(int n, int m)
    {
        board = new ArrayList<>();
        for (int i = 0; i < n; ++i) {
            ArrayList<Tile> fila = new ArrayList<>();
            for (int j = 0; j < m; ++j) {
                if(i == 0) fila.add(new BlackTile());
                else{
                    if(j == 0) fila.add(new BlackTile());
                    else fila.add(new WhiteTile());
                }
            }
            board.add(fila);
        }
    }

    /** @brief Consultora de si el kakuro está completo
    \pre <em>Cierto</em>
    \post Retorna <em>true</em> si el kakuro está completo, es decir, no tiene WhiteTiles vacías. .
     */
    public boolean isKakuroCompleted()
    {
        boolean found = false;
        for(int i = 0; !found && i < board.size(); ++i){
            for(int j = 0; !found && j < board.get(i).size(); ++j){
                if(!board.get(i).get(j).isBlack()){
                    if(board.get(i).get(j).getValue() == 0) found = true; //white tile found
                }
            }
        }
        return !found;
    }

    /** @brief Consultora de si se puede escribir un valor en una coordenada. Si se puede, se escribe el valor.
    \pre <em>Cierto</em>
    \post mode 2 -> creando, solo mira si repetida
          mode 1 -> jugando, checkea todas estas condifciones:
          Devuelve 0 si se puede escribir el valor porque es correcto i no se repite
          Devuelve 1 si el valor value no puede ir en una casilla blanca (value > 9 or value < 0)
          Devuelve 2 si el valor value esta fuera del tamaño del Kakuro
          Devuelve 3 si la casilla es negra
          Devuelve 4 si se repite el valor en la fila
          Devuelve 5 si se la suma es completa en la fila y no es correcta
          Devuelve 6 si se repite el valor en la columna
          Devuelve 7 si se la suma es completa en la columna y no es correcta
     */
    public int tryToWrite(int i, int j, int value, int mode)
    {
        if(mode == 2) {
            if (rowCheck(i, j, value, 2) == 0 && columnCheck(i, j, value, 2) == 0) return 0;
            else return 1;
        }
        if(value < 0 || value > 9) return 1;
        if(i >= board.size() || i < 0 || j >= board.get(i).size() || j < 0) return 2;
        if(board.get(i).get(j).isBlack()) return 3;
        if(value != 0){
            int row = rowCheck(i, j, value, 1);
            if(row == 1) return 4;
            if(row == 2) return 5;
            int col = columnCheck(i, j, value, 1);
            if(col == 1) return 6;
            if(col == 2) return 7;
        }
        WhiteTile wt = (WhiteTile)board.get(i).get(j);
        wt.setValue(value);
        board.get(i).set(j, wt);
        return 0;
    }

    /** @brief Consultora de si en la columna j de una posicion i,j se puede escribir el valor startValue.
    \pre <em>La posicion i,j es una casilla blanca</em>
    \post Modo 0 (resolviendo o verificando):
            Devuelve 0(true) si el valor pasado por parametro startValue no se repite en la misma columna
                y si la suma se puede completar con los espacios que faltan.
            Devuelve 1(false) si no es así.
          Modo 1 (jugando):
            Devuelve 0 si no se repite en la columna.
            Devuelve 1 si se repite en la columna
            Devuelve 2 si al completar la columna la suma NO es correcta.
          Modo 2 (creando):
            Devuelve 0 si no se repite en la columna.
            Devuelve 1 si se repite en la columna
     */
    public int columnCheck(int i, int j, int startValue, int mode)
    {
        int totalSum = 0, howManyEmpty = 0;
        int ii = i - 1;
        ArrayList<Integer> vectValues = new ArrayList<>();
        while (ii > 0 && !isBlackKakuroTile(ii,j)) {
            int valueWhiteTile = getValueTile(ii,j);

            if (valueWhiteTile == 0) ++howManyEmpty;
            else if (valueWhiteTile == startValue) return 1;
            else vectValues.add(valueWhiteTile);

            totalSum += valueWhiteTile;
            --ii;
        }
        Tile tile = board.get(ii).get(j);
        BlackTile bt = (BlackTile) tile;
        int sumCol = bt.getBottomVal();

        if (mode == 0 && totalSum > sumCol) return 1;

        ++i;
        while (i < getKakuroRows() && !isBlackKakuroTile(i,j)) {
            int valueWhiteTile = getValueTile(i,j);

            if (valueWhiteTile == 0) ++howManyEmpty;
            else if (valueWhiteTile == startValue) return 1;
            else vectValues.add(valueWhiteTile);

            totalSum += valueWhiteTile;
            if (mode == 0 && totalSum+startValue > sumCol) return 1;
            ++i;
        }

        if (mode == 2) return 0;
        if (howManyEmpty == 0){
            if(totalSum + startValue == sumCol) return 0;
            if(mode == 1) return 2;
            else return 1;
        }
        if (mode == 1) return 0;

        //solving now
        totalSum += startValue;

        int sumaMinima = 0, numsOnMin = 0;
        for(int k = 1; k <= 9 && howManyEmpty > numsOnMin; ++k){
            if(!vectValues.contains(k)){
                ++numsOnMin;
                sumaMinima += k;
            }
        }
        if(numsOnMin != howManyEmpty || totalSum + sumaMinima > sumCol) return 1;

        int sumaMaxima = 0, numsOnMax = 0;
        for(int k = 9; k > 0 && howManyEmpty > numsOnMax; --k){
            if(!vectValues.contains(k)){
                ++numsOnMax;
                sumaMaxima += k;
            }
        }
        if(numsOnMax != howManyEmpty || totalSum + sumaMaxima < sumCol) return 1;
        return 0;
    }

    /** @brief Consultora de si en la fila i de una posicion i,j se puede escribir el valor startValue.
    \pre <em>La posicion i,j es una casilla blanca</em>
    \post Modo 0 (resolviendo o verificando):
            Devuelve 0(true) si el valor pasado por parametro startValue no se repite en la misma fila
                y si la suma se puede completar con los espacios que faltan.
            Devuelve 1(false) si no es así.
        Modo 1 (jugando):
            Devuelve 0 si no se repite en la fila.
            Devuelve 1 si se repite en la fila
            Devuelve 2 si al completar la fila la suma NO es correcta.
        Modo 2 (creando):
            Devuelve 0 si no se repite en la fila.
            Devuelve 1 si se repite en la fila
     */
    public int rowCheck(int i, int j, int startValue, int mode)
    {
        int totalSum = 0, howManyEmpty = 0;
        int jj = j - 1;
        ArrayList<Integer> vectValues = new ArrayList<>();
        while (jj > 0 && !isBlackKakuroTile(i,jj)) {
            int valueWhiteTile = getValueTile(i,jj);

            if (valueWhiteTile == 0) ++howManyEmpty;
            else if (valueWhiteTile == startValue) return 1;
            else vectValues.add(valueWhiteTile);

            totalSum += valueWhiteTile;
            --jj;
        }
        Tile tile = board.get(i).get(jj);
        BlackTile bt = (BlackTile) tile;
        int sumRow = bt.getRightVal();

        if (mode == 0 && totalSum > sumRow) return 1;

        ++j;
        while (j < getKakuroCols() && !isBlackKakuroTile(i,j)) {
            int valueWhiteTile = getValueTile(i,j);

            if (valueWhiteTile == 0) ++howManyEmpty;
            else if (valueWhiteTile == startValue) return 1;
            else vectValues.add(valueWhiteTile);

            totalSum += valueWhiteTile;
            if (mode == 0 && totalSum+startValue > sumRow) return 1;
            ++j;
        }
        if (mode == 2) return 0;
        if (howManyEmpty == 0){
            if(totalSum + startValue == sumRow) return 0;
            else if(mode == 1) return 2;
            else return 1;
        }
        if (mode == 1) return 0;

        totalSum += startValue;

        int sumaMinima = 0, numsOnMin = 0;
        for(int k = 1; k <= 9 && howManyEmpty > numsOnMin; ++k){
            if(!vectValues.contains(k)){
                ++numsOnMin;
                sumaMinima += k;
            }
        }
        if(numsOnMin != howManyEmpty || totalSum + sumaMinima > sumRow) return 1;

        int sumaMaxima = 0, numsOnMax = 0;
        for(int k = 9; k > 0 && howManyEmpty > numsOnMax; --k){
            if(!vectValues.contains(k)){
                ++numsOnMax;
                sumaMaxima += k;
            }
        }
        if(numsOnMax != howManyEmpty || totalSum + sumaMaxima < sumRow) return 1;
        return 0;
    }

    /** @brief Operación para convertir una codificación de casilla (String) en Tile
    \pre <em>El parametro de entrada es una codificación correcta de una casilla, n,m posicion valida</em>
    \post Devuelve la Tile equivalente al input del parametro tileString, la tile tendra posicion n,m
     */
    private Tile convertToTile(String tileString, int n, int m)
    {
        char firstChar = tileString.charAt(0);
        if (firstChar == '?') return new WhiteTile(n, m);
        else if (firstChar == '*') return new BlackTile();
        else if (firstChar == 'C') {
            String restOfTile;
            int i = 1, tam = tileString.length();
            while (i < tam && tileString.charAt(i) != 'F') ++i;
            restOfTile = tileString.substring(1, i);
            int bottomVal = Integer.parseInt(restOfTile);
            int rightVal = -1;
            ++i;
            if (i < tam) {
                restOfTile = tileString.substring(i, tam);
                rightVal = Integer.parseInt(restOfTile);
            }
            return new BlackTile(rightVal, bottomVal);
        } else if (firstChar == 'F') {
            tileString = tileString.substring(1);
            int num = Integer.parseInt(tileString);
            return new BlackTile(num, -1);
        } else {
            int num = Integer.parseInt(tileString);
            return new WhiteTile(num, n, m);
        }
    }

    /** @brief Función para convertir de representación oficial a clase Kakuro
    \pre <em>La string s contiene la representación de un Kakuro en formato estándar</em>
    \post El p.i pasa a ser el Kakuro representado por la string s´. Además también puede leer la id y la dificultad del kakuro en caso de que estuviera representada.
     Devuelve <em>true</em> si el kakuro leido tenia id.
     */
    public boolean readKakuroFromString(String kakuroString)
    {
        String[] kakuroLines = kakuroString.split("\n");
        String line = kakuroLines[0];
        line = line.replace(" ", "");
        String[] size = line.split(",");
        int rows = Integer.parseInt(size[0]);
        int cols = Integer.parseInt(size[1]);
        board.clear();
        for (int i = 0; i < rows; ++i) {
            line = kakuroLines[i + 1];
            line = line.replace(" ", "");
            String[] lineSplitted = line.split(",");
            ArrayList<Tile> filaKakuro = new ArrayList<>();
            for (int j = 0; j < cols; ++j) {
                String tileString = lineSplitted[j];
                Tile tile = convertToTile(tileString, i, j);
                filaKakuro.add(tile);
            }
            board.add(filaKakuro);
        }
        if(kakuroLines.length >= rows+2) {
            String dificultad = kakuroLines[rows + 1];
            this.dificultad = Integer.parseInt(dificultad);
            String id = kakuroLines[rows + 2];
            this.id = Integer.parseInt(id);
            return false;
        }
        else {
            this.dificultad = calculaDificultad();
            return true;
        }
    }

    /** @brief Operación que transforma un Kakuro en un string para ser escrito por pantalla.
    \pre <em>Kakuro no vacio</em>
    \post Devuelve un string que representa al Kakuro implicito en formato para imprimir por pantalla.
     */
    public String kakuroToStringNice()
    {
        StringBuilder kakuroBuilder = new StringBuilder();
        boolean first = true;
        for (int j = 0; j < board.get(0).size(); ++j) {
            if (first) {
                kakuroBuilder.append("      ").append(j);
                first = false;
            } else {
                if (j < 10) kakuroBuilder.append("     ").append(j);
                else kakuroBuilder.append("    ").append(j);
            }
        }
        for (int i = 0; i < board.size(); ++i) {
            first = true;
            kakuroBuilder.append("\n");
            for (int k = 0; k < board.get(i).size(); ++k) {
                if (first) {
                    kakuroBuilder.append("   -------");
                    first = false;
                } else kakuroBuilder.append("------");
            }
            kakuroBuilder.append("\n");
            first = true;
            for (int j = 0; j < board.get(i).size(); ++j) {
                if (first) {
                    kakuroBuilder.append(i);
                    if (i < 10) kakuroBuilder.append("  |");
                    else kakuroBuilder.append(" |");
                    first = false;
                } else kakuroBuilder.append("|");
                kakuroBuilder.append(board.get(i).get(j).printTile());
            }
            kakuroBuilder.append("|");
        }
        first = true;
        kakuroBuilder.append("\n");
        for (int k = 0; k < board.get(0).size(); ++k) {
            if (first) {
                kakuroBuilder.append("   -------");
                first = false;
            } else kakuroBuilder.append("------");
        }
        kakuroBuilder.append("\n");

        return kakuroBuilder.toString();
    }

    /** @brief Operación que transforma un Kakuro en una matriz de Strings.
    \pre <em>Kakuro no vacio</em>
    \post Devuelve una matriz de strings representando al kakuro implícito.
     */
    public String[][] doMatrixFromKakuro()
    {
        String[][] matrixKakuro = new String[board.size()][board.get(0).size()];
        for(int i = 0; i < board.size(); ++i){
            for(int j = 0; j < board.get(i).size(); ++j){
                Tile tile = board.get(i).get(j);
                String stringTile = tile.printTile();
                if (stringTile.equals("     ")) matrixKakuro[i][j] = "";
                else if (stringTile.equals("#####")) matrixKakuro[i][j] = "#";
                else {
                    String[] numsTile = stringTile.split("");
                    if (numsTile[0].equals(" ") && numsTile[1].equals(" ") && !numsTile[2].equals(" ")) { //white tile not empty
                        matrixKakuro[i][j] = numsTile[2];
                    }
                    else matrixKakuro[i][j] = stringTile;
                }
            }
        }
        return matrixKakuro;
    }

    /** @brief Operación que transforma un Kakuro en un string en formato estandar.
    \pre <em>Kakuro no vacio</em>
    \post Devuelve un string que representa al Kakuro implicito en formato estandar.
     */
    public String kakuroToStringStandard(boolean withIdAndDiff)
    {
        StringBuilder kakuroStringBuilder = new StringBuilder(board.size() + "," + board.get(0).size() + "\n");
        for (ArrayList<Tile> tiles : board) {
            for (int j = 0; j < board.get(0).size(); ++j) {
                Tile tile = tiles.get(j);
                if (tile.isBlack()) {
                    BlackTile bt = (BlackTile) tile;
                    if (bt.getBottomVal() == -1 && bt.getRightVal() == -1) kakuroStringBuilder.append("*");
                    else if (bt.getBottomVal() == -1 && bt.getRightVal() != -1)
                        kakuroStringBuilder.append("F").append(bt.getRightVal());
                    else if (bt.getBottomVal() != -1 && bt.getRightVal() == -1)
                        kakuroStringBuilder.append("C").append(bt.getBottomVal());
                    else kakuroStringBuilder.append("C").append(bt.getBottomVal()).append("F").append(bt.getRightVal());
                } else {
                    WhiteTile wt = (WhiteTile) tile;
                    if (wt.getValue() == 0) kakuroStringBuilder.append("?");
                    else kakuroStringBuilder.append(wt.getValue());
                }
                if (j < board.get(0).size() - 1) kakuroStringBuilder.append(",");
            }
            kakuroStringBuilder.append("\n");
        }
        if(withIdAndDiff) {
            kakuroStringBuilder.append(dificultad).append("\n");
            kakuroStringBuilder.append(id).append("\n");
        }
        return kakuroStringBuilder.toString();
    }

    /** @brief Operación que calcula la dificultad de un kakuro.
    \pre <em>Kakuro no vacio</em>
    \post Devuelve un int que representa la dificultad estimada del Kakuro implicito.
     */
    public int calculaDificultad()
    {
        int dificultad;
        double numBlack, numWhite;
        numBlack = numWhite = 0;
        double averagePossibilities = 0.0;
        Kakuro copia = new Kakuro(this);
        CtrlSolve s = new CtrlSolve(copia); // se modifica el Kakuro al llamar a putAllWhiteTilePossibilities

        s.putAllWhiteTilesPossibilities();
        for(int i = 0; i < copia.getKakuroRows(); ++i) {
            for(int j = 0; j < copia.getKakuroCols(); ++j) {
                if(i >= 1 && j >= 1) {
                    if (copia.isBlackKakuroTile(i,j)) ++numBlack;
                    else {
                        ++numWhite;
                        WhiteTile wt = (WhiteTile)copia.getTile(i,j);
                        averagePossibilities += wt.getSizePossibilities();
                    }
                }
            }
        }
        double prop = numWhite/(numWhite+numBlack);
        averagePossibilities /= numWhite;

        int tam = copia.getKakuroRows()*copia.getKakuroCols();
        double valueToClasify = (numWhite*(1-prop)*averagePossibilities)/tam;

        if(valueToClasify <= 0.4) dificultad = 1;
        else if(valueToClasify <= 0.7) dificultad = 2;
        else dificultad = 3;

        return dificultad;
    }

    /*                  |==========================================================|                  */
    /*                  | FUNCIONES HECHAS PUBLICAS PARA TESTEAR EL DRIVERGENERATE |                  */
    /*                  |==========================================================|                  */

    /** @brief Operación hecha publica para probar con JUnit */
    public Tile convertToTileForJUNIT(String t, int i, int j) { return convertToTile(t, i, j); }
    /** @brief Operación hecha publica para probar con JUnit */
    public void dfsPublic(int i, int j, boolean[][] vis) { dfs(i, j, vis); }
}
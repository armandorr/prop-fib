/** @file BlackTile.java
 @brief Especificación de la classe BlackTile
 */

package Dominio.Clases;

/** @class BlackTile
 @brief Representa una casilla negra de un Kakuro

 Hecha por Hasnain Shafqat

 Contiene diversas funciones creadoras, consultoras, modificadoras y una función de escritura
 */
public class BlackTile extends Tile
{
    /** @brief Valor que hay a la derecha en la casilla negra */
    private int rightVal;
    /** @brief Valor que hay abajo en la casilla negra */
    private int bottomVal;

    //Creadoras
    /** @brief Creadora por defecto
    Se ejecuta automáticamente al crear una BlackTile
    \pre <em>Cierto</em>
    \post El resultado es una BlackTile con ambos valores (right y bottom) inicializados a -1
     */
    public BlackTile()
    {
        rightVal = -1;
        bottomVal = -1;
    }

    /** @brief Creadora copiadora
    Dada una BlackTile, crea una copia de esta
    \pre <em>bt es una BlackTile</em>
    \post El resultado es un una BlackTile idéntica a bt
     */
    public BlackTile (BlackTile bt)
    {
        this.rightVal = bt.rightVal;
        this.bottomVal = bt.bottomVal;
    }

    /** @brief Creadora con valores determinados
    Creadora de una BlackTile con los valores determinadoss
    \pre <em>Cierto</em>
    \post El resultado es una BlackTile con rightVal = r y bottomVal = b
     */
    public BlackTile(int r, int b)
    {
        rightVal = r;
        bottomVal = b;
    }

    //Consultoras
    /** @brief Consultora de si una Tile es negra o no (función gancho)
    \pre <em>Cierto</em>
    \post Devuelve cierto
     */
    public boolean isBlack()
    {
        return true;
    }

    /** @brief Consultora del valor rightVal
    \pre <em>El parámetro implícito es una BlackTile con valores inicializados correctamente</em>
    \post La función retorna el valor rightVal del parámetro implícito
     */
    public int getRightVal()
    {
        return rightVal;
    }

    /** @brief Consultora del valor bottomVal
    \pre <em>El parámetro implícito es una BlackTile con valores inicializados correctamente</em>
    \post La función retorna el valor bottomVal del parámetro implícito
     */
    public int getBottomVal()
    {
        return bottomVal;
    }

    //Modificadoras
    /** @brief Operación que nos cambia el valor bottomVal
    \pre <em>el parametro b es un entero</em>
    \post Hemos cambiado el valor de bottomVal, haciendo que sea igual que el parametro b
     */
    public void setBottomVal(int b)
    {
        bottomVal = b;
    }

    /** @brief Operación que nos cambia el valor rightVal
    \pre <em> el parametro r es mayor que 0</em>
    \post Hemos cambiado el valor de rightVal, haciendo que sea igual que el parámetro r
     */
    public void setRightVal(int r)
    {
        rightVal = r;
    }

    //"Escritura"
    /** @brief Función para imprimir la representación de una BlackTile
    \pre <em>Cierto</em>
    \post La función imprime la BlackTile siguiendo estas normas:
        [#] si la casilla es negra por completo
        [r →] si la casilla solo contiene rightVal y rightVal = r
        [b ↓] si la casilla solo contiene bottomVal y bottomVal = b
        [r,b] con los dos valores rightVal = r y bottomVal = b
     */
    public String printTile()
    {
        String res = "";
        if (rightVal == -1 && bottomVal == -1) res += ("#####");
        else if (bottomVal == -1) {
            if (rightVal < 10) res += (rightVal + "  → ");
            else res += (rightVal + " → ");
        }
        else if (rightVal == -1) {
            if (bottomVal < 10) res += (bottomVal + "  ↓ ");
            else res += (bottomVal + " ↓ ");
        }
        else {
            if (bottomVal < 10) res += (bottomVal + "  ");
            else res += (bottomVal + " ");
            if (rightVal < 10) res += (" " + rightVal);
            else res += rightVal;
        }
        return res;
    }
}
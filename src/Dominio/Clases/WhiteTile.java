/** @file WhiteTile.java
 @brief Especificación de la clase WhiteTile
 */

package Dominio.Clases;

import java.util.ArrayList;

/** @class WhiteTile
 @brief Representa una casilla blanca de un Kakuro

  Hecha por Marco Patiño.

 Contiene diversas funciones creadoras, una consultora para obtener el valor de la casilla y diferentes
 funciones para meter las posibilidades que tiene la casilla en su vector possibilities.
 */

public class WhiteTile extends Tile
{
    /** @brief Valor de la casilla */
    private int value;
    /** @brief Posibles numeros que puede contener esta casilla */
    private ArrayList<Integer> possibilities;
    /** @brief Indice para el vector possibilities */
    private int pointer;
    /** @brief Coordenada que indica la fila en la que se encuentra la casilla */
    private final int posRow;
    /** @brief Coordenada que indica la columna en la que se encuentra la casilla */
    private final int posCol;
    /** @brief Indica el valor derecho de la ficha negra más cercana a la izquierda, en la misma fila*/
    private int sumRow;
    /** @brief Indica el valor inferior de la ficha negra más cercana hacia arriba, en la misma columna*/
    private int sumCol;

    //Creadoras
    /** @brief Creadora con valor incial de posicion
    \pre <em>Cierto</em>
    \post El resultado es una casilla blanca con valor 0, el vector possibilities vacío, pointer = -1, con posicion n,m
          y con sumas en fila y columna nulas.
     */
    public WhiteTile(int n, int m)
    {
        value = 0;
        possibilities = new ArrayList<>();
        pointer = -1;
        posRow = n;
        posCol = m;
        sumRow = 0;
        sumCol = 0;
    }

    /** @brief Creadora con valor incial v
    \pre <em>Cierto</em>
    \post El resultado es una casilla blanca con valor v, el vector possibilities vacío, pointer = -1, posicion n,m
          y sumas en fila y columna nulas
     */
    public WhiteTile(int v, int n, int m)
    {
        value = v;
        possibilities = new ArrayList<>();
        pointer = -1;
        posRow = n;
        posCol = m;
        sumRow = 0;
        sumCol = 0;
    }

    /** @brief Creadora por defecto
    \pre <em>Cierto</em>
    \post El resultado es una casilla blanca con value = 0, el vector possibilities vacío y pointer = -1, posicion -1,-1
          y sumas en fila y columnas nula.
     */
    public WhiteTile()
    {
        value = 0;
        possibilities = new ArrayList<>();
        pointer = -1;
        posRow = -1;
        posCol = -1;
        sumRow = 0;
        sumCol = 0;
    }

    /** @brief Creadora con valor incial v
    \pre <em>Cierto</em>
    \post El resultado es una casilla blanca con valor v, el vector possibilities vacío y pointer = -1, posicion -1,-1
          y sumas en fila y columnas nula.
     */
    public WhiteTile(int v)
    {
        value = v;
        possibilities = new ArrayList<>();
        pointer = -1;
        posRow = -1;
        posCol = -1;
        sumRow = 0;
        sumCol = 0;
    }

    /** @brief Creadora copiadora
    \pre <em>wt es una WhiteTile</em>
    \post El resultado es una casilla blanca nueva con el valor de wt,
    un vector possibilities igual al de wt y un pointer igual al de wt
     */
    public WhiteTile (WhiteTile wt)
    {
        this.value = wt.value;
        this.possibilities = new ArrayList<>(wt.possibilities);
        this.pointer = wt.pointer;
        this.posRow = wt.posRow;
        this.posCol = wt.posCol;
        this.sumRow = wt.sumRow;
        this.sumCol = wt.sumCol;
    }

    //Consultoras
    /** @brief Consultora de la posRow.
    \pre <em>Cierto</em>
    \post Devuelve un entero representando el valor de la posRow de la casilla.
     */
    public int getPosRow()
    {
        return posRow;
    }

    /** @brief Consultora de la posCol.
    \pre <em>Cierto</em>
    \post Devuelve un entero representando el valor de la posCol de la casilla.
     */
    public int getPosCol()
    {
        return posCol;
    }

    /** @brief Consultora del valor
    \pre <em>Cierto</em>
    \post Nos devuelve el valor del parámetro implícito
     */
    public int getValue()
    {
        return value;
    }

    /** @brief Consultora de si esta Tile es una BlackTile
    \pre <em>Cierto</em>
    \post Nos devuelve falso siempre porque es una funcion gancho
     */
    public boolean isBlack()
    {
        return false;
    }

    /** @brief Consultora del apuntador
    \pre <em>Cierto</em>
    \post Nos devuelve el puntero del paramtero implicito
     */
    public int getPointer()
    {
        return pointer;
    }

    /** @brief Consultora del tamaño del vector de posibilidades
    \pre <em>Cierto</em>
    \post Nos devuelve el tamaño del vector de posibilidades del parámetro implícito
     */
    public int getSizePossibilities()
    {
        return possibilities.size();
    }

    /** @brief Operación consultora de la posibilidad actual
    \pre <em>Cierto</em>
    \post Nos devuelve el valor que tocaría según el puntero pointer y el vector possibilities
     */
    public int getPossibility()
    {
        return possibilities.get(pointer);
    }

    /** @brief Consultora del vector de posibilidades
    \pre <em>Cierto</em>
    \post Nos devuelve el vector possibilities que contiene todas las posibilidades del parámetro implícito
     */
    public ArrayList<Integer> getPossibilities()
    {
        return possibilities;
    }

    /** @brief Consultora del atributo sumRow
    \pre <em>Cierto</em>
    \post Nos devuelve valor del atributo sumRow del parámetro implícito
     */
    public int getSumRow()
    {
        return sumRow;
    }

    /** @brief Consultora del atributo sumCol
    \pre <em>Cierto</em>
    \post Nos devuelve valor del atributo sumCol del parámetro implícito
     */
    public int getSumCol()
    {
        return sumCol;
    }

    //Modificadoras
    /** @brief Función que modifica el vector de posibilidades
    \pre <em>first ordenado de orden ascendente</em>
    \post Si el vector possibilities estaba vacío coloca el parametro first en su lugar
    Si no es el caso, coloca la intersección de ambos
     */
    public void putPossibilities(ArrayList<Integer> first)
    {
        if(possibilities.size() == 0) possibilities = first;
        else intersectVectors(first);
    }

    /** @brief Modificadora del valor
    \pre <em>0 <= v <= 9</em>
    \post Nos cambia el value del parámetro implícito
     */
    public void setValue(int v)
    {
        value = v;
    }

    /** @brief Modificadora de sumCol
    \pre <em>Cierto</em>
    \post El atributo sumCol del parametro implicito pasa a ser el valor pasado como parámetro a la funcion.
     */
    public void setSumCol(int sumCol)
    {
        this.sumCol = sumCol;
    }

    /** @brief Modificadora de sumRow
    \pre <em>Cierto</em>
    \post El atributo sumRow del parametro implicito pasa a ser el valor pasado como parámetro a la funcion.
     */
    public void setSumRow(int sumRow)
    {
        this.sumRow = sumRow;
    }

    /** @brief Modificadora que incrementa el puntero
    \pre <em>Cierto</em>
    \post Nos incrementa en 1 el puntero pointer del parametro implicito
     */
    public void incPointer()
    {
        ++pointer;
    }

    /** @brief Modificadora del valor de la casilla
    \pre <em>Cierto</em>
    \post El valor de la casilla pasa a ser el valor del vector de posibilades al que apunta el pointer
     */
    public void setValuePointer()
    {
        value = getPossibility();
    }

    /** @brief Modificadora del valor a través del puntero
    \pre <em>El puntero no puede apuntar a la última posición del vector de posibilidades</em>
    \post El valor de la casilla blanca es el próximo del vector de posibilidades
     */
    public void incValue()
    {
        incPointer();
        value = getPossibility();
    }

    /** @brief Operación que nos sirve para hacer un reset del puntero pointer y del valor de la casilla

    Se ejecuta cuando queremos dejar la casilla como al principio sin perder su vector de posibilidades
    \pre <em>Cierto</em>
    \post Se ha hecho un reset de la casilla
     */
    public void resetPointer()
    {
        pointer = -1;
        value = 0;
    }

    //Auxiliares
    /** @brief Operación de intersección de arrayLists
    Dado un vector aux ordenado crecientemente hace una intersección entre el vector possibilities y aux y lo guarda en el vector del parametro implicito
    \pre <em>aux está ordenado en orden creciente</em>
    \post El vector del parámetro implícito pasa a ser la intersección entre el parámetro de entrada y si mismo
     */
    private void intersectVectors(ArrayList<Integer> aux)
    {
        int i = 0, j = 0;
        ArrayList<Integer> result = new ArrayList<>();
        while(i < possibilities.size() && j < aux.size()){
            if(possibilities.get(i) > aux.get(j)) ++j;
            else if (possibilities.get(i) < aux.get(j)) ++i;
            else{
                result.add(possibilities.get(i));
                ++i; ++j;
            }
        }
        possibilities = result;
        inverseVector();
    }

    /** @brief Operación de inversión

    Invierte el vector possibilities del parámetro implícito
    \pre <em>Cierto</em>
    \post El vector del parámetro implícito pasa a ser su propia inversión
     */
    private void inverseVector()
    {
        ArrayList<Integer> result = new ArrayList<>(possibilities);
        int size = possibilities.size();
        for(int i = 0; i < size; ++i){
            possibilities.set(i,result.get(size-1-i));
        }
    }

    //Escritura
    /** @brief Operación que devuelve un string que representa una casilla en la salida no estándar
    \pre <em>Cierto</em>
    \post Retorna un string representando a la casilla blanca
     */
    public String printTile()
    {
        if (value == 0) return "     ";
        else return ("  " + value + "  ");
    }

    /** @brief FUNCION PRIVADA HECHA PÚBLICA PARA TESTEO CON JUNIT
    \pre <em>v1 y v2 ordenados ascendentemente</em>
    \post Retorna un arraylist que es la interseccion de v1 y v2
     */
    public ArrayList<Integer> publicIntersectVectors(ArrayList<Integer> v1, ArrayList<Integer> v2)
    {
        int i = 0, j = 0;
        ArrayList<Integer> result = new ArrayList<>();
        while(i < v2.size() && j < v1.size()){
            if(v2.get(i) > v1.get(j)) ++j;
            else if (v2.get(i) < v1.get(j)) ++i;
            else{
                result.add(v2.get(i));
                ++i; ++j;
            }
        }
        return result;
    }

    /** @brief FUNCION PRIVADA HECHA PÚBLICA PARA TESTEO CON JUNIT
    \pre <em>Cierto</em>
    \post Retorna un arraylist que es la inversión del parametro v
     */
    public ArrayList<Integer> publicInverseVector(ArrayList<Integer> v)
    {
        ArrayList<Integer> result = new ArrayList<>(v);
        int size = v.size();
        for(int i = 0; i < size; ++i){
            result.set(i,v.get(size-1-i));
        }
        return result;
    }
}
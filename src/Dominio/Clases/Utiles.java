/** @file Utiles.java
 @brief Especificación de la clase Utiles
 */

package Dominio.Clases;

import Dominio.Controladores.CtrlDominio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.Scanner;

/** @class Utiles
    @brief Representa un conjunto de funciones útiles de formato estético

    Hecha por Hasnain Shafqat

    Clase que contiene distintas funciones que no dependen de niguna clase, y nos son de ayuda en distintas clases
 */
public class Utiles
{

    /** @brief Controlador de dominio*/
    CtrlDominio ctrlDominio = new CtrlDominio();

    /** @brief Creadora por defecto
    \pre <em>Cierto</em>
    \post Nos crea un objeto de la clase Utiles vacío
     */
    public Utiles()
    {

    }

    /** @brief Operación que dado un string s, nos lo convierte en un array de ints
    \pre <em>Cierto</em>
    \post Nos devuelve el string s convertido a un array de numeros
     */
    public int[] convertStringtoIntArrayNums(String s)
    {
        String[] linea = s.split(" ");
        int[] nums = new int[linea.length];
        try {
            for (int i = 0; i < linea.length; ++i) {
                nums[i] = Integer.parseInt(linea[i]);
            }
        }
        catch (NumberFormatException e) {
            clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(wellTabed("|     ERROR:", "|"));
            System.out.println(wellTabed("|        Has introducido un carácter no valido", "|"));
            System.out.println(wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
            return new int[0];
        }
        return nums;
    }

    /** @brief Operación que nos lee un numero positivo de la terminal, pero tambien nos sirve para detectar entradas como un q o una r
    \pre <em>La entrada ha de ser diferente de numeros negtivos y caracteres que no sean alfabeticos</em>
    \post Nos devuelve distintos valores dependiendo de la entrada, -1 si es una q, -2 si es una r, -3 si es cualquiero otra letra,
          y finalmente si es un numero, devuelve un numero
     */
    public int readInt()
    {
        int integerToRead = 0;
        try {
            Scanner entrada = new Scanner(System.in);
            String line = entrada.nextLine();
            while (line.equals("")) line = entrada.nextLine();
            line = line.replaceAll(" ", "");
            String[] linea = line.split(" ");
            String primer = linea[0];
            primer = primer.toLowerCase();
            if (primer.charAt(0) == 'q') {
                return -1;
            } else if (primer.charAt(0) == 'r') return -2;
            else if (primer.charAt(0) > 'a' && primer.charAt(0) < 'z') return -3;
            integerToRead = Integer.parseInt(line);
        }
        catch (NumberFormatException e) {
            clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(wellTabed("|     ERROR:", "|"));
            System.out.println(wellTabed("|        Has introducido un carácter no valido", "|"));
            System.out.println(wellTabed("|        "+e.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
        return integerToRead;
    }

    /** @brief Operación que nos lee una linea de la terminal
    \pre <em>Cierto</em>
    \post Nos devuelve un string que no es vacío y además contiene la entrada de la primera linea introducida
     */
    public String readLine()
    {
        Scanner entrada = new Scanner(System.in);
        String line = entrada.nextLine();
        while (line.equals("")) line = entrada.nextLine();
        return line;
    }

    /** @brief Operación que trata un string de input
    \pre <em>s no es un string vacío</em>
    \post Nos devuelve -1 si es una q, -2 si una r, y un 0 en caso contrario
     */
    public int treatInput(String s)
    {
        String[] linea = s.split(" ");
        String primer = linea[0];
        primer = primer.toLowerCase();
        if (primer.charAt(0) == 'q') {
            return -1;
        }
        else if (primer.charAt(0) == 'r') return -2;
        else if (primer.charAt(0) > 'a' && primer.charAt(0) < 'z') return -3;
        else return 0;
    }

    /** @brief Operación que nos devuelve un string tabulado + symbol al final
    \pre <em>Cierto</em>
    \post Nos devuelve un string que es el string comment alargado para ocupar el espacio de 120 caracteres concatenando symbol al final
     */
    public String wellTabed(String comment, String symbol)
    {
        StringBuilder resultat = new StringBuilder(comment);
        int dif = 120-comment.length();
        resultat.append(" ".repeat(Math.max(0, dif - 1)));
        resultat.append(symbol);
        return resultat.toString();
    }

    /** @brief Operación que nos imprime una linea de iguales
    \pre <em>Cierto</em>
    \post Nos imprime por la pantalla una linea de iguales de longitud 120 caracteres
     */
    public void printLineEqual()
    {
        System.out.println("========================================================================================================================");
    }

    /** @brief Operación de escritura centrada
    \pre <em>Cierto</em>
    \post Nos devuelve un string que contiene el string comment rodeado por el parametro symbol, y centrado entre dos '|'
     */
    public String Centered(String comment, String symbol)
    {
        int dif = 120 - (comment.length()+6);
        int mitad = dif/2;
        int tam = mitad;
        StringBuilder res = new StringBuilder("|");
        if (dif%2 == 1) ++tam;
        res.append(" ".repeat(Math.max(0, tam)));
        res.append(symbol).append(" ");

        res.append(comment);

        res.append(" ").append(symbol);
        res.append(" ".repeat(Math.max(0, mitad)));
        res.append("|");
        return res.toString();
    }

    /** @brief Operación de escritura en un buen formato con espacios
    \pre <em>Cierto</em>
    \post Nos imprime el string s centrado horizontal y verticalmente en un rectangulo
     */
    public void printNiceWithSpaces(String s)
    {
        printLineEqual();
        System.out.println(Centered("", " "));
        System.out.println(Centered(s,"|"));
        System.out.println(Centered("", " "));
        printLineEqual();
    }

    /** @brief Operación de escritura en un buen formato
    \pre <em>Cierto</em>
    \post Nos imprime el string s centrado horizontalmente en un rectangulo
     */
    public void printNice(String s)
    {
        printLineEqual();
        System.out.println(Centered(s,"|"));
        printLineEqual();
    }

    /** @brief Operación que centra un Kakuro
    \pre <em>Cierto</em>
    \post Nos devuelve un string que contiene el string original centrado y en el formato adecuado para el menu
     */
    public String KakuroToCentred(String s)
    {
        StringBuilder res = new StringBuilder();
        String[] Lineas = s.split("\n");
        res.append(Centered(Lineas[0] + "   ", " "));
        for (int i = 1; i < Lineas.length;++i) {
            res.append("\n").append(Centered(Lineas[i], " "));
        }
        return res.toString();
    }

    /** @brief Operación que limpia la terminal
    \pre <em>Cierto</em>
    \post Nos limpia la terminal, equivalente a hacer un clear en la terminal y añadirle espacios
     */
    public void clearScreen()
    {
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
    }

    /** @brief Operación que dado un array de strings que contiene los nombres de distintos archivos de un repositorio,
        nos los imprime por la pantalla en el formato adecuado. Además, nos imprime las opciones de refrescar y salir en el mismo formato
    \pre <em>El parametro pathnames ha de ser diferente a null</em>
    \post Nos imprime la lista de archivos y las distintas operaciones que podemos realizar en el formato adecuado
     */
    public void showFiles(String[] pathnames)
    {
        int tam = Objects.requireNonNull(pathnames).length;
        System.out.println(wellTabed("|     Ficheros del repositorio:", "|"));
        for (int i = 0; i < tam; ++i) {
            String x = "|          ["+i + "]: " + pathnames[i];
            System.out.println(wellTabed(x, "|"));
        }
        System.out.println(Centered("", " "));
        System.out.println(wellTabed("|          [R] : Refrescar", "|"));
        System.out.println(wellTabed("|          [Q] : Salir", "|"));
        printLineEqual();
    }

    /** @brief Operación que dado un directorio, nos introduce en kActual el Kakuro que hayamos eligido como input.
    \pre <em>direc ha de ser un path existente y correcto</em>
    \post Nos devuelve un 0 si pudimos hacer la eleción de dicho archivo para poder reeler el kakuro, en caso contrario nos devuelve un -1
     */
    public int getI(Kakuro[] kActual, String direc)
    {
        int ret = 0;
        String[] pathnames = null;
        boolean refresca = true;
        int num = 0;
        while (refresca) {
            refresca = false;
            printNice("Escoge el número del archivo que quieres como input:");
            File f = new File(direc);
            pathnames = f.list();
            showFiles(pathnames);
            num = readInt();
            if (num == -2) refresca = true;
            else if (num == -1) ret = -1;
            clearScreen();
        }
        if (num >= 0 && num < Objects.requireNonNull(pathnames).length){
            ctrlDominio.readKakuroFromFile(direc+pathnames[num]);
            //System.out.println(ctrlDominio.kakuroToStringNice());
            kActual[0] = ctrlDominio.getKakuroDominio();
        }
        return ret;
    }

    /** @brief Operación que dado un directorio, nos modifica el Kakuro pasado como parametro a partir de un archivo que elegimos como input.
    Además, nos permite elegir el resultado esperado mediante otro archivo que elegiremos como output y este archivo lo pondremos en la primera posición del array expected
    \pre <em>direc ha de ser un path existente</em>
    \post Nos devuelve un 0 si pudimos hacer la eleción de dichos archivos, en caso contrario nos devuelve un -1
     */
    public int getIO(Kakuro[] k,String[] expected, String direc)
    {
        int ret = 0;
        try {
            boolean refresca = true;
            while (refresca) {
                refresca = false;
                printNice("INPUT FILE SELECTION");
                System.out.println(wellTabed("|     Escoge el número del archivo que quieres como input:", "|"));
                String[] pathnames;
                File f = new File(direc);
                pathnames = f.list();
                showFiles(pathnames);
                int num = readInt();
                if (num == -2) refresca = true;
                else if (num == -1) ret = -1;
                else if (num >= 0 && num < Objects.requireNonNull(pathnames).length){

                    ctrlDominio.readKakuroFromFile(direc+pathnames[num]);
                    k[0] = ctrlDominio.getKakuroDominio();

                    clearScreen();
                    refresca = true;
                    while (refresca) {
                        refresca = false;
                        clearScreen();
                        System.out.println(ctrlDominio.kakuroToStringNice());
                        printNice("OUTPUT FILE SELECTION");
                        System.out.println(wellTabed("|     Escoge el número del archivo que quieres como output:", "|"));
                        showFiles(pathnames);
                        num = readInt();
                        if (num == -2) refresca = true;
                        else if (num == -1) ret = -1;
                        else if (num >= 0 && num < pathnames.length){
                            File out = new File(direc + pathnames[num]);
                            Scanner scan = new Scanner(out);
                            while (scan.hasNext()) {
                                expected[0] += scan.nextLine() + "\n";
                            }
                        }
                    }
                    refresca = false;
                }
            }
        }
        catch(FileNotFoundException ex) {
            clearScreen();
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println(wellTabed("|     ERROR:", "|"));
            System.out.println(wellTabed("|        Has introducido un carácter no valido", "|"));
            System.out.println(wellTabed("|        "+ex.getMessage(),"|"));
            System.out.println("! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ");
            System.out.println();
        }
        return ret;
    }
}
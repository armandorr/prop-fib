/** @file Usuario.java
 @brief Especificación de la classe Usuario
 */

package Dominio.Clases;

/** @class Usuario
 @brief Representa un usuario en nuestro sistema

 Hecha por Marco Patiño

 Clase que representa un usuario. Contiene un username y un password
 */
public class Usuario
{
    /** @brief Nombre de usuario*/
    private String username;
    /** @brief Contraseña del usuario*/
    private String password;

    /** @brief Creadora por defecto
    \pre <em>Cierto</em>
    \post Nos crea un usuario con nombre de usuario y contraseña igual a null
     */
    public Usuario ()
    {
        username = null;
        password = null;
    }

    /** @brief Creadora que dado un nombre de usuario name y una contraseña pass nos crea un usuario
    \pre <em>Cierto</em>
    \post Nos crea un usuario con nombre de usuario igual a name y contraseña igual a pass
     */
    public Usuario (String name, String pass)
    {
        username = name;
        password = pass;
    }

    /** @brief Operación consultora que nos devuelve el nombre de usuario del parámetro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto el nombre de usuario del p.i
     */
    public String getUsername()
    {
        return username;
    }

    /** @brief Operación consultora que nos devuelve la contraseña del parámetro implícito
    \pre <em>Cierto</em>
    \post Hemos devuelto la contraseña del p.i
     */
    public String getPassword()
    {
        return password;
    }

    /** @brief Operación modificadora que dado un nombre de usuario us, cambia el nombre de usuario del parámetro implícito
     * por el string us
    \pre <em>Cierto</em>
    \post Hemos cambiado el nombre de usuario del p.i
     */
    public void setUserName(String us)
    {
        username = us;
    }

    /** @brief Operación modificadora que dada una contraseña pass, cambia la contraseña del parámetro implícito
     * por el string pass
    \pre <em>Cierto</em>
    \post Hemos cambiado la contraseña del p.i
     */
    public void setUserPass(String pass)
    {
        password = pass;
    }
}

/** @file MainConsola.java
 @brief Especificación de la clase MainConsola
 */

package Mains;

import Dominio.Controladores.CtrlPresentacion;

/** @class MainConsola
 @brief Función principal del programa por consola que inicializa el mismo llamando a CtrlPresentacion con consola

 Hecha por David Marin

 */
public class MainConsola
{
    /** @brief Función principal del programa por consola
    \pre <em>Cierto</em>
    \post Ha comenzado la presentación por consola
     */
    public static void main(String[] args)
    {
        CtrlPresentacion.iniciarPresentacionConsola();
    }
}
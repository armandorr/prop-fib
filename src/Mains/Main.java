/** @file Main.java
 @brief Especificación de la clase Main
 */

/** @mainpage Proyecto gestión Kakuros PROP entrega final 2020 Q1.

 Se construye un programa modular que ofrece un menú de opciones para realizar diversas funcionalidades sobre Kakuros.
 Se añaden los controladores de capas <em>CtrlPresentacion</em>, <em>CtrlDominio</em> y <em>CtrlPersistencia</em>
 y de funcionalidades <em>CtrlSolve</em>, <em>CtrlGenerate</em>, <em>CtrlPlay</em>, <em>CtrlVerify</em>.
 Se introducen muchas vistas de Presentacion que ofrecen una aplicación gráfica con todas las funcionalidades.
 Se introducen las clases de Dominio <em>Kakuro</em>, <em>Tile</em>, <em>WhiteTile</em>, <em>BlackTile</em>, <em>Utiles</em> y <em>Usuario</em>.
 Se introducen las clases de Persistencia <em>PersistenciaKakuro</em> y <em>PersistenciaUsuario</em>.
 Además se añaden drivers sobre los controladores de funcionalidades y un menú correspondiente para ejecutarlos y testear con ellos.
 Por último cabe destacar que hemos hecho tests unitarios con JUnit de las clases <em>Kakuro</em>, <em>WhiteTile</em>, <em>Utiles</em>,
 <em>Usuario</em>, <em>PersistenciaKakuro</em> y <em>PersistenciaUsuario</em>.
 */

package Mains;

import Dominio.Controladores.CtrlPresentacion;

/** @class Main
 @brief Función principal del programa que inicializa el mismo llamando a CtrlPresentacion

 Hecha por Hasnain Shafqat
 */
public class Main
{
    /** @brief Función principal de la apliación gráfica
    \pre <em>Cierto</em>
    \post Ha comenzado la presentación gráfica
     */
    public static void main(String[] args)
    {
        CtrlPresentacion.iniciarPresentacion(true);
    }
}
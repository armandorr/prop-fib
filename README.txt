Para compilar:							'make'
(tener instalado javac): 'sudo apt install default-jdk'

Despues, para:

a) ejecutar aplicacion grafica principal:  			'make run'

b) ejecutar aplicacion de consola principal:			'make runConsole'

c) ejecutar el menu de todos los drivers:			'make runDriver'	

d) crear el .jar de la aplicacion grafica:			'make jar'	      	-> crea KakuroGameAPP.jar
   d1) ejecutar el .jar creado anteriormente: 			'make runjar'

e) crear el .jar de la aplicacion de consola:   		'make jarConsole'     	-> crea KakuroGameCON.jar
   e1) ejecutar el .jar creado anteriormente: 			'make runjarConsole'

f) crear el .jar de los menus de drivers: 			'make jarDriver'	-> crea MenuDrivers.jar
   f1) ejecutar el .jar creado anteriormente: 			'make runjarDriver'

g) ejecutar TODAS las pruebas de JUnits:   			'make allJunits'
   g1) ejecutar la prueba de JUnit de Kakuro:  			'make junitKakuro'
   g2) ejecutar la prueba de JUnit de Tile:    			'make junitTile'
   g3) ejecutar la prueba de JUnit de Utiles:  			'make junitUties'
   g4) ejecutar la prueba de JUnit de Usuario:			'make junitUsuario'
   g5) ejecutar la prueba de JUnit de PersistenciaKakuro: 	'make junitPersistenciaKakuro'
   g6) ejecutar la prueba de JUnit de PersistenciaUsuario: 	'make junitPersistenciaUsuario'

h) limpiar contenido carpeta bin:				'make clean'

Recomendacion: Usar pantalla completa para visualizar correctamente los menus por consola
